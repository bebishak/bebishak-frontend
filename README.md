<div id="top"></div>

<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a href="https://gitlab.com/bebishak/bebishak-frontend">
    <img src="flutter_frontend/assets/logo.png" alt="Logo" width="80" height="80">
  </a>

  <h3 align="center">Kukie</h3>

  <p align="center">
    Take a photo of your fridge or pantry and get recipe recommendations based on the things you have!
    <!-- <br />
    <a href="https://github.com/othneildrew/Best-README-Template"><strong>Explore the docs »</strong></a> -->
    <!-- <br />
    <br />
    <a href="https://github.com/othneildrew/Best-README-Template">View Demo</a>
    ·
    <a href="https://github.com/othneildrew/Best-README-Template/issues">Report Bug</a>
    ·
    <a href="https://github.com/othneildrew/Best-README-Template/issues">Request Feature</a> -->
  </p>
</div>



<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <!-- <li><a href="#roadmap">Roadmap</a></li> -->
    <!-- <li><a href="#contributing">Contributing</a></li> -->
    <!-- <li><a href="#license">License</a></li> -->
    <li><a href="#members">Members</a></li>
    <li><a href="#acknowledgments">Acknowledgments</a></li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
## About The Project

The UN’s twelfth sustainable development goal is ‘Responsible Consumption and Production’, with its mission statement being ‘Ensuring sustainable consumption and production patterns’. The focus of this goal is to establish responsible habits for sustaining the environment. We are interested in catering our solution towards this goal in order to refine the overall consumption of raw ingredients during the process of cooking.

We aim to improve the consumption of many, specifically caretakers and young adults, through our recipe suggestion and ingredient detection application. At the start of the pandemic, specifically early March 2020 in Indonesia, most citizens had restricted access to in-person dining and were encouraged to stay and cook at home. This had the majority of people desperate to find ways to prepare different kinds of food at home from the ingredients that they have. Our aim is to provide an all in one solution for people to know what ingredients that they have and how to make use of them accordingly. This simplifies the ‘not knowing what to cook’ thought by turning it into a straightforward process.

Our solution makes it easier for users to quickly attain a list of the ingredients that they have, along with the collection of dishes they can make from them. By taking a picture of the contents of their fridge, pantry, and even groceries freshly bought from the store, our AI can detect the users’ ingredients, hence allowing our application to recommend an array of recipes that make use of the identified materials. These recipes range from Western to Eastern cuisines, all throughout breakfast, lunch and dinner, with a vegan filter option for those looking to exclusively browse through our plant-based alternatives. We seek to assist users in saving time to decide what they can make with what they have, as well as utilize the most of their staples, thus encouraging the avoidance of excessive food waste. Moreover, our application allows users to upload their own recipes and share them with others who may be looking for a unique taste, and give feedback through our favorites and rating systems, urging creativity and commitment in home cooking. 

This repository only hosts the frontend to our project. The <a href="https://gitlab.com/bebishak/bebishak-backend">Backend</a> and <a href="https://gitlab.com/bebishak/bebishak-ai2">AI</a> services are hosted in their own respective repos.

<div align="right">(<a href="#top">back to top</a>)</div>



## Built With

This repository holds the frontend section of the whole project. It is a mobile app that was created using [Flutter](https://flutter.dev/).

<div align="right">(<a href="#top">back to top</a>)</div>


<!-- GETTING STARTED -->
## Getting Started

If you would like to try out the finished application, you can download the app's APK from [here](https://storage.googleapis.com/kukie-apk/app.apk). _Note: only available for Android devices at the moment._

To get a local copy of the project up and running instead, follow these simple steps.

### Prerequisites

[Flutter](https://docs.flutter.dev/get-started/install/windows) needs to be installed, alongside a choice of your preferred IDE. [Android Studio](https://docs.flutter.dev/get-started/install/windows#android-setup) is recommended.

### Installation

1. Clone the repo
   ```sh
   git clone https://gitlab.com/bebishak/bebishak-frontend.git
   ```
2. Open `flutter_frontend` project using your IDE.
3. If there are errors due to missing imports, run the following command in the terminal from the project folder
   ```sh
   flutter pub get
   ```
4. The repo does not contain the credentials required by Firebase. Please contact this repo's owner or admin for the necessary files.
5. Connect your IDE to a mobile device (via emulator or your plugged in device) and hit
   ```sh
   flutter run --dart-define=BACKEND_HOSTNAME={BACKEND_HOSTNAME} --dart-define=AI_ADDR={AI_ADDRESS}
   ```
   You need to replace the characters inside the curly braces with the appropriate information. Replace them with the corresponding hostnames for the two services. For example:

    ```sh
   flutter run --dart-define=BACKEND_HOSTNAME=localhost --dart-define=AI_ADDR=localhost:4200
   ```
   
   This will launch the mobile app on your device with the appropriate conenctions with the services.

<div align="right">(<a href="#top">back to top</a>)</div>



<!-- USAGE EXAMPLES -->
## Usage

When you first open the app, you will be greated with the landing page. From here, you need to either register a new account or sign in.

<div align="center">
  <table class="tableImageCaption">
    <tr>
      <td> <img src="./screenshots/start.png" width=180> </td>
      <td> <img src="./screenshots/register.png" width=180> </td>
      <td> <img src="./screenshots/login.png" width=180> </td>
    </tr>
    <tr>
      <td> <p align="center"><i>Landing page</i></p> </td>
      <td> <p align="center"><i>Register</i></p> </td>
      <td> <p align="center"><i>Login</i></p> </td>
    </tr>
  </table>
</div>

Once you successfully logged in, you will see the home screen. Tap the big camera icon to take a photo of your fridge or pantry. After confirming, please wait for a moment for the image to be processed and you will see a list of ingredients that has been identified. You can manually input more ingredients if needed or remove some. Once you are done, a list of recipes will be given to you as suggestions of what you can make from the previously listed ingredients.

<div align="center">
  <table class="tableImageCaption">
    <tr>
      <td> <img src="./screenshots/camera.png"  width=180> </td>
      <td> <img src="./screenshots/processing.png"  width=180> </td>
      <td> <img src="./screenshots/ingredients.png"  width=180> </td>
      <td> <img src="./screenshots/recipes.png"  width=180> </td>
    </tr>
    <tr>
      <td> <p align="center"><i>Take a picture</i></p> </td>
      <td> <p align="center"><i>Processing image</i></p> </td>
      <td> <p align="center"><i>List of ingredients</i></p> </td>
      <td> <p align="center"><i>Recipe selection</i></p> </td>
    </tr>
  </table>
</div>

Tapping on any of the recipes will open their page and you can follow the steps to make a delicious meal!

<div align="center">
  <table class="tableImageCaption">
    <tr>
      <td> <img src="./screenshots/recipe.png"  width=180> </td>
      <td> <img src="./screenshots/recipe2.png"  width=180> </td>
      <td> <img src="./screenshots/recipe3.png"  width=180> </td>
    </tr>
  </table>
</div>

Dont forget to leave a like and rate your favorite recipes!

Want to contribute to Kukie? Feel free to upload your very own recipe for other people to enjoy!

<div align="center">
  <table class="tableImageCaption">
    <tr>
      <td> <img src="./screenshots/new_recipe.png"  width=180> </td>
      <td> <img src="./screenshots/new_recipe2.png"  width=180> </td>
      <td> <img src="./screenshots/new_recipe3.png"  width=180> </td>
    </tr>
  </table>
</div>

<div align="right">(<a href="#top">back to top</a>)</div>

<!-- MEMBERS -->
## Members

- [Gardyan Akbar](https://gitlab.com/giantsweetroll)
- [Jason Jeremy Wijadi](https://gitlab.com/digaji)
- [Aimee Putri Hartono](https://gitlab.com/gwenwaters)
- [Claudia Rachel Wijaya](https://gitlab.com/rachelwijaya)

Project Link: [https://gitlab.com/bebishak](https://gitlab.com/bebishak)

<div align="right">(<a href="#top">back to top</a>)</div>



<!-- ACKNOWLEDGMENTS -->
## Acknowledgments

_Flutter packages used in the making of the project_
* [collection](https://pub.dev/packages/collection)
* [contained_tab_bar_view](https://pub.dev/packages/contained_tab_bar_view)
* [firebase_auth](https://pub.dev/packages/firebase_auth)
* [firebase_core](https://pub.dev/packages/firebase_core)
* [firebase_storage](https://pub.dev/packages/firebase_storage)
* [flutter_cache_manager](https://pub.dev/packages/flutter_cache_manager)
* [flutter_launcher_icons](https://pub.dev/packages/flutter_launcher_icons)
* [flutter_spinkit](https://pub.dev/packages/flutter_spinkit)
* [fluttertoast](https://pub.dev/packages/fluttertoast)
* [google_fonts](https://pub.dev/packages/google_fonts)
* [image_picker](https://pub.dev/packages/image_picker)
* [provider](https://pub.dev/packages/provider)

<div align="right">(<a href="#top">back to top</a>)</div>



<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
<!-- [contributors-shield]: https://img.shields.io/github/contributors/othneildrew/Best-README-Template.svg?style=for-the-badge
[contributors-url]: https://github.com/othneildrew/Best-README-Template/graphs/contributors
[forks-shield]: https://img.shields.io/github/forks/othneildrew/Best-README-Template.svg?style=for-the-badge
[forks-url]: https://github.com/othneildrew/Best-README-Template/network/members
[stars-shield]: https://img.shields.io/github/stars/othneildrew/Best-README-Template.svg?style=for-the-badge
[stars-url]: https://github.com/othneildrew/Best-README-Template/stargazers
[issues-shield]: https://img.shields.io/github/issues/othneildrew/Best-README-Template.svg?style=for-the-badge
[issues-url]: https://github.com/othneildrew/Best-README-Template/issues -->
<!-- [license-shield]: https://img.shields.io/github/license/othneildrew/Best-README-Template.svg?style=for-the-badge -->
<!-- [license-url]: https://github.com/othneildrew/Best-README-Template/blob/master/LICENSE.txt
[linkedin-shield]: https://img.shields.io/badge/-LinkedIn-black.svg?style=for-the-badge&logo=linkedin&colorB=555
[linkedin-url]: https://linkedin.com/in/othneildrew -->
<!-- [product-screenshot]: images/screenshot.png -->

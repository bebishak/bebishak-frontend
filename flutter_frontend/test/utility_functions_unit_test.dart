import 'package:flutter_frontend/shared/constants.dart';
import 'package:flutter_frontend/shared/utility_functions.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  group('utility_functions.dart unit test - ', () {
    test('Test convertSortByIndexToEnum() function', () {
      expect(convertSortByIndexToEnum(0), SortBy.relevance);
      expect(convertSortByIndexToEnum(1), SortBy.rating);
      expect(convertSortByIndexToEnum(2), SortBy.uploadDate);
      expect(convertSortByIndexToEnum(3), SortBy.relevance);
      expect(convertSortByIndexToEnum(-1), SortBy.relevance);
    });

    test('Test convertUploadDateIndexToEnum() function', () {
      expect(convertUploadDateIndexToEnum(0), UploadDate.anytime);
      expect(convertUploadDateIndexToEnum(1), UploadDate.thisWeek);
      expect(convertUploadDateIndexToEnum(2), UploadDate.thisMonth);
      expect(convertUploadDateIndexToEnum(3), UploadDate.thisYear);
      expect(convertUploadDateIndexToEnum(4), UploadDate.anytime);
      expect(convertUploadDateIndexToEnum(-1), UploadDate.anytime);
    });

    test('Test convertCookingDurationIndexToEnum() function', () {
      expect(convertCookingDurationIndexToEnum(0), CookingDuration.any);
      expect(convertCookingDurationIndexToEnum(1), CookingDuration.short);
      expect(convertCookingDurationIndexToEnum(2), CookingDuration.medium);
      expect(convertCookingDurationIndexToEnum(3), CookingDuration.long);
      expect(convertCookingDurationIndexToEnum(4), CookingDuration.any);
      expect(convertCookingDurationIndexToEnum(-1), CookingDuration.any);
    });

    test('Test makeNullStringEmpty() function', () {
      expect(makeNullStringEmpty(null), '');
      expect(makeNullStringEmpty(''), '');
      expect(makeNullStringEmpty('null'), 'null');
      expect(makeNullStringEmpty('random'), 'random');
      expect(makeNullStringEmpty('random string'), 'random string');
      expect(makeNullStringEmpty('null string'), 'null string');
    });

    test('Test convertSortByEnumToIndex() function', () {
      expect(convertSortByEnumToIndex(SortBy.relevance), 0);
      expect(convertSortByEnumToIndex(SortBy.rating), 1);
      expect(convertSortByEnumToIndex(SortBy.uploadDate), 2);
    });

    test('Test convertUploadDateEnumToIndex() function', () {
      expect(convertUploadDateEnumToIndex(UploadDate.anytime), 0);
      expect(convertUploadDateEnumToIndex(UploadDate.thisWeek), 1);
      expect(convertUploadDateEnumToIndex(UploadDate.thisMonth), 2);
      expect(convertUploadDateEnumToIndex(UploadDate.thisYear), 3);
    });

    test('Test convertCookingDurationEnumToIndex() function', () {
      expect(convertCookingDurationEnumToIndex(CookingDuration.any), 0);
      expect(convertCookingDurationEnumToIndex(CookingDuration.short), 1);
      expect(convertCookingDurationEnumToIndex(CookingDuration.medium), 2);
      expect(convertCookingDurationEnumToIndex(CookingDuration.long), 3);
    });
  });
}
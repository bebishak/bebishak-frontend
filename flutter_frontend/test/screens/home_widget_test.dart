import 'package:flutter_frontend/models/models.dart';
import 'package:flutter_frontend/screens/home.dart';
import 'package:flutter_frontend/shared/testing.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() {
  group('HomeScreen widget test - ', () {
    testWidgets('Check for widgets existence', (WidgetTester tester) async {
      await tester.pumpWidget(StreamProvider.value(
        value: createDummyUserStream(),
        initialData: UninitializedKukieUser(),
        child: makeWidgetTestable(const HomeScreen()),
      ));
      await tester.pumpAndSettle();

//      List<Finder> finders = [
//        find.byKey(const Key('takeAPhoto')),
//        find.byKey(const Key('photoButton')),
//        find.byKey(const Key('subtitle')),
//        find.byKey(const Key('meal1')),
//        find.byKey(const Key('meal2')),
//        find.byKey(const Key('trendingRecipes')),
//        find.byKey(const Key('recentRecipes')),
//        find.byKey(const Key('yourRecipes')),
//        find.byKey(const Key('addRecipeCard')),
//      ];
//
//      for(Finder finder in finders) {
//        expect(finder, findsOneWidget);
//      }

      expect(find.byKey(const Key('takeAPhoto')), findsOneWidget);
      expect(find.byKey(const Key('photoButton')), findsOneWidget);
      expect(find.byKey(const Key('subtitle')), findsOneWidget);
      expect(find.byKey(const Key('meal1')), findsOneWidget);
      expect(find.byKey(const Key('meal2')), findsOneWidget);
      expect(find.byKey(const Key('trendingRecipes')), findsOneWidget);
      expect(find.byKey(const Key('recentRecipes')), findsOneWidget);
      expect(find.byKey(const Key('yourRecipes')), findsOneWidget);
      expect(find.byKey(const Key('addRecipeCard')), findsOneWidget);
    });

    testWidgets('Tapping on camera icon should open camera and go to processing screen', (WidgetTester tester) async {
      await tester.pumpWidget(StreamProvider.value(
        value: createDummyUserStream(),
        initialData: UninitializedKukieUser(),
        child: makeWidgetTestable(const HomeScreen()),
      ));
      await tester.pumpAndSettle();

      final finder = find.byKey(const Key('photoButton'));

      await tester.tap(finder);
      await tester.pumpAndSettle();
    });
  });
}
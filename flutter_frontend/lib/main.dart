import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_frontend/models/models.dart';
import 'package:flutter_frontend/screens/home.dart';
import 'package:flutter_frontend/services/auth.dart';
import 'package:flutter_frontend/shared/custom_theme.dart';
import 'package:flutter_frontend/widgets/auth_enforcer.dart';
import 'package:provider/provider.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return StreamProvider<KukieUser?>.value(
      value: AuthService().user,
      initialData: UninitializedKukieUser(),
      child: MaterialApp(
        key: const Key('root'),
        debugShowCheckedModeBanner: false,
        home: const AuthEnforcer(child: HomeScreen(),),
        // home: Builder(
        //   builder: (context) {
        //     return createTestDialogWidget(
        //       context,
        //       RecipeFilterDialog(),
        //     );
        //   }
        // ),
        // home: Scaffold(
        //   body: RecipeFilterDialog(),
        // ),
        theme: CustomTheme.lightTheme,
      ),
    );
  }
}

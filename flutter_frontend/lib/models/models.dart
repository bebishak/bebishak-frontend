import 'package:collection/collection.dart';
import 'package:flutter_frontend/shared/constants.dart';

class Ingredient {
  String name, unit;
  double amount;

  Ingredient({
    this.name = '',
    this.unit = '',
    this.amount = 0,
  });

  @override
  bool operator == (Object other) {
    if (identical(this, other)) {
      return true;
    } else if (other.runtimeType != runtimeType) {
      return false;
    } else {
      return other is Ingredient &&
          name == other.name &&
          unit == other.unit &&
          amount == other.amount;
    }
  }

  List<dynamic> convertDataAsList() => [
    name,
    amount,
    unit
  ];
}

class RecipeFilterData {
  String text;
  List<String> selectedMeals = [];
  List<String> selectedCuisines = [];
  bool veganOnly = false;
  bool likedOnly = false;
  SortBy sortBy = SortBy.relevance;
  UploadDate uploadDate = UploadDate.anytime;
  CookingDuration cookingDuration = CookingDuration.any;

  RecipeFilterData({
    String? text,
    List<String>? selectedMeals,
    List<String>? selectedCuisines,
    bool? veganOnly,
    bool? likedOnly,
    SortBy? sortBy,
    UploadDate? uploadDate,
    CookingDuration? cookingDuration,
  }) :
  text = text ?? '',
  selectedMeals = selectedMeals ?? [],
  selectedCuisines = selectedCuisines ?? [],
  veganOnly = veganOnly ?? false,
  likedOnly = likedOnly ?? false,
  sortBy = sortBy ?? SortBy.relevance,
  uploadDate = uploadDate ?? UploadDate.anytime,
  cookingDuration = cookingDuration ?? CookingDuration.any;

  RecipeFilterData copyWith({
    String? text,
    List<String>? selectedMeals,
    List<String>? selectedCuisines,
    bool? veganOnly,
    bool? likedOnly,
    SortBy? sortBy,
    UploadDate? uploadDate,
    CookingDuration? cookingDuration,
  }) => RecipeFilterData(
    text: text ?? this.text,
    selectedMeals: selectedMeals ?? this.selectedMeals,
    selectedCuisines: selectedCuisines ?? this.selectedCuisines,
    veganOnly: veganOnly ?? this.veganOnly,
    likedOnly: likedOnly ?? this.likedOnly,
    sortBy: sortBy ?? this.sortBy,
    uploadDate: uploadDate ?? this.uploadDate,
    cookingDuration: cookingDuration ?? this.cookingDuration
  );

  @override
  bool operator == (Object other) {

    Function eq = const ListEquality().equals;

    if (identical(this, other)) {
      return true;
    } else if (other.runtimeType != runtimeType) {
      return false;
    } else {
      return other is RecipeFilterData &&
          sortBy == other.sortBy &&
          uploadDate == other.uploadDate &&
          cookingDuration == other.cookingDuration &&
          veganOnly == other.veganOnly &&
          likedOnly == other.likedOnly &&
          eq(selectedMeals, other.selectedMeals) &&
          eq(selectedCuisines, other.selectedCuisines) &&
          text == other.text;
    }
  }
}

class Recipe {
  String username, name, desc, authorUid;
  String? imgUrl;
  List<String> mealTags, cuisineTags, steps;
  List<Ingredient> ingredients;
  double duration, globalRating, userRating;
  int rateCount;
  bool isLiked, isVegan;

  Recipe({
    required this.username,
    required this.authorUid,
    required this.name,
    this.desc = '',
    this.imgUrl,
    List<String>? mealTags,
    List<String>? cuisineTags,
    List<String>? steps,
    List<Ingredient>? ingredients,
    this.duration = 0,
    this.globalRating = 0,
    this.rateCount = 0,
    this.userRating = 0,
    this.isLiked = false,
    this.isVegan = false,
  }) : mealTags = mealTags ?? [],
  cuisineTags = cuisineTags ?? [],
  steps = steps ?? [],
  ingredients = ingredients ?? [];

  Recipe copyWith({
    String? username,
    String? authorUid,
    String? name,
    String? desc,
    String? imgUrl,
    List<String>? mealTags,
    List<String>? cuisineTags,
    List<String>? steps,
    List<Ingredient>? ingredients,
    double? duration,
    double? globalRating,
    int? rateCount,
    double? userRating,
    bool? isLiked,
    bool? isVegan,
  }) => Recipe(
    username: username ?? this.username,
    authorUid: authorUid ?? this.authorUid,
    name: name ?? this.name,
    desc: desc ?? this.desc,
    imgUrl: imgUrl ?? this.imgUrl,
    mealTags: mealTags ?? this.mealTags,
    cuisineTags: cuisineTags ?? this.cuisineTags,
    steps: steps ?? this.steps,
    ingredients: ingredients ?? this.ingredients,
    duration: duration ?? this.duration,
    globalRating: globalRating ?? this.globalRating,
    rateCount: rateCount ?? this.rateCount,
    isLiked: isLiked ?? this.isLiked,
    isVegan: isVegan ?? this.isVegan,
    userRating: userRating ?? this.userRating
  );

  @override
  bool operator == (Object other) {

    Function eq = const ListEquality().equals;

    if (identical(this, other)) {
      return true;
    } else if (other.runtimeType != runtimeType) {
      return false;
    } else {
      return other is Recipe &&
          username == other.username &&
          authorUid == other.authorUid &&
          name == other.name &&
          desc == other.desc &&
          imgUrl == other.imgUrl &&
          eq(mealTags, other.mealTags) &&
          eq(cuisineTags, other.cuisineTags) &&
          eq(ingredients, other.ingredients) &&
          duration == other.duration &&
          globalRating == other.globalRating &&
          rateCount == other.rateCount &&
          userRating == other.userRating &&
          isLiked == other.isLiked &&
          isVegan == other.isVegan;
    }
  }

  String convertStepsToString() {
    String str = "";

    for (String step in steps) {
      str = str + step + recipeStepsDeliminator;
    }

    // Remove last deliminator
    if (steps.length <= 0) {
      return '';
    } else {
      return str.substring(0, str.length - recipeStepsDeliminator.length);
    }
  }

  static List<String> convertStringToSteps(String stepsString) {
    return stepsString.split(recipeStepsDeliminator);
  }

  List<List<dynamic>> getIngredientsList() {
    List<List<dynamic>> ls = [];

    for (Ingredient ingredient in ingredients) {
      ls.add(ingredient.convertDataAsList());
    }

    return ls;
  }
}

class AzureAccessToken {
  final String value;

  AzureAccessToken(this.value);
}

class KukieUser{
  final String uid, username;
  bool enableNotifications, highlightMissingIngredients;

  KukieUser({
    required this.uid,
    required this.username,
    this.enableNotifications = false,
    this.highlightMissingIngredients = true,
  });
}

class UninitializedKukieUser extends KukieUser {
  UninitializedKukieUser() : super(
    uid: 'undefined',
    username: 'undefined'
  );
}
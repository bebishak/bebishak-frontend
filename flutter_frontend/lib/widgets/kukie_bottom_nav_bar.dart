import 'package:flutter/material.dart';
import 'package:flutter_frontend/screens/home.dart';
import 'package:flutter_frontend/screens/ingredients.dart';
import 'package:flutter_frontend/screens/processing.dart';
import 'package:flutter_frontend/screens/profile.dart';
import 'package:flutter_frontend/services/image_picker.dart';
import 'package:flutter_frontend/shared/constants.dart';
import 'package:flutter_frontend/shared/utility_functions.dart';
import 'package:flutter_frontend/shared/widget_prefabs.dart';
import 'package:flutter_frontend/widgets/auth_enforcer.dart';
import 'package:image_picker/image_picker.dart';

class KukieBottomNavigationBar extends StatelessWidget {

  final int currentIndex;

  const KukieBottomNavigationBar({
    Key? key,
    this.currentIndex = 0
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      currentIndex: currentIndex,
      showSelectedLabels: false,
      showUnselectedLabels: false,
      key: const Key('navBar'),
      iconSize: 35,
      onTap: (index) async {
        Navigator.popUntil(context, (route) => route.isFirst);
        switch (index) {
          case 0:
            Navigator.pushReplacement(
                context,
                createFadingPageRouteBuilder(child: const AuthEnforcer(
                  child: HomeScreen(),
                ))
            );
            break;

          case 1:
            await performTakePhotoOperation(context);
            break;

          case 2:
            Navigator.pushReplacement(
              context,
              createFadingPageRouteBuilder(child: const ProfileScreen())
            );
            break;

          default:
            break;
        }
      },
      items: const [
        BottomNavigationBarItem(
            icon: Icon(
              Icons.home,
              color: greenColor3,
            ),
            label: ''
        ),
        BottomNavigationBarItem(
            icon: Icon(
              Icons.camera_alt,
              color: greenColor3,
            ),
            label: ''
        ),
        BottomNavigationBarItem(
            icon: Icon(
              Icons.person,
              color: greenColor3,
            ),
            label: ''
        ),
      ],
    );
  }
}

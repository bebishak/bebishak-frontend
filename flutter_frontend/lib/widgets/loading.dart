import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

/// Show the loading widget in a dialog
void showLoading(BuildContext context) {
  showDialog(
      context: context,
      barrierDismissible: false,
      useSafeArea: true,
      builder: (context) {
        return const Loading();
      }
  );
}

class Loading extends StatelessWidget {
  const Loading({Key? key}) : super(key: key);

  Widget _createSpinKitWidget() {
    return Center(
      child: SpinKitFadingCircle(
        color: Colors.blue[400],
        size: 50.0,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.transparent,
      child: _createSpinKitWidget(),
    );
  }
}
import 'package:flutter/material.dart';
import 'package:flutter_frontend/models/models.dart';
import 'package:flutter_frontend/shared/constants.dart';
import 'package:flutter_frontend/widgets/kukie_expansion_tile.dart';
import 'package:flutter_frontend/shared/utility_functions.dart';
import 'package:flutter_frontend/widgets/toggle_button.dart';
import 'package:google_fonts/google_fonts.dart';

class RecipeFilterDialog extends StatefulWidget {

  final RecipeFilterData? filterInfo;
  final Function(RecipeFilterData filterInfo) onSaved;

  const RecipeFilterDialog({
    Key? key,
    required this.onSaved,
    this.filterInfo,
  }) : super(key: key);

  @override
  _RecipeFilterDialogState createState() => _RecipeFilterDialogState();
}

class _RecipeFilterDialogState extends State<RecipeFilterDialog> {

  late RecipeFilterData filterInfo;
  late List<bool> mealSelection;
  late List<bool> cuisineSelection;
  late int sortBySelectedIndex;
  late int uploadDateSelectedIndex;
  late int cookingDurationSelectedIndex;

  Widget _createDropDown({
    required List<String> options,
    required int selectedIndex,
    Function(int index)? onChanged,
  }) => KukieExpansionTile(
      title: Text(
        options[selectedIndex],
        overflow: TextOverflow.ellipsis,
        style: Theme.of(context).textTheme.caption!.copyWith(
          color: Colors.white,
        ),
      ),
      headerBackgroundColor: greenColor3,
      headerBorderRadius: BorderRadius.circular(10),
      headerBoxShadow: const [
        BoxShadow(
            color: Color.fromRGBO(0, 0, 0, 0.15),
            blurRadius: 10,
            spreadRadius: 1,
            offset: Offset(0, 2)
        )
      ],
      trailingIconColor: greyColor,
      expandedCrossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        for (int i=0; i<options.length; i++) if (i != selectedIndex) Padding(
          padding: const EdgeInsets.fromLTRB(35, 3, 3, 0),
          child: InkWell(
            onTap: () {
              setState(() {
                selectedIndex = i;
                if (onChanged != null) {
                  onChanged(i);
                }
              });
            },
            child: Container(
              padding: const EdgeInsets.symmetric(
                horizontal: 20,
                vertical: 15,
              ),
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(10),
                  boxShadow: const [
                    BoxShadow(
                      color: Color.fromRGBO(0, 0, 0, 0.15),
                      blurRadius: 5,
                      spreadRadius: 2,
                      offset: Offset(0, 2)
                    )
                  ]
              ),
              child: Text(
                options[i],
                maxLines: 10,
                overflow: TextOverflow.ellipsis,
                style: Theme.of(context).textTheme.caption!.copyWith(
                  height: calculateLineSpacing(13, 16),
                  wordSpacing: calculateLineSpacing(13, 16),
                  color: darkGreyColor,
                ),
              ),
            ),
          ),
        ),
        const SizedBox(height: 10,),
      ]
  );

  Widget _createDropDownTitle(String title) => Text(
    title,
    style: GoogleFonts.lato(
      textStyle: Theme.of(context).textTheme.bodyText1,
      fontWeight: FontWeight.bold,
      color: greenColor1,
      letterSpacing: 3.5,
      height: calculateLineSpacing(16, 19),
      wordSpacing: calculateLineSpacing(16, 19),
    ),
  );

  @override
  void initState() {
    super.initState();
    filterInfo = widget.filterInfo ?? RecipeFilterData();
    mealSelection = List.generate(meals.length, (index) => false);
    cuisineSelection = List.generate(cuisines.length, (index) => false);

    sortBySelectedIndex = convertSortByEnumToIndex(filterInfo.sortBy);
    uploadDateSelectedIndex = convertUploadDateEnumToIndex(filterInfo.uploadDate);
    cookingDurationSelectedIndex = convertCookingDurationEnumToIndex(filterInfo.cookingDuration);

    // Get meal and cuisine selection
    for (String meal in filterInfo.selectedMeals) {
      mealSelection[meals.indexOf(meal)] = true;
    }
    for (String cuisine in filterInfo.selectedCuisines) {
      cuisineSelection[cuisines.indexOf(cuisine)] = true;
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        widget.onSaved(filterInfo);
        return true;
      },
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(20),
          boxShadow: const [
            BoxShadow(
              color: Color.fromRGBO(0, 0, 0, 0.15),
              offset: Offset(0, 2),
              blurRadius: 5,
              spreadRadius: 3,
            ),
          ],
        ),
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(
              vertical: 20,
              horizontal: 25,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisSize: MainAxisSize.min,
              children: [
                Text(
                  'MEAL',
                  style: GoogleFonts.lato(
                    textStyle: Theme.of(context).textTheme.bodyText1,
                    fontWeight: FontWeight.bold,
                    color: brownColor1,
                    letterSpacing: 3.5,
                    wordSpacing: calculateLineSpacing(16, 19),
                    height: calculateLineSpacing(16, 19),
                  ),
                ),
                const SizedBox(height: 10,),
                SelectionListView(
                  selections: meals,
                  selected: mealSelection,
                  onPressed: (isSelected, text) {
                    if (isSelected) {
                      filterInfo.selectedMeals.add(text);
                    } else {
                      filterInfo.selectedMeals.remove(text);
                    }
                  },
                ),
                const SizedBox(height: 20,),
                Text(
                  'CUISINE',
                  style: GoogleFonts.lato(
                    textStyle: Theme.of(context).textTheme.bodyText1,
                    fontWeight: FontWeight.bold,
                    color: brownColor1,
                    letterSpacing: 3.5,
                    wordSpacing: calculateLineSpacing(16, 19),
                    height: calculateLineSpacing(16, 19),
                  ),
                ),
                const SizedBox(height: 10,),
                SelectionListView(
                  selections: cuisines,
                  selected: cuisineSelection,
                  onPressed: (isSelected, text) {
                    if (isSelected) {
                      filterInfo.selectedCuisines.add(text);
                    } else {
                      filterInfo.selectedCuisines.remove(text);
                    }
                  },
                ),
                const SizedBox(height: 30,),
                const Divider(
                  thickness: 3,
                  color: greyColor,
                ),
                const SizedBox(height: 10,),
                // Vegan only option
                ToggleSelection(
                  title: 'vegan only',
                  isToggled: filterInfo.veganOnly,
                  onChanged: (isToggled) {
                    filterInfo.veganOnly = isToggled;
                  }
                ),
                ToggleSelection(
                    title: 'liked only',
                    isToggled: filterInfo.likedOnly,
                    onChanged: (isToggled) {
                      filterInfo.likedOnly = isToggled;
                    }
                ),
                const SizedBox(height: 10,),
                const Divider(
                  thickness: 3,
                  color: greyColor,
                ),
                const SizedBox(height: 20,),
                _createDropDownTitle('SORT BY'),
                const SizedBox(height: 5,),
                _createDropDown(
                  options: sortBy,
                  selectedIndex: sortBySelectedIndex,
                  onChanged: (index) {
                    sortBySelectedIndex = index;
                    filterInfo.sortBy = convertSortByIndexToEnum(index);
                  }
                ),
                const SizedBox(height: 10,),
                _createDropDownTitle('UPLOAD DATE'),
                const SizedBox(height: 5,),
                _createDropDown(
                  options: uploadDate,
                  selectedIndex: uploadDateSelectedIndex,
                  onChanged: (index) {
                    uploadDateSelectedIndex = index;
                    filterInfo.uploadDate = convertUploadDateIndexToEnum(index);
                  }
                ),
                const SizedBox(height: 10,),
                _createDropDownTitle('COOKING DURATION'),
                const SizedBox(height: 5,),
                _createDropDown(
                    options: cookingDuration,
                    selectedIndex: cookingDurationSelectedIndex,
                    onChanged: (index) {
                      cookingDurationSelectedIndex = index;
                      filterInfo.cookingDuration = convertCookingDurationIndexToEnum(index);
                    }
                ),
                const SizedBox(height: 40,),
                Center(
                  child: ElevatedButton(
                    style: roundedButton.copyWith(
                      backgroundColor: getMaterialColorProperty(greenColor1)
                    ),
                    onPressed: () {
                      Navigator.pop(context);
                      widget.onSaved(filterInfo);
                    },
                    child: Text(
                      'CLOSE',
                      style: GoogleFonts.lato(
                        textStyle: Theme.of(context).textTheme.bodyText1,
                        fontWeight: FontWeight.bold,
                        letterSpacing: 3.5,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class SelectionListView extends StatefulWidget {

  final List<String> selections;
  final List<bool> selected;
  final Function(bool isSelected, String text)? onPressed;

  /// Creates a horizontal ListView with toggle buttons
  /// [selections] A list containing the available selections (in String)
  /// [selected] A list of boolean indicating which of the selection is initially toggled
  /// [onPressed] A callback function that returns the status of the toggle and the text of the selection that triggered it
  const SelectionListView({
    Key? key,
    required this.selections,
    required this.selected,
    this.onPressed,
  }) : assert(selections.length == selected.length),
        super(key: key);

  @override
  _SelectionListViewState createState() => _SelectionListViewState();
}

class _SelectionListViewState extends State<SelectionListView> {

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Row(
          children: [
            for (int i = 0; i < widget.selections.length; i++) Padding(
                padding: const EdgeInsets.symmetric(horizontal: 5),
                child: ToggleElevatedButton(
                  child: Text(
                    widget.selections[i].toUpperCase(),
                    style: Theme.of(context).textTheme.caption!.copyWith(
                        letterSpacing: 2.85,
                        height: calculateLineSpacing(13, 16),
                        wordSpacing: calculateLineSpacing(13, 16),
                        color: Colors.white
                    ),
                  ),
                  isToggled: widget.selected[i],
                  onChanged: (isSelected) {
                    setState(() {
                      widget.selected[i] = isSelected;
                      if (widget.onPressed != null) {
                        widget.onPressed!(isSelected, widget.selections[i]);
                      }
                    });
                  },
                )
            ),
          ]
      ),
    );
  }
}

class ToggleSelection extends StatefulWidget {

  final String title;
  final bool isToggled;
  final Function(bool isToggled) onChanged;

  const ToggleSelection({
    Key? key,
    required this.title,
    required this.onChanged,
    this.isToggled = false,
  }) : super(key: key);

  @override
  _ToggleSelectionState createState() => _ToggleSelectionState();
}

class _ToggleSelectionState extends State<ToggleSelection> {

  bool isToggled = false;

  @override
  void initState() {
    super.initState();
    isToggled = widget.isToggled;
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Text(
          widget.title.toUpperCase(),
          style: GoogleFonts.lato(
            textStyle: Theme.of(context).textTheme.bodyText1,
            fontWeight: FontWeight.bold,
            letterSpacing: 3.5,
            height: calculateLineSpacing(16, 19),
            wordSpacing: calculateLineSpacing(16, 19),
            color: greenColor1,
          ),
        ),
        Expanded(
          child: Align(
            alignment: Alignment.centerRight,
            child: Switch(
              value: isToggled,
              activeTrackColor: greyColor,
              inactiveTrackColor: greenColor3,
              thumbColor: getMaterialColorProperty(greenColor1),
              onChanged: (enabled) {
                setState(() {
                  isToggled = enabled;
                  widget.onChanged(enabled);
                });
              },
            ),
          ),
        ),
      ],
    );
  }
}

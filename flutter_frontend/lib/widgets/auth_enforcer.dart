import 'package:flutter/material.dart';
import 'package:flutter_frontend/models/models.dart';
import 'package:flutter_frontend/screens/start.dart';
import 'package:provider/provider.dart';

/// A widget used to enforce authentication. If user authentication fails,
/// bring the user to the start screen
class AuthEnforcer extends StatelessWidget {

  final Widget child;
  final bool informUserWhenSessionExpire;

  /// Class constructor
  /// [child] The child widget to display when authentication succeeds
  /// [informUserWhenSessionExpire] Whether to display message informing
  /// the user their current session has expired.
  const AuthEnforcer({
    Key? key,
    required this.child,
    this.informUserWhenSessionExpire = false
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    KukieUser? user = context.watch<KukieUser?>();

    return user != null && user is! UninitializedKukieUser? child : StartScreen(
      showSessionExpireMessage: informUserWhenSessionExpire,
    );
  }
}

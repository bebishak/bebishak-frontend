import 'package:flutter/material.dart';
import 'package:flutter_frontend/models/models.dart';
import 'package:flutter_frontend/screens/recipe_detail.dart';
import 'package:flutter_frontend/shared/constants.dart';
import 'package:flutter_frontend/shared/utility_functions.dart';
import 'package:flutter_frontend/shared/widget_prefabs.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

class RecipeTile extends StatelessWidget {

  final double borderRadius = 20;
  final double horizontalPadding = 10;
  final double imgWidth;
  final Recipe recipe;
  final List<String> ownedIngredients;

  const RecipeTile({
    Key? key,
    required this.recipe,
    this.ownedIngredients = const [],
    this.imgWidth = 94,
  }) : super(key: key);

  /// Open the recipe page of the recipe
  void openRecipePage(BuildContext context) {
    Navigator.push(
      context,
      createFadingPageRouteBuilder(child: RecipeScreen(
        recipe: recipe,
        ownedIngredients: ownedIngredients,
      )),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          const SizedBox(width: 5,),
          Flexible(
            flex: 1,
            child: InkWell(
              onTap: () {
                openRecipePage(context);
              },
              child: Container(
                padding: const EdgeInsets.all(8.0),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(borderRadius),
                    color: Colors.white,
                    boxShadow: const [
                      BoxShadow(
                        color: Color.fromRGBO(0, 0, 0, 0.15),
                        blurRadius: 5,
                        spreadRadius: 3,
                        offset: Offset(0, 1),
                      )
                    ]
                ),
                child: createImageWithPlaceholderWidget(
                    placeholderPath: 'assets/dish.png',
                    imgUrl: makeNullStringEmpty(recipe.imgUrl),
                    imgSize: imgWidth
                ),
              ),
            ),
          ),
          const SizedBox(width: 10,),
          Flexible(
            flex: 2,
            child: InkWell(
              onTap: () {
                openRecipePage(context);
              },
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(borderRadius),
                  boxShadow: const [
                    BoxShadow(
                      color: Color.fromRGBO(0, 0, 0, 0.15),
                      blurRadius: 5,
                      spreadRadius: 3,
                      offset: Offset(0, 1),
                    )
                  ],
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Container(
                      decoration: BoxDecoration(
                          color: greenColor1,
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(borderRadius),
                            topRight: Radius.circular(borderRadius),
                          )
                      ),
                      padding: EdgeInsets.fromLTRB(
                        horizontalPadding,
                        10,
                        horizontalPadding,
                        5,
                      ),
                      child: Text(
                        recipe.name.toUpperCase(),
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                          fontFamily: 'LeagueSpartan',
                          color: Colors.white,
                          fontSize: 16,
                          letterSpacing: 3.5,
                          height: calculateLineSpacing(16, 21),
                          wordSpacing: calculateLineSpacing(16, 21),
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(
                        horizontalPadding,
                        5,
                        horizontalPadding,
                        0,
                      ),
                      child: Row(
                        mainAxisSize: MainAxisSize.max,
                        children: [
                          RatingBar.builder(
                            allowHalfRating: false,
                            initialRating: recipe.globalRating,
                            ignoreGestures: true,
                            itemSize: 23,
                            itemBuilder: (context, _) => const Icon(
                              Icons.star_rounded,
                              color: Colors.amber,
                            ),
                            onRatingUpdate: (rating) {},
                          ),
                          const SizedBox(width: 10,),
                          Text(
                            '( ${recipe.rateCount} )',
                            style: Theme.of(context).textTheme.overline!.copyWith(
                              color: greenColor1,
                              letterSpacing: 2.41,
                              wordSpacing: calculateLineSpacing(11, 13),
                              height: calculateLineSpacing(11, 13),
                            ),
                          ),
                          Expanded(
                            flex: 1,
                            child: Align(
                              alignment: Alignment.centerRight,
                              child: Icon(
                                recipe.isLiked ? Icons.favorite : Icons.favorite_outline,
                                color: recipe.isLiked? Colors.red : const Color.fromRGBO(0, 0, 0, 0.4),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(
                        horizontal: horizontalPadding,
                        vertical: 3,
                      ),
                      child: Text(
                        'By ${recipe.username}'.toUpperCase(),
                        style: Theme.of(context).textTheme.caption!.copyWith(
                          color: brownColor2,
                          letterSpacing: 2.85,
                          wordSpacing: calculateLineSpacing(13, 16),
                          height: calculateLineSpacing(13, 16),
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(
                        horizontalPadding,
                        0,
                        horizontalPadding,
                        15,
                      ),
                      child: Text(
                        recipe.desc,
                        maxLines: 2,
                        style: Theme.of(context).textTheme.caption!.copyWith(
                          color: darkGreyColor,
                          letterSpacing: 1.2,
                          height: calculateLineSpacing(13, 16),
                          wordSpacing: calculateLineSpacing(13, 16),
                        ),
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class UploadImageSelection extends StatelessWidget {

  final Function(XFile? pickedImage) onImagePicked;
  final double iconSize = 50;

  const UploadImageSelection({
    Key? key,
    required this.onImagePicked,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        InkWell(
          onTap: () {
            // TODO: Upload image from gallery
          },
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Icon(
                Icons.photo,
                size: iconSize,
              ),
              Text(
                'Gallery',
                style: Theme.of(context).textTheme.bodyText1,
              )
            ],
          ),
        ),
        const SizedBox(width: 10,),
        InkWell(
          onTap: () {
            // TODO: Take photo from camera
          },
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Icon(
                Icons.camera_alt,
                size: iconSize,
              ),
              Text(
                'Camera',
                style: Theme.of(context).textTheme.bodyText1,
              )
            ],
          ),
        )
      ],
    );
  }
}

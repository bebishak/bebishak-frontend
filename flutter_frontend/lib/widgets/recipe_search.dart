import 'package:flutter/material.dart';
import 'package:flutter_frontend/models/models.dart';
import 'package:flutter_frontend/shared/constants.dart';
import 'package:flutter_frontend/widgets/recipe_filter.dart';
import 'package:google_fonts/google_fonts.dart';

class RecipeSearch extends StatefulWidget {

  final RecipeFilterData? filterInfo;
  final Color backgroundColor;
  final TextEditingController? inputController;
  final Function(RecipeFilterData filter) onSearch;

  const RecipeSearch({
    Key? key,
    this.filterInfo,
    this.backgroundColor = Colors.white,
    this.inputController,
    required this.onSearch,
  }) : super(key: key);

  @override
  _RecipeSearchState createState() => _RecipeSearchState();
}

class _RecipeSearchState extends State<RecipeSearch> {

  late RecipeFilterData filterInfo;
  String recipeName = '';

  @override
  void initState() {
    super.initState();
    filterInfo = widget.filterInfo ?? RecipeFilterData();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(25),
          boxShadow: const [
            BoxShadow(
              color: Color.fromRGBO(0, 0, 0, 0.01),
              spreadRadius: 1,
              blurRadius: 3,
              offset: Offset(0, 2),
            ),
          ]
      ),
      child:  Row(
        children: [
          IconButton(
            icon: const Icon(
              Icons.search,
              size: 30,
              color: Color.fromRGBO(0, 0, 0, 0.42),
            ),
            onPressed: () {
              widget.onSearch(filterInfo.copyWith(
                text: recipeName
              ));
            },
          ),
          Expanded(
            child: TextField(
              controller: widget.inputController,
              style: GoogleFonts.lato(
                textStyle: Theme.of(context).textTheme.subtitle2,
                fontWeight: FontWeight.normal,
                letterSpacing: 3.5,
                color: brownColor1,
              ),
              decoration: InputDecoration(
                contentPadding: const EdgeInsets.symmetric(
                  vertical: 0,
                ),
                border: InputBorder.none,
                hintText: 'SEARCH FOR RECIPES...',
                hintStyle: GoogleFonts.lato(
                  textStyle: Theme.of(context).textTheme.subtitle2,
                  fontWeight: FontWeight.normal,
                  letterSpacing: 3.5,
                  color: greyColor,
                ),
              ),
              onChanged: (val) => recipeName = val.trim(),
              onSubmitted: (String val) {
                widget.onSearch(filterInfo.copyWith(
                  text: recipeName
                ));
              },
            ),
          ),
          // IconButton(
          //   icon: const Icon(
          //     Icons.filter_list_alt,
          //     size: 30,
          //     color: Color.fromRGBO(0, 0, 0, 0.42),
          //   ),
          //   onPressed: () {
          //     showDialog(
          //       context: context,
          //       builder: (context) => Dialog(
          //         child: RecipeFilterDialog(
          //           filterInfo: filterInfo,
          //           onSaved: (filterInfo) {
          //             this.filterInfo = filterInfo.copyWith(
          //               text: recipeName,
          //               selectedMeals: filterInfo.selectedMeals.toSet().toList(),
          //               selectedCuisines: filterInfo.selectedCuisines.toSet().toList(),
          //             );
          //           },
          //         ),
          //       ),
          //     );
          //   },
          // ),
        ],
      ),
    );
  }
}

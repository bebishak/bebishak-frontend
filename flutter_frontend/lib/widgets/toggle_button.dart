import 'package:flutter/material.dart';
import 'package:flutter_frontend/shared/constants.dart';
import 'package:flutter_frontend/shared/utility_functions.dart';

class ToggleElevatedButton extends ElevatedButton {

  ToggleElevatedButton({
    Key? key,
    required Widget child,
    bool isToggled = false,
    Color toggledColor = brownColor1,
    Color untoggledColor = brownColor3,
    Function(bool)? onChanged,
    ButtonStyle? style,
  }) : super(key: key,
    onPressed: () {
      if (onChanged != null) {
        onChanged(!isToggled);
      }
    },
    style: style ?? roundedButton.copyWith(
      backgroundColor: getMaterialColorProperty(
        isToggled ? toggledColor : untoggledColor,
      ),
    ),
    child: child,
  );
}
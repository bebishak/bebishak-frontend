import 'package:flutter/material.dart';
import 'package:flutter_frontend/shared/utility_functions.dart';

class EmptyFieldValidator {
  static String? validate(String? val, {FocusNode? focusNode}) {

    if (makeNullStringEmpty(val).isEmpty) {
      if (focusNode != null) {
        focusNode.requestFocus();
      }
      return 'Field cannot be empty';
    } else {
      return null;
    }
  }
}

class EmailFieldValidator {
  static String? validate(String? val) =>
      makeNullStringEmpty(val).isEmpty? 'Field cannot be empty' : null;
}

class PasswordFieldValidator {
  static String? validate(String? val) =>
      makeNullStringEmpty(val).length < 6? 'Password too short' : null;
}

class ConfirmPasswordFieldValidator {
  /// Checks if the second password matches the first.
  static String? validate(String? val, String firstPass) =>
      makeNullStringEmpty(val) != firstPass? 'Password does not match' : null;
}

class PositiveNumberFieldValidator {
  static String? validate(String? val, {
    bool greaterThanZero = true,
    FocusNode? focusNode,
  }) {
    try {
      String? checkEmpty = EmptyFieldValidator.validate(val, focusNode: focusNode);
      if (checkEmpty == null) {
        String value = makeNullStringEmpty(val);
        double numValue = double.parse(value);

        if (greaterThanZero) {
          if (numValue <= 0) {
            if (focusNode != null) {
              focusNode.requestFocus();
            }

            return 'Value must be greater than zero';
          } else {
            return null;
          }
        } else {
          if (numValue < 0) {
            if (focusNode != null) {
              focusNode.requestFocus();
            }
            return 'Value must not be negative';
          } else {
            return null;
          }
        }
      } else {
        return checkEmpty;
      }
    } catch (e) {
      return 'Unable to process input';
    }
  }
}
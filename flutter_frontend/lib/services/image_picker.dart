import 'package:image_picker/image_picker.dart';

/// Takes a picture of an image
Future<XFile?> takePhoto() async {
  final ImagePicker picker = ImagePicker();

  return await picker.pickImage(source: ImageSource.camera);
}

/// Pick a picture from the user's phone gallery
Future<XFile?> pickGallery() async {
  final ImagePicker picker = ImagePicker();

  return await picker.pickImage(source: ImageSource.gallery);
}
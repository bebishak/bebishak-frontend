import 'dart:io';

import 'package:firebase_storage/firebase_storage.dart';
import 'package:image_picker/image_picker.dart';

import 'package:path/path.dart' as path;

class StorageService {
  final FirebaseStorage _storage = FirebaseStorage.instance;

  /// Uploads the recipe image to cloud storage.
  /// Returns the url of the image if upload was a success.
  Future<String> uploadRecipeImage({
    required XFile image,
    required String uid,
    required String recipeName,
  }) async {
    // Convert XFile to File
    File file = File(image.path);
    // String fName = path.basename(file.path);
    String extension = path.extension(file.path);
    String fName = '$uid-$recipeName';
    String fNameWithExtension = '$fName$extension';

    // // Rename image file
    // String oriDir = path.dirname(file.path);
    // String newDir = path.join(oriDir, fNameWithExtension);
    // file = file.renameSync(newDir);

    Reference storageRef = _storage.ref().child(fNameWithExtension);
    await storageRef.putFile(file);

    return await storageRef.getDownloadURL();
  }
}
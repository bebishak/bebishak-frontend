// import 'package:aad_oauth/aad_oauth.dart';
// import 'package:aad_oauth/model/config.dart';
import 'package:flutter_frontend/models/models.dart';
import 'dart:developer' as developer;
import 'package:firebase_auth/firebase_auth.dart';

class AuthService {
  final FirebaseAuth _auth = FirebaseAuth.instance;

  /// Convert User from firebase to KukieUser
  KukieUser? userFromFirebaseUser(User? user) {
    return user != null && user.emailVerified? KukieUser(
      uid: user.uid,
      username: user.displayName ?? '',
    ) : null;
  }

  /// auth change user stream
  Stream<KukieUser?> get user {
    return _auth.userChanges().map(userFromFirebaseUser);
  }

  /// sign in with email and password
  Future signIn(String email, String password) async {
    UserCredential result = await _auth.signInWithEmailAndPassword(email: email, password: password);
    User? user = result.user;

    if (user!.emailVerified) {
      return user;
    } else {
      logout();
      return null;
    }
  }

  /// register with email and password
  Future register({
    required String email,
    required String password,
    required String username,
  }) async {
    // try {
    //   developer.log('Registering a new user to firebase...', name: 'auth.dart - register()');
    //   UserCredential result = await _auth.createUserWithEmailAndPassword(email: email, password: password);
    //   User? user = result.user;
    //
    //   // Update display name
    //   developer.log('Updating display name...', name: 'auth.dart - register()');
    //   await user!.updateDisplayName(username);
    //
    //   // Send email verification
    //   developer.log('Sending email verification...', name: 'auth.dart - register()');
    //   await user.sendEmailVerification();
    //
    //   developer.log('Registering process complete!', name: 'auth.dart - register()');
    //
    //   return user;
    // } catch(e) {
    //   developer.log(e.toString(), name: 'auth.dart - register()');
    //   return null;
    // }
    developer.log('Registering a new user to firebase...', name: 'auth.dart - register()');
    UserCredential result = await _auth.createUserWithEmailAndPassword(email: email, password: password);
    User? user = result.user;

    // Update display name
    developer.log('Updating display name...', name: 'auth.dart - register()');
    await user!.updateDisplayName(username);

    // Send email verification
    developer.log('Sending email verification...', name: 'auth.dart - register()');
    await user.sendEmailVerification();

    developer.log('Registering process complete!', name: 'auth.dart - register()');

    return user;
  }

  /// sign out
  Future logout() async {
    try {
      return await _auth.signOut();
    } catch (e, stacktrace) {
      developer.log(
        'Unable to sign out',
        error: e,
        stackTrace: stacktrace,
        name: 'auth.dart - signOut()'
      );
      return null;
    }
  }

  /// Change the password of the currently logged in user.
  Future changePassword(User user, String oldPass, String newPass) async {
    final cred = EmailAuthProvider.credential(email: user.email!, password: oldPass);

    await user.reauthenticateWithCredential(cred).then((value) async {
      await user.updatePassword(newPass);
    });
  }

  /// Function for 'forgot password'.
  Future resetPassword(String email) async {
    await _auth.sendPasswordResetEmail(email: email);
  }

  /// Change the email of the currently logged in user.
  Future changeEmail(User user, String password, String newEmail) async {
    final cred = EmailAuthProvider.credential(
        email: user.email!,
        password: password
    );

    await user.reauthenticateWithCredential(cred).then((value) async {
      await user.updateEmail(newEmail);
      await user.updatePassword(password);
    });
  }
}

// Old Auth code using Azure
// class AuthService {
//
//   static const String clientId = '61b57d50-795f-40cc-9835-c9c457bdacbe';
//   static const String tenantId = '45025077-442f-4fe1-a097-6b708064d2e0';
//   static const String redirectUri = 'https://login.live.com/oauth20_desktop.srf';
//   static const String clientSecret = 'ya-7Q~_qASHrZd~jumLiPUYav4DV~GQQYV9zk';
//
//   static final Config config = Config(
//     tenant: 'bebishak',
//     clientId: clientId,
//     redirectUri: redirectUri,
//     // clientSecret: clientSecret,
//     scope: '$clientId offline_access',
//     isB2C: true,
//     policy: 'B2C_1_kukie-signup-signin',
//     nonce: 'defaultNonce',
//     prompt: 'login'
//   );
//   // static final Config config = Config(
//   //   tenant: tenantId,
//   //   clientId: clientId,
//   //   redirectUri: redirectUri,
//   //   scope: 'openid profile offline_access',
//   // );
//   late AadOAuth oauth;
//
//   AuthService() {
//     oauth = AadOAuth(config);
//   }
//
//   Future<String?> login () async {
//     developer.log('logging in...', name: 'auth.dart - login()');
//     await oauth.login();
//     developer.log('login task completed', name: 'auth.dart - login()');
//     return await oauth.getAccessToken();
//   }
//
//   Future logout() async {
//     developer.log('logging out...', name: 'auth.dart - logout()');
//     await oauth.logout();
//     developer.log('logged out', name: 'auth.dart - logout()');
//   }
//
//   /// Checks for user access key continuously
//   /// [checkRate] How frequent to do the check
//   static Stream<AzureAccessToken?> checkAuthentication({
//     Duration checkRate = const Duration(seconds: 1)
//   }) async* {
//     AadOAuth auth = AadOAuth(config);
//     while (true) {
//       String? token = await auth.getAccessToken();
//       yield token != null ? AzureAccessToken(token) : null;
//       Future.delayed(checkRate);
//       // yield AzureAccessToken('pending...');
//     }
//   }
// }
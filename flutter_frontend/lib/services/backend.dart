import 'dart:convert';
import 'dart:typed_data';

import 'package:flutter_frontend/models/models.dart';
import 'package:flutter_frontend/shared/constants.dart';
import 'package:image_picker/image_picker.dart';
import 'package:http/http.dart' as http;

import 'dart:developer' as developer;

/// Uploads image to the backend to get list of detected ingredients.
Future<List<String>> identifyIngredients(XFile image, {
  int retries = 2
}) async {
  // await Future.delayed(const Duration(minutes: 10));
  developer.log(
    'Reading image file as bytes...',
    name: 'backend.dart - identifyIngredients()'
  );
  Uint8List imageBytes = await image.readAsBytes();
  developer.log(
      'Done...',
      name: 'backend.dart - identifyIngredients()'
  );

  Uri locationUri = Uri(
      scheme: 'https',
      host: bebishakAiAddress,
      path: '/detect'
  );

  developer.log(
      'Creating HTTP Multipart request',
      name: 'backend.dart - identifyIngredients()'
  );
  var request = http.MultipartRequest('POST', locationUri);
  request.files.add(
    http.MultipartFile.fromBytes(
      'file',
      imageBytes,
      filename: image.name
    )
  );

  late http.StreamedResponse response;

  for (int currentRetry = 0; currentRetry <= retries; currentRetry++) {
    developer.log(
        'Sending request (${currentRetry + 1})',
        name: 'backend.dart - identifyIngredients()'
    );
    response = await request.send();

    developer.log(
        'Response code ${response.statusCode}',
        name: 'backend.dart - identifyIngredients()'
    );

    if (response.statusCode == 200) {
      developer.log(
          'Request success',
          name: 'backend.dart - identifyIngredients()'
      );
      var jsonData = jsonDecode(await response.stream.bytesToString());

      List<String> ingredients = jsonData['ingredients'];

      return ingredients;
    }
  }

  developer.log(
      'Request failed',
      name: 'backend.dart - identifyIngredients()'
  );
  var responseMsg = jsonDecode(await response.stream.bytesToString());
  throw Exception("(${response.statusCode}) $responseMsg");
}

/// Sends the list of ingredients to the backend and get relevant recipes.
///
/// If [ingredients] is provided, the function will only return recipes
/// that matches/contains the specified ingredients. This feature is
/// turned off if [userUid] is provided.
///
/// If a value for [userUid] and [ingredients] is null is provided, this function will return
/// all recipes associated by that particular user.
///
/// If a value for [isLiked] is provided, [userUid] cannot be null.
/// Additionally this function will return all recipes liked by the
/// specified user, even if that recipe does not belong to the user.
Future<List<Recipe>> getRecipes({
  List<String>? ingredients,
  RecipeFilterData? filterOptions,
  required String userUid,
  bool? isLiked,
  int retries = 2,
}) async {
  RecipeFilterData filter = filterOptions ?? RecipeFilterData();

  late http.Response response;

  if (ingredients != null) {
    //#region get recipes based off ingredients
    Uri locationUri = Uri(
        scheme: 'https',
        host: backendHostname,
        path: '/get_recipes_avail',
    );

    for (int currentRetry = 0; currentRetry <= retries; currentRetry++) {
      developer.log(
        'Getting recipes from available ingredients... ($currentRetry)',
        name: 'backend.dart - getRecipes()'
      );
      response = await http.post(
        locationUri,
        headers: <String, String> {
          'Content-Type' : 'application/json; charset=UTF-8'
        },
        body: jsonEncode(<String, dynamic> {
          "ingredients" : ingredients,
          "search" : filter.text,
          "userId" : userUid
        })
      );

      developer.log(
          'Response code: ${response.statusCode}',
          name: 'backend.dart - getRecipes()'
      );

      if (response.statusCode == 200) {
        return getRecipesFromResponse(response.body);
      }
    }
    //#endregion
  } else if (isLiked == null) {
    //#region get own recipe
    Uri locationUri = Uri(
      scheme: 'https',
      host: backendHostname,
      path: '/get_own_recipe',
    );

    for (int currentRetry = 0; currentRetry <= retries; currentRetry++) {
      developer.log(
          'Getting the user\'s own recipes... ($currentRetry)',
          name: 'backend.dart - getRecipes()'
      );
      response = await http.post(
        locationUri,
        headers: <String, String> {
          'Content-Type' : 'application/json; charset=UTF-8'
        },
        body: jsonEncode(<String, dynamic> {
          "id" : userUid,
          "search" : filter.text,
        })
      );

      developer.log(
          '(Own recipe) Response code: ${response.statusCode}',
          name: 'backend.dart - getRecipes()'
      );

      if (response.statusCode == 200) {
        return getRecipesFromResponse(response.body);
      }
    }

    //#endregion
  } else {
    //#region get liked recipes
    Uri locationUri = Uri(
      scheme: 'https',
      host: backendHostname,
      path: '/get_liked_recipes',
    );

    for (int currentRetry = 0; currentRetry <= retries; currentRetry++) {
      developer.log(
          'Getting the liked recipes... ($currentRetry)',
          name: 'backend.dart - getRecipes()'
      );

      response = await http.post(
        locationUri,
        headers: <String, String> {
          'Content-Type' : 'application/json; charset=UTF-8'
        },
          body: jsonEncode(<String, dynamic> {
            "id" : userUid,
            "search" : filter.text,
          })
      );

      developer.log(
          '(Liked recipe) Response code: ${response.statusCode}',
          name: 'backend.dart - getRecipes()'
      );

      if (response.statusCode == 200) {
        return getRecipesFromResponse(response.body);
      }
    }

    //#endregion
  }

  // If goes here, means failed
  developer.log(
      'Failed (${response.statusCode}): ${response.body}',
      name: 'backend.dart - getRecipes()'
  );
  return [];
}

/// Get trending recipes
Future<List<Recipe>> getTrendingRecipes({
  required String userUid,
  int retries = 2,
}) async {
  Uri locationUri = Uri(
      scheme: 'https',
      host: backendHostname,
      path: '/popular_recipes'
  );

  late http.Response response;

  for (int currentRetry = 0; currentRetry <= retries; currentRetry++) {
    developer.log(
        'Getting trending recipes... ($currentRetry)',
        name: 'backend.dart - getTrendingRecipes()'
    );
    response = await http.post(
        locationUri,
        headers: <String, String> {
          'Content-Type' : 'application/json; charset=UTF-8'
        },
        body: jsonEncode(<String, dynamic> {
          "id" : userUid,
        })
    );

    developer.log(
        'Response code: ${response.statusCode}',
        name: 'backend.dart - getTrendingRecipes()'
    );

    if (response.statusCode == 200) {
      return getRecipesFromResponse(response.body);
    }
  }

  // If goes here, means unable to get list of trending recipes
  developer.log(
      'Unable to get trending recipes (${response.statusCode}): ${response.body}',
      name: 'backend.dart - getTrendingRecipes()'
  );
  return [];
}

/// Get recent recipes associated with the provided user [uid]
Future<List<Recipe>> getRecentRecipes(String uid) async {
  List<Recipe> recipes = [];

  // TODO: Add proper implementation
  // Future.delayed(const Duration(seconds: 1));

  return recipes;
}

/// Deletes the selected recipe from the database
Future deleteRecipe(Recipe recipe, {
  int retries = 2
}) async {
  Uri locationUri = Uri(
      scheme: 'https',
      host: backendHostname,
      path: '/delete_recipe',
    queryParameters: {
      "title" : recipe.name,
      "userId" : recipe.authorUid,
    }
  );

  late http.Response response;

  for (int currentRetry = 0; currentRetry <= retries; currentRetry++) {
    developer.log(
        'Deleting recipe...($currentRetry)',
        name: 'backend.dart - deleteRecipe()'
    );
    response = await http.post(
      locationUri,
      headers: <String, String> {
        'Content-Type' : 'application/json; charset=UTF-8'
      },
    );

    developer.log(
        'Deleting recipe response code: ${response.statusCode}',
        name: 'backend.dart - deleteRecipe()'
    );

    if (response.statusCode == 200) {
      return;
    }
  }

  // If goes here something went wrong
  developer.log(
      'Unable to delete recipe (${response.statusCode}): ${response.body}',
      name: 'backend.dart - getTrendingRecipes()'
  );

  throw Exception("(${response.statusCode}) ${response.body}");
}

/// Update the user rating and favorite status of the recipe
Future<void> updateRecipeRatingAndFavoriteStatus({
  required KukieUser user,
  required Recipe recipe,
  int retries = 2,
}) async {
  developer.log(
    'Performing updating user rating and favorite status...',
    name: 'backend.dart - updateRecipeRatingAndFavoriteStatus()'
  );
  bool isLiked = recipe.isLiked;

  bool updateLikeSuccess = false, updateUserRatingSuccess = false;

  Uri uriIsLiked = Uri(
      scheme: 'https',
      host: backendHostname,
      path: isLiked ? '/likes_recipe' : '/unlike_recipe',
  );

  Uri uriUserRating = Uri(
      scheme: 'https',
      host: backendHostname,
      path: '/rate_recipe',
  );

  late http.Response responseLike;
  late http.Response responseRating;

  for (int currentRetry = 0; currentRetry <= retries; currentRetry++) {
    //#region Like/Unlike recipe
    developer.log(
        'Updating like status ($currentRetry)',
        name: 'backend.dart - updateRecipeRatingAndFavoriteStatus()'
    );
    if (!updateLikeSuccess) {
      responseLike = await http.post(
        uriIsLiked,
        headers: <String, String> {
          'Content-Type' : 'application/json; charset=UTF-8'
        },
        body: jsonEncode(<String, dynamic> {
          "authorId" : recipe.authorUid,
          "likerId" : user.uid,
          "recipeTitle" : recipe.name,
        })
      );

      developer.log(
          'Response code: ${responseLike.statusCode}',
          name: 'backend.dart - updateRecipeRatingAndFavoriteStatus()'
      );
    }

    if (responseLike.statusCode == 200) {
      developer.log(
          'Successfully updated recipe like status',
          name: 'backend.dart - updateRecipeRatingAndFavoriteStatus()'
      );
      updateLikeSuccess = true;
    }
    //#endregion

    //#region Update user rating for recipe
    developer.log(
        'Updating like user rating for recipe ($currentRetry)',
        name: 'backend.dart - updateRecipeRatingAndFavoriteStatus()'
    );
    if (!updateUserRatingSuccess) {
      responseRating = await http.post(
        uriUserRating,
        headers: <String, String> {
          'Content-Type' : 'application/json; charset=UTF-8'
        },
        body: jsonEncode(<String, dynamic> {
          "authorId" : recipe.authorUid,
          "likerId" : user.uid,
          "recipeTitle" : recipe.name,
          "rating" : recipe.userRating.toString(),
        })
      );

      developer.log(
          'Response code: ${responseRating.statusCode}',
          name: 'backend.dart - updateRecipeRatingAndFavoriteStatus()'
      );
    }

    if (responseRating.statusCode == 201) {
      developer.log(
          'Successfully updated user rating for recipe',
          name: 'backend.dart - updateRecipeRatingAndFavoriteStatus()'
      );
      updateUserRatingSuccess = true;
    }
    //#endregion

    if (updateUserRatingSuccess && updateLikeSuccess) {
      return;
    }
  }

  // If goes here, mean one or both processes failed

  String errMsgLike = "Updating Like status (${responseLike.statusCode}):\n"
                      "${responseLike.body}";
  String errMsgRating = "Updating recipe user rating (${responseRating.statusCode}):\n"
                        "${responseRating.body}";

  String errMsgFinal = "";

  if (!updateLikeSuccess && !updateUserRatingSuccess) {
    errMsgFinal = "Unable update like status and rating for recipe\n"
      // "--------------------------------------------------\n"
      "$errMsgLike\n"
      // "--------------------------------------------------\n"
      "$errMsgRating";
  } else if (!updateLikeSuccess) {
    errMsgFinal = "Unable update like status and rating for recipe\n"
        "$errMsgLike";
  } else {
    errMsgFinal = "Unable update like status and rating for recipe\n"
        "$errMsgFinal";
  }

  throw Exception(errMsgFinal);
}

/// Stores user details in the neo4j database
Future<void> createUserInDb(KukieUser user, {
  int retries = 2,
}) async {
  developer.log(
      'Creating user with uid:${user.uid} and username:${user.username} in the db...',
      name: 'backend.dart - createUserInDb()'
  );

  Uri locationUri = Uri(
    scheme: 'https',
    host: backendHostname,
    path: '/create',
    queryParameters: {
      "id" : user.uid,
      "username" : user.username,
      "notif" : false.toString(),
      "missingIngredients" : true.toString(),
    },
  );

  late http.Response response;

  for (int currentRetry = 0; currentRetry <= retries; currentRetry++) {
    developer.log(
        "Performing request ($currentRetry)",
        name: 'backend.dart - createUserInDb()'
    );
    response = await http.post(
      locationUri,
      headers: <String, String> {
        'Content-Type' : 'application/json'
      },
    );

    developer.log(
        "Response code: ${response.statusCode}",
        name: 'backend.dart - createUserInDb()'
    );


    if (response.statusCode == 200) {
      developer.log(
          "Success",
          name: 'backend.dart - createUserInDb()'
      );
      return;
    }
  }

  throw Exception("Unable to crate user in db (${response.statusCode}):\n${response.body}");
}

/// Checks if the user already exists in the database
Future<bool> checkIfUserExistsInDb(KukieUser user, {
  int retries = 2,
}) async {
  Uri locationUri = Uri(
      scheme: 'https',
      host: backendHostname,
      path: '/check_user',
      queryParameters: {
        'userId' : user.uid
      }
  );

  late http.Response response;

  for (int currentRetry = 0; currentRetry <= retries; currentRetry++) {
    response = await http.post(
      locationUri,
      headers: <String, String> {
        'Content-Type' : 'application/json'
      },
    );

    if (response.statusCode == 200) {
      return response.body.toLowerCase() == 'true';
    }
  }

  // If it gets here it means it keeps on failing after max retries
  throw Exception("Unable to process request (${response.statusCode}):\n${response.body}");

}

/// Upload a recipe to the database.
///
/// Set the [isUpdate] flag to true if you would like to update a created recipe
/// or otherwise to create a new one.
Future<void> uploadRecipe(Recipe recipe, {
  int retries = 2,
  bool isUpdate = false,
}) async {
  Uri locationUri = Uri(
      scheme: 'https',
      host: backendHostname,
      path: isUpdate ? '/update_recipe' : '/new_recipe'
  );
  Map<String, String> headers = <String, String> {
    'Content-Type' : 'application/json',
  };
  String body = jsonEncode(<String, dynamic> {
    "userId" : recipe.authorUid,
    "title" : recipe.name,
    "img" : recipe.imgUrl ?? "",
    "time" : recipe.duration,
    "desc" : recipe.desc,
    "isVegan" : recipe.isVegan,
    "ingredientsList" : recipe.getIngredientsList(),
    "cuisines" : recipe.cuisineTags,
    "meals" : recipe.mealTags,
    "steps" : recipe.convertStepsToString()
  });

  late http.Response response;

  for (int currentRetry = 0; currentRetry <= retries; currentRetry++) {
    response = isUpdate ? await http.put(
        locationUri,
        headers: headers,
        body: body
    ) : await http.post(
        locationUri,
        headers: headers,
        body: body
    );

    if (response.statusCode == 200) {
      return;
    }
  }

  // If it gets here it means it keeps on failing after max retries
  throw Exception("Unable to process request (${response.statusCode}):\n${response.body}");
}

/// Converts response body into a list of Recipe
List<Recipe> getRecipesFromResponse(String responseBody) {
  List<Recipe> recipes = [];
  for (dynamic item in jsonDecode(responseBody)) {
    Recipe recipe = Recipe(
      name: item['title'],
      authorUid: item['authorId'],
      username: item['authorUsername'],
      cuisineTags: [...item['cuisines']],
      mealTags: [...item['meals']],
      isLiked: item['isLiked'],
      imgUrl: item['img'],
      desc: item['desc'],
      isVegan: item['isVegan'],
      rateCount: item['rateCount'],
      userRating: item['userRating'].toDouble(),
      globalRating: item['globalrates']['rating'] ?? 0,
      duration: item['time'].toDouble(),
      steps: Recipe.convertStringToSteps(item['steps']),
      ingredients: [...item['ingredientsList'].map((x) {
        return Ingredient(
          name: x[0],
          amount: x[1].toDouble(),
          unit: x[2],
        );
      }).toList()],
    );
    recipes.add(recipe);
  }

  return recipes;
}
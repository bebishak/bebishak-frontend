import 'package:flutter/material.dart';

const String appTitle = 'Kukie';
const String recipeStepsDeliminator = '&nbsp';
const String backendHostname = String.fromEnvironment('BACKEND_HOSTNAME');
const String bebishakAiAddress = String.fromEnvironment('AI_ADDR');
// const String backendHostname = "10.0.2.2";

const Color brownColor1 = Color.fromRGBO(159, 99, 82, 1.0);
const Color brownColor2 = Color.fromRGBO(184, 140, 128, 1.0);
const Color brownColor3 = Color.fromRGBO(213, 185, 177, 1.0);
const Color greenColor1 = Color.fromRGBO(69, 115, 115, 1.0);
const Color greenColor2 = Color.fromRGBO(115, 144, 134, 1.0);
const Color greenColor3 = Color.fromRGBO(164, 189, 190, 1.0);
const Color blueColor = Color.fromRGBO(163, 203, 204, 1.0);
const Color greyColor = Color.fromRGBO(216, 216, 214, 1.0);
const Color darkGreyColor = Color.fromRGBO(112, 112, 112, 1.0);

Color primaryButtonColor = brownColor2;
Color secondaryButtonColor = greenColor2;

const List<String> meals = [
  'Breakfast',
  'brunch',
  'lunch',
  'snack',
  'dinner'
];
const List<String> cuisines = [
  'western',
  'chinese',
  'korean',
  'indonesian'
];
final List<String> sortBy = [
  'RELEVANCE',
  'RATING',
  'UPLOAD DATE'
];
final List<String> uploadDate = [
  'ANYTIME',
  'THIS WEEK',
  'THIS MONTH',
  'THIS YEAR'
];
final List<String> cookingDuration = [
  'ANY',
  'SHORT (< 10 MINUTES)',
  'MEDIUM (10 - 30 MINUTES)',
  'LONG (> 30 MINUTES)',
];

const List<BoxShadow> containerDefaultShadow = [
  BoxShadow(
    color: Color.fromRGBO(0, 0, 0, 0.1),
    offset: Offset(0, 4),
    blurRadius: 0.1,
    spreadRadius: 0.5
  )
];

InputDecoration defaultInputDecoration = InputDecoration(
  contentPadding: const EdgeInsets.symmetric(
    horizontal: 10,
    vertical: 0,
  ),
  border: OutlineInputBorder(
    borderRadius: BorderRadius.circular(15),
    borderSide: BorderSide.none,
  ),
  filled: true,
  fillColor: greyColor,
);

ButtonStyle roundedButton = ButtonStyle(
  elevation: MaterialStateProperty.all<double>(4),
  shape: MaterialStateProperty.all<RoundedRectangleBorder>(
    RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(18.0),
    ),
  ),
);

enum SortBy {relevance, rating, uploadDate }

enum UploadDate {anytime, thisWeek, thisMonth, thisYear}

enum CookingDuration {short, medium, long, any}
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_frontend/models/models.dart';
import 'package:flutter_frontend/services/backend.dart';
import 'package:flutter_frontend/shared/constants.dart';
import 'package:flutter_frontend/shared/utility_functions.dart';
import 'package:flutter_frontend/widgets/loading.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';

import 'dart:developer' as developer;

/// Create a PageRouteBuilder equipped with fading transition.
PageRouteBuilder createFadingPageRouteBuilder({
  required Widget child,
  Duration fadeDuration = const Duration(milliseconds: 200),
}) => PageRouteBuilder(
  pageBuilder: (context, a1, a2) => child,
  transitionsBuilder: (context, anim, a2, child) => FadeTransition(opacity: anim, child: child),
  transitionDuration: fadeDuration,
);

/// Creates the widget for the loading upload dialog
Widget createUploadLoadingDialogWidget(BuildContext context) => createKukieDialogContent(

  content: Padding(
    padding: const EdgeInsets.fromLTRB(15, 0, 25, 15),
    child: Padding(
      padding: const EdgeInsets.fromLTRB(15, 20, 15, 40),
      child: Text(
        'UPLOADING IN PROGRESS...',
        textAlign: TextAlign.center,
        style: Theme.of(context).textTheme.subtitle1!.copyWith(
          color: greenColor1,
          letterSpacing: 2,
          height: calculateLineSpacing(20, 32),
          wordSpacing: calculateLineSpacing(20, 32),
        ),
      ),
    ),
  ),
  iconPath: 'assets/breakfast.png',
);

/// Creates the widget for the delete recipe dialog
Widget createDeleteRecipeDialogWidget(
    BuildContext context, {
    required String userId,
    required Recipe recipe,
    Function()? onDelete,
}) => createKukieDialogContent(
  content: Padding(
    padding: const EdgeInsets.fromLTRB(15, 25, 15, 30),
    child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          'ARE YOU SURE YOU WOULD LIKE TO DELETE THIS RECIPE?',
          textAlign: TextAlign.center,
          style: Theme.of(context).textTheme.subtitle1!.copyWith(
            color: greenColor1,
            letterSpacing: 2,
            height: calculateLineSpacing(20, 32),
            wordSpacing: calculateLineSpacing(20, 32),
          ),
        ),
        const SizedBox(height: 25,),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            ElevatedButton(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 5),
                child: Text(
                  'YES',
                  style: GoogleFonts.lato(
                    textStyle: Theme.of(context).textTheme.bodyText1,
                    fontWeight: FontWeight.normal,
                    color: Colors.white,
                    letterSpacing: 3.5
                  ),
                ),
              ),
              style: roundedButton.copyWith(
                backgroundColor: getMaterialColorProperty(
                  Theme.of(context).colorScheme.secondary,
                ),
              ),
              onPressed: () async {
                showLoading(context);
                await deleteRecipe(recipe).then((value) {
                  Navigator.pop(context);   // Pop loading
                  Navigator.pop(context);   // Pop this dialog
                  Navigator.pop(context);   // Pop recipe form
                  Fluttertoast.showToast(msg: 'Recipe deleted');

                  if (onDelete != null) {
                    onDelete();
                  }

                }).catchError((e) {
                  Navigator.pop(context);   // Pop loading
                  developer.log(
                    'Unable to delete recipe:',
                    error: e,
                    name: 'widget_prefabs.dart - createDeleteRecipeDialogWidget()'
                  );
                  Fluttertoast.showToast(msg: 'Something went wrong, please try again');
                });
              },
            ),
            ElevatedButton(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10),
                child: Text(
                  'NO',
                  style: GoogleFonts.lato(
                      textStyle: Theme.of(context).textTheme.bodyText1,
                      fontWeight: FontWeight.normal,
                      color: Colors.white,
                      letterSpacing: 3.5
                  ),
                ),
              ),
              style: roundedButton.copyWith(
                backgroundColor: getMaterialColorProperty(brownColor1,),
              ),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
          ],
        )
      ],
    ),
  ),
  iconPath: 'assets/trash-bin.png',
);

/// Creates the widget for the failed recipe upload dialog
Widget createFailedUploadDialogWidget(BuildContext context) => createKukieDialogContent(
  content: Padding(
    padding: const EdgeInsets.fromLTRB(15, 20, 15, 15),
    child: Column(
      children: [
        Text(
          'YOUR UPLOAD HAS FAILED',
          textAlign: TextAlign.center,
          style: Theme.of(context).textTheme.subtitle1!.copyWith(
            color: greenColor1,
            letterSpacing: 2,
            height: calculateLineSpacing(20, 32),
            wordSpacing: calculateLineSpacing(20, 32),
          ),
        ),
        const SizedBox(height: 20,),
        Text(
          'Please try again!',
          textAlign: TextAlign.center,
          style: Theme.of(context).textTheme.bodyText1!.copyWith(
            color: darkGreyColor,
            letterSpacing: 3.5,
            height: calculateLineSpacing(16, 19),
            wordSpacing: calculateLineSpacing(16, 19),
          ),
        ),
        const SizedBox(height: 20,),
        ElevatedButton(
          style: roundedButton.copyWith(
            backgroundColor: getMaterialColorProperty(
              Theme.of(context).colorScheme.secondary
            )
          ),
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 15),
            child: Text(
              'OKAY',
              style: Theme.of(context).textTheme.bodyText1!.copyWith(
                color: Colors.white,
                letterSpacing: 3.5,
              ),
            ),
          ),
          onPressed: () {
            Navigator.pop(context);   // Pop this dialog
          },
        )
      ],
    ),
  ),
  iconPath: 'assets/breakfast.png',
);

/// Creates the widget for the finished recipe upload dialog
Widget createFinishedUploadDialogWidget(BuildContext context) =>
    createKukieDialogContent(
      content: Padding(
        padding: const EdgeInsets.fromLTRB(15, 20, 15, 15),
        child: Column(
          children: [
            Text(
              'YOUR RECIPE HAS BEEN SUCCESSFULLY UPLOADED!',
              textAlign: TextAlign.center,
              style: Theme.of(context).textTheme.subtitle1!.copyWith(
                color: greenColor1,
                letterSpacing: 2,
                height: calculateLineSpacing(20, 32),
                wordSpacing: calculateLineSpacing(20, 32),
              ),
            ),
            const SizedBox(height: 20,),
            Text(
              'Thank you for contributing to our community',
              textAlign: TextAlign.center,
              style: GoogleFonts.lato(
                textStyle: Theme.of(context).textTheme.bodyText1,
                fontWeight: FontWeight.normal,
                letterSpacing: 3.5,
                height: calculateLineSpacing(16, 19),
                wordSpacing: calculateLineSpacing(16, 19),
                color: darkGreyColor
              ),
            ),
            const SizedBox(height: 20,),
            ElevatedButton(
              style: roundedButton.copyWith(
                backgroundColor: getMaterialColorProperty(
                    Theme.of(context).colorScheme.secondary
                ),
              ),
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: Text(
                  'CLOSE',
                  style: GoogleFonts.lato(
                    color: Colors.white,
                    textStyle: Theme.of(context).textTheme.bodyText1,
                    fontWeight: FontWeight.normal,
                    letterSpacing: 3.5,
                    height: calculateLineSpacing(16, 19),
                  ),
                ),
              ),
              onPressed: () {
                Navigator.pop(context);   // Pop this dialog
              },
            )
          ],
        ),
      ),
      iconPath: 'assets/breakfast.png',
    );

/// Create the default widget boiler plate for the various dialogs in the app
Widget createKukieDialogContent({
  required Widget content,
  required String iconPath,
  double iconHeight = 128
}) => Wrap(
  children: [
    Column(
      children: [
        Stack(
          children: [
            SizedBox(
              height: iconHeight,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  const Expanded(
                    child: SizedBox(),
                  ),
                  Expanded(
                    child: Container(
                      decoration: BoxDecoration(
                        color: Colors.white,
                        border: Border.all(
                          color: Colors.white,
                          width: 0,
                        ),
                        borderRadius: const BorderRadius.only(
                          topLeft: Radius.circular(20.0),
                          topRight: Radius.circular(20.0)
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Center(
              child: Image.asset(
                iconPath,
                key: const Key('image'),
                fit: BoxFit.scaleDown,
                height: iconHeight,
              ),
            ),
          ],
        ),
        Container(
          decoration: BoxDecoration(
            color: Colors.white,
            border: Border.all(
              color: Colors.white,
              width: 0
            ),
            borderRadius: const BorderRadius.only(
              bottomLeft: Radius.circular(20.0),
              bottomRight: Radius.circular(20.0),
            ),
            // boxShadow: [
            //   BoxShadow(
            //     color: Color.fromRGBO(0, 0, 0, 0.4),
            //     spreadRadius: 0.1,
            //     blurRadius: 1,
            //   ),
            // ],
          ),
          child: Center(
            child: Padding(
              key: const Key('content'),
              padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
              child: content,
            ),
          ),
        ),
      ],
    ),
  ],
);

/// Create a widget tree for the screens after user authentication.
/// Screens create from this method will have the KUKIE banner on the top left
/// left of the screen.
Widget createDefaultAppBody({
  required BuildContext context,
  Widget? body,
  Widget? leading,
  Widget? trailing,
  Widget? bottomNavigationBar,
  Color backgroundColor = Colors.transparent,
  bool extendBodyBehindAppBar = false,
}) => Scaffold(
  backgroundColor: backgroundColor,
  extendBodyBehindAppBar: extendBodyBehindAppBar,
  bottomNavigationBar: bottomNavigationBar,
  appBar: PreferredSize(
    key: const Key('appBar'),
    preferredSize: Size.fromHeight(
      leading != null || trailing != null ? 98 : 68
    ),
    child: SafeArea(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            decoration: const BoxDecoration(
              color: brownColor2,
              borderRadius: BorderRadius.only(
                bottomRight: Radius.circular(25.0),
              ),
            ),
            child: Padding(
              padding: const EdgeInsets.fromLTRB(7, 0, 18, 5),
              child: Text(
                appTitle.toUpperCase(),
                textAlign: TextAlign.left,
                key: const Key('appName'),
                style: Theme.of(context).textTheme.headline3!.copyWith(
                  fontFamily: 'Kiona',
                  color: Colors.white,
                  letterSpacing: 0,
                  height: 1.2,
                  wordSpacing: 1.2,
                ),
              ),
            ),
          ),
          Row(
            children: [
              if (leading != null) leading,
              if (trailing != null) Expanded(
                child: Align(
                  alignment: Alignment.centerRight,
                  child: trailing,
              )),
            ],
          )
        ],
      ),
    ),
  ),
  body: body,
);

Widget createIconButtonForAppBar({
  Color iconColor = Colors.black,
  IconData? icon,
  Function()? onPressed,
}) => IconButton(
  icon: Icon(
    icon ?? Icons.arrow_back_ios,
    size: 30,
    color: iconColor,
  ),
  onPressed: onPressed,
);


Widget createImageWithPlaceholderWidget({
  required String placeholderPath,
  required String imgUrl,
  double imgSize = 94,
}) {

  Widget placeHolderImageWidget = Image.asset(
    placeholderPath,
    width: imgSize,
    fit: BoxFit.cover,
  );

  return Container(
    constraints: BoxConstraints(
      minHeight: imgSize,
      minWidth: imgSize,
      maxWidth: imgSize,
      maxHeight: imgSize,
    ),
    child: FutureBuilder(
      future: DefaultCacheManager().getSingleFile(imgUrl),
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          if (snapshot.hasError) {
            developer.log(
              'Unable to load image at address "$imgUrl"',
              error: snapshot.error,
              name: 'widget_prefabs.dart - createImageWithPlaceholderWidget()'
            );
            return placeHolderImageWidget;
          } else {
            File file = snapshot.data as File;
            return Image.file(
              file,
              fit: BoxFit.cover,
            );
          }
        } else {
          return const SpinKitRing(color: greenColor1);
        }
      },
    ),
  );
}

/// Creates the widget that contains an image inside
Widget createImageContainer({
  required Widget image,
  Color backgroundColor = Colors.white,
  double width = 162,
  double height = 162,
  EdgeInsets padding = const EdgeInsets.all(15),
  BorderRadius? borderRadius,
  List<BoxShadow> boxShadow = const [
    BoxShadow(
        color: Color.fromRGBO(0, 0, 0, 0.1),
        blurRadius: 7,
        spreadRadius: 0.1
    )
  ],
}) => Container(
  padding: padding,
  height: height,
  width: width,
  decoration: BoxDecoration(
      color: backgroundColor,
      boxShadow: boxShadow,
      borderRadius: borderRadius ?? BorderRadius.circular(30),
  ),
  child: image,
);
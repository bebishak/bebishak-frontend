class Boolean {
  bool value;

  Boolean({this.value = false});
}

class Integer {
  int value;

  Integer({this.value = 0});
}
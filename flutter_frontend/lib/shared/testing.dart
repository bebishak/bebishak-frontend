import 'package:flutter/material.dart';
import 'package:flutter_frontend/models/models.dart';

Widget createTestDialogWidget(BuildContext context, Widget content) => Scaffold(
  body: Center(
    child: ElevatedButton(
      child: const Text('Press Me'),
      onPressed: () {
        showDialog(
          context: context,
          builder: (context) => Dialog(
            elevation: 0,
            backgroundColor: Colors.transparent,
            child: content,
          ),
        );
      },
    ),
  ),
);

Widget makeWidgetTestable(Widget child) {
  return MaterialApp(
    home: child,
  );
}

class MockKukieUser extends KukieUser {
  MockKukieUser() : super(
      uid: '2301902296',
      username: 'EpicKukieUser12'
  );
}

Stream<KukieUser?> createDummyUserStream() async* {
  yield MockKukieUser();
}
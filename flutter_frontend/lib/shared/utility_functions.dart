import 'package:flutter/material.dart';
import 'package:flutter_frontend/screens/ingredients.dart';
import 'package:flutter_frontend/screens/processing.dart';
import 'package:flutter_frontend/services/image_picker.dart';
import 'package:flutter_frontend/shared/constants.dart';
import 'package:flutter_frontend/shared/widget_prefabs.dart';
import 'package:image_picker/image_picker.dart';

MaterialStateProperty<Color> getMaterialColorProperty(Color color) =>
    MaterialStateProperty.resolveWith<Color>(
        (Set<MaterialState> states) {
      return color;
    }
);

MaterialStateProperty<EdgeInsetsGeometry> getMaterialEdgeInsetsProperty(EdgeInsetsGeometry edgeInsets) =>
    MaterialStateProperty.resolveWith<EdgeInsetsGeometry>(
        (Set<MaterialState> states) {
      return edgeInsets;
    }
);

/// Calculates the line spacing value from adobe XD
double calculateLineSpacing(double fontSize, double rawUnitValue)
    => (fontSize * rawUnitValue / 1000) + 1;

/// Function to convert String? to String by changing the String to empty
/// String if it is null.
String makeNullStringEmpty(String? str) => str ?? '';

/// Converts index to the corresponding SortBy enum
SortBy convertSortByIndexToEnum(int index) {
  switch (index) {
    case 0:
      return SortBy.relevance;
    case 1:
      return SortBy.rating;
    case 2:
      return SortBy.uploadDate;
    default:
      return SortBy.relevance;
  }
}

/// Converts SortBy enum to the corresponding index
int convertSortByEnumToIndex(SortBy sortBy) {
  switch (sortBy) {
    case SortBy.relevance:
      return 0;
    case SortBy.rating:
      return 1;
    case SortBy.uploadDate:
      return 2;
    default:
      return 0;
  }
}

/// Converts index to the corresponding UploadDate enum
UploadDate convertUploadDateIndexToEnum(int index) {
  switch (index) {
    case 0:
      return UploadDate.anytime;
    case 1:
      return UploadDate.thisWeek;
    case 2:
      return UploadDate.thisMonth;
    case 3:
      return UploadDate.thisYear;
    default:
      return UploadDate.anytime;
  }
}

/// Converts UploadDate enum to the corresponding index
int convertUploadDateEnumToIndex(UploadDate uploadDate) {
  switch (uploadDate) {
    case UploadDate.anytime:
      return 0;
    case UploadDate.thisWeek:
      return 1;
    case UploadDate.thisMonth:
      return 2;
    case UploadDate.thisYear:
      return 3;
    default:
      return 0;
  }
}

/// Converts index to the corresponding CookingDuration enum
CookingDuration convertCookingDurationIndexToEnum(int index) {
  switch (index) {
    case 0:
      return CookingDuration.any;
    case 1:
      return CookingDuration.short;
    case 2:
      return CookingDuration.medium;
    case 3:
      return CookingDuration.long;
    default:
      return CookingDuration.any;
  }
}

/// Converts CookingDuration enum to the corresponding index
int convertCookingDurationEnumToIndex(CookingDuration cookingDuration) {
  switch (cookingDuration) {
    case CookingDuration.any:
      return 0;
    case CookingDuration.short:
      return 1;
    case CookingDuration.medium:
      return 2;
    case CookingDuration.long:
      return 3;
    default:
      return 0;
  }
}

/// Convert a double into a String, removing trailing zeroes
String convertDoubleToString(double num) {
  RegExp regex = RegExp(r"([.]*0)(?!.*\d)");

  return num.toString().replaceAll(regex, "");
}

// /// Function to get the Azure AD B2C Access Key (token) from StreamProvider
// /// [context] A BuildContext instance
// String? getAccessKeyToken(BuildContext? context) {
//   if (context != null) {
//     try {
//       return context.watch<String?>();
//     } catch(e){
//       developer.log(
//         'Error getting access token: ${e.toString()}',
//         name: 'utility_functions.dart - getAccessKeyToken()',
//       );
//     }
//   } else {
//     return null;
//   }
// }

Future performTakePhotoOperation(BuildContext context) async {
  XFile? img = await takePhoto();
  if (img != null) {
    var result = await Navigator.push(
      context,
      createFadingPageRouteBuilder(child: ProcessingScreen(
        key: const Key('processingScreen'),
        imageFile: img,
      ),),
    );
    final List<String> detectedIng = [...result];

    Navigator.push(
      context,
      createFadingPageRouteBuilder(child: IngredientsScreen(
        key: const Key('ingredientsScreen'),
        detectedIng: detectedIng,
      ),),
    );
  }
}
import 'dart:math';

import 'package:flutter/material.dart';

class CirclePainter extends CustomPainter {

  final Color color;
  final double paintWidth;
  final double strokeWidth;
  late Paint trackPaint;

  CirclePainter({
    this.color = Colors.blue,
    this.paintWidth = 50,
    this.strokeWidth = 7
  }) {
    trackPaint = Paint()
      ..color = color
      ..style = PaintingStyle.stroke
      ..strokeWidth = strokeWidth
      ..strokeCap = StrokeCap.square;
  }

  @override
  void paint(Canvas canvas, Size size) {

    final radius = (min(size.width, size.height) - paintWidth) / 2;
    final center = Offset(size.width / 2, size.height / 2);

    canvas.drawCircle(center, radius, trackPaint);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) => true;

}
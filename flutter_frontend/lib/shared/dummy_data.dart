import 'package:flutter_frontend/models/models.dart';

List<Recipe> dummyRecipes = [
  // Recipe(
  //   name: 'Ice Cream',
  //   username: 'monkaS',
  //   imgUrl: 'https://upload.wikimedia.org/wikipedia/commons/thumb/2/2e/Ice_cream_with_whipped_cream%2C_chocolate_syrup%2C_and_a_wafer_%28cropped%29.jpg/1200px-Ice_cream_with_whipped_cream%2C_chocolate_syrup%2C_and_a_wafer_%28cropped%29.jpg',
  //   desc: 'twelve times did the cheese move sideways to Switzerland by radio some garble woohoo wologigngo',
  //   isLiked: false,
  //   rateCount: 20,
  //   globalRating: 4,
  //   duration: 10,
  //   isVegan: true,
  //   mealTags: [
  //     "Breakfast",
  //     "lunch"
  //   ],
  //   ingredients: [
  //     Ingredient(
  //         name: 'Apple',
  //         unit: 'pcs',
  //         amount: 12
  //     ),
  //     Ingredient(
  //         name: 'Water',
  //         unit: 'L',
  //         amount: 1.2
  //     ),
  //     Ingredient(
  //         name: 'souls of your enemies',
  //         unit: 'kg',
  //         amount: 5
  //     )
  //   ],
  //   steps: [
  //     'This is the procedure of the recipe in which was proceed with the procedures',
  //     'This is the procedure of the recipe in ',
  //     'This is the procedure of the recipe in  the procedures',
  //     'Short step hehe',
  //   ],
  // ),
  // Recipe(
  //   name: 'Lasagna',
  //   username: 'GiantSweetroll',
  //   imgUrl: 'https://firebasestorage.googleapis.com/v0/b/kukie-d1b18.appspot.com/o/vIMhf7svExhAke1LEJw4m0uyDAO2-Ice%20Cream.jpg?alt=media&token=bada03cb-fd41-4010-a63a-a511af382682',
  //   desc: 'four times did the cheese move upwards to Switzerland by radio some garble woohoo wologigngo',
  //   isLiked: false,
  //   rateCount: 10,
  //   globalRating: 3,
  //   duration: 5,
  //   ingredients: [
  //     Ingredient(
  //         name: 'Banana',
  //         unit: 'pcs',
  //         amount: 12
  //     ),
  //     Ingredient(
  //         name: 'Cheese',
  //         unit: 'L',
  //         amount: 1.2
  //     ),
  //     Ingredient(
  //         name: 'Rice',
  //         unit: 'kg',
  //         amount: 5
  //     )
  //   ],
  //   steps: [
  //     'This is the procedure of the recipe in which was proceed with the procedures',
  //     'This is the procedure of the recipe in ',
  //     'This is the procedure of the recipe in  the procedures',
  //     'Short step hehe',
  //   ],
  // ),
  // Recipe(
  //   name: 'Strawberry Milkshake',
  //   username: 'ArnoldDune',
  //   imgUrl: 'https://firebasestorage.googleapis.com/v0/b/kukie-d1b18.appspot.com/o/vIMhf7svExhAke1LEJw4m0uyDAO2-Ice%20Cream.jpg?alt=media&token=bada03cb-fd41-4010-a63a-a511af382682',
  //   desc: 'Eleven times did the cheese move downwards to Switzerland by radio some garble woohoo wologigngo',
  //   isLiked: false,
  //   rateCount: 20,
  //   globalRating: 2,
  //   duration: 5,
  //   ingredients: [
  //     Ingredient(
  //         name: 'Strawberries',
  //         unit: 'pcs',
  //         amount: 4
  //     ),
  //     Ingredient(
  //         name: 'Milk',
  //         unit: 'L',
  //         amount: 4.5
  //     ),
  //     Ingredient(
  //         name: 'Olives',
  //         unit: 'kg',
  //         amount: 10
  //     )
  //   ],
  //   steps: [
  //     'This is the procedure of the recipe in which was proceed with the procedures',
  //     'This is the procedure of the recipe in ',
  //     'This is the procedure of the recipe in  the procedures',
  //     'Short step hehe',
  //   ],
  // ),
  Recipe(
    name: 'Chicken Nasi Goreng',
    username: 'bebishak',
    authorUid: 'bebishakuid',
    imgUrl: 'https://images.aws.nestle.recipes/resized/f736de146c36dd5ffc9a51216272c18d_Nasgor_Woku_944_744_419.jpg',
    desc: 'Fried rice is a dish of cooked rice that has been stir-fried in a wok or a frying pan and is usually mixed with other ingredients such as eggs, vegetables, seafood, or meat. ',
    isLiked: false,
    rateCount: 20,
    globalRating: 4,
    duration: 20,
    mealTags: [],
    cuisineTags: ['Indonesian'],
    isVegan: false,
    ingredients: [
      Ingredient(
          name: 'Sesame oil',
          unit: 'tbsp.',
          amount: 3
      ),
      Ingredient(
          name: 'Carrot',
          unit: 'Pcs',
          amount: 1
      ),
      Ingredient(
          name: 'Green onions',
          unit: 'Pcs',
          amount: 2
      ),
      Ingredient(
          name: 'Egg',
          unit: 'Pcs',
          amount: 1
      ),
      Ingredient(
          name: 'Cloves garlic',
          unit: 'Pcs',
          amount: 2
      ),
      Ingredient(
          name: 'Cooked rice',
          unit: 'Pcs',
          amount: 2
      ),
      Ingredient(
          name: 'Sweet soy sauce',
          unit: 'tbsp.',
          amount: 3
      ),
    ],
    steps: [
      'Heat a large cast iron skillet over high heat until very hot, about 2 minutes. Add 1 tablespoon oil.',
      'Beat egg with 2 teaspoons water and a large pinch salt and add to skillet. Cook, stirring to form large soft curds, about 30 seconds. Transfer to a plate. ',
      'Return skillet to high heat and add 2 tablespoons oil, the carrots, and whites of the green onions. Cook until lightly golden, about 2 minutes. Add garlic and ginger and cook, stirring, until fragrant, 1 minute.',
      'Add rice, peas, and cooked eggs to skillet. Pour in soy sauce and cook, stirring until heated through, 1 minute. Season with salt and pepper and stir in the remaining green onions.',
    ],
  ),
  Recipe(
    name: 'Kimchi Fried Rice',
    username: 'GogleGuy',
    authorUid: 'GogleGuyuid',
    imgUrl: 'https://omnivorescookbook.com/wp-content/uploads/2020/08/200806_Kimchi-Fried-Rice_550.jpg',
    desc: 'Kimchi fried rice or kimchi-bokkeum-bap is a variety of bokkeum-bap, a popular dish in South Korea. ',
    isLiked: false,
    rateCount: 20,
    globalRating: 4,
    duration: 10,
    mealTags: [
      'Breakfast',
      'Lunch',
      'Dinner'
    ],
    cuisineTags: ['Korean'],
    isVegan: false,
    ingredients: [],
    steps: [],
  ),
  Recipe(
    name: 'Paella Vedura',
    username: 'MonicaBeluci',
    authorUid: 'MonicaBeluciuid',
    imgUrl: 'https://data.thefeedfeed.com/recommended/post_938652.jpg',
    desc: 'a Spanish dish of rice, saffron, chicken, seafood, etc., cooked and served in a large shallow pan.',
    isLiked: false,
    rateCount: 20,
    globalRating: 3,
    duration: 10,
    mealTags: [
      'Breakfast',
      'Lunch',
      'Dinner'
    ],
    cuisineTags: ['Korean'],
    isVegan: false,
    ingredients: [],
    steps: [],
  ),
  Recipe(
    name: 'French Onion Rice',
    username: 'MrBaguette',
    authorUid: 'MrBaguetteuid',
    imgUrl: 'https://nishkitchen.com/wp-content/uploads/2020/04/French-Onion-Rice-1B-480x480.jpg',
    desc: 'A dinner star featuring fluffy rice interlaced with crispy caramelized onions.',
    isLiked: false,
    rateCount: 20,
    globalRating: 4,
    duration: 10,
    mealTags: [
      'Breakfast',
      'Lunch',
      'Dinner'
    ],
    cuisineTags: ['Korean'],
    isVegan: false,
    ingredients: [],
    steps: [],
  ),
  Recipe(
    name: 'Chicken Rice Bowl',
    username: 'mybowl',
    authorUid: 'mybowluid',
    imgUrl: 'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/delish-190522-cilantro-lime-chicken-and-rice-bowl-portrait-046-pf-1-1559073830.jpg',
    desc: 'A bowl dish consisting of an assortment of vegetables and chicken served over a bed of rice.',
    isLiked: false,
    rateCount: 20,
    globalRating: 4,
    duration: 10,
    mealTags: [
      'Breakfast',
      'Lunch',
      'Dinner'
    ],
    cuisineTags: ['Korean'],
    isVegan: false,
    ingredients: [],
    steps: [],
  ),
];

List<Recipe> dummyRecipes2 = [
  Recipe(
    name: 'Ice Cream',
    username: 'monkaS',
    authorUid: 'monkaSuid',
    imgUrl: 'https://upload.wikimedia.org/wikipedia/commons/thumb/2/2e/Ice_cream_with_whipped_cream%2C_chocolate_syrup%2C_and_a_wafer_%28cropped%29.jpg/1200px-Ice_cream_with_whipped_cream%2C_chocolate_syrup%2C_and_a_wafer_%28cropped%29.jpg',
    desc: 'A tasty ice cream to fulfill your day',
    isLiked: false,
    rateCount: 20,
    globalRating: 5,
    duration: 10,
    isVegan: true,
    mealTags: [
      "Breakfast",
      "lunch"
    ],
    ingredients: [
      Ingredient(
          name: 'Apple',
          unit: 'pcs',
          amount: 12
      ),
      Ingredient(
          name: 'Water',
          unit: 'L',
          amount: 1.2
      ),
      Ingredient(
          name: 'souls of your enemies',
          unit: 'kg',
          amount: 5
      )
    ],
    steps: [
      'This is the procedure of the recipe in which was proceed with the procedures',
      'This is the procedure of the recipe in ',
      'This is the procedure of the recipe in  the procedures',
      'Short step hehe',
    ],
  ),
  Recipe(
    name: 'Lasagna',
    username: 'GiantSweetroll',
    authorUid: 'GiantSweetrolluid',
    imgUrl: 'https://d12man5gwydfvl.cloudfront.net/wp-content/uploads/2017/06/Mau-Bisa-Bikin-Lasagna-Ini-Dia-Resep-Lasagna-Sederhana-Yang-Enak-Banget-dan-Bikin-Kamu-Ketagihan-Masak-Dirumah-940x667.jpg',
    desc: 'Garfield\'s favorite food specially made for you',
    isLiked: false,
    rateCount: 10,
    globalRating: 4,
    duration: 5,
    ingredients: [
      Ingredient(
          name: 'Banana',
          unit: 'pcs',
          amount: 12
      ),
      Ingredient(
          name: 'Cheese',
          unit: 'L',
          amount: 1.2
      ),
      Ingredient(
          name: 'Rice',
          unit: 'kg',
          amount: 5
      )
    ],
    steps: [
      'This is the procedure of the recipe in which was proceed with the procedures',
      'This is the procedure of the recipe in ',
      'This is the procedure of the recipe in  the procedures',
      'Short step hehe',
    ],
  ),
  Recipe(
    name: 'Strawberry Milkshake',
    username: 'ArnoldDune',
    authorUid: 'ArnoldDuneuid',
    imgUrl: 'https://www.unicornsinthekitchen.com/wp-content/uploads/2018/08/Strawberry-Milkshake-square.jpg',
    desc: 'Sweet drink for your lovely',
    isLiked: false,
    rateCount: 20,
    globalRating: 2,
    duration: 5,
    ingredients: [
      Ingredient(
          name: 'Strawberries',
          unit: 'pcs',
          amount: 4
      ),
      Ingredient(
          name: 'Milk',
          unit: 'L',
          amount: 4.5
      ),
      Ingredient(
          name: 'Olives',
          unit: 'kg',
          amount: 10
      )
    ],
    steps: [
      'This is the procedure of the recipe in which was proceed with the procedures',
      'This is the procedure of the recipe in ',
      'This is the procedure of the recipe in  the procedures',
      'Short step hehe',
    ],
  ),
];
import 'package:flutter/material.dart';
import 'package:flutter_frontend/shared/constants.dart';
import 'package:flutter_frontend/shared/utility_functions.dart';
import 'package:google_fonts/google_fonts.dart';

class CustomTheme {
  static ThemeData get lightTheme => ThemeData(
    scaffoldBackgroundColor: greenColor3,
    colorScheme: ColorScheme.fromSwatch().copyWith(
      primary: primaryButtonColor,
      secondary: secondaryButtonColor,
    ),
    inputDecorationTheme: InputDecorationTheme(
      errorMaxLines: 10,
      errorStyle: GoogleFonts.lato(
        fontWeight: FontWeight.normal,
        fontSize: 16,
        letterSpacing: 3.5,
        height: calculateLineSpacing(16, 19),
        wordSpacing: calculateLineSpacing(16, 19),
      ),
    ),
    textTheme: TextTheme(
      // Note: All fontSize are measured in pixels, not dp.
      // (It's in ldpi I think)
      headline1: const TextStyle(
        fontFamily: 'Kiona',
        fontSize: 102,
        fontWeight: FontWeight.normal,
        letterSpacing: -1.5,
      ),
      headline2: const TextStyle(
        fontFamily: 'Kiona',
        fontSize: 35,
        fontWeight: FontWeight.normal,
        letterSpacing: -0.5,
      ),
      headline3: TextStyle(
        fontFamily: 'LeagueSpartan',
        fontSize: 33,
        fontWeight: FontWeight.bold,
        letterSpacing: 3.3,
        height: calculateLineSpacing(33, 47),
        wordSpacing: calculateLineSpacing(33, 47),
      ),
      headline4: const TextStyle(
        fontFamily: 'LeagueSpartan',
        fontSize: 30,
        fontWeight: FontWeight.bold,
        letterSpacing: 0.25,
      ),
      headline5: const TextStyle(
        fontFamily: 'LeagueSpartan',
        fontSize: 25,
        fontWeight: FontWeight.bold,
        letterSpacing: 0,
      ),
      headline6: const TextStyle(
        fontFamily: 'LeagueSpartan',
        fontSize: 22,
        fontWeight: FontWeight.bold,
        letterSpacing: 0.15,
      ),
      subtitle1: TextStyle(
        fontFamily: 'LeagueSpartan',
        fontSize: 20,
        fontWeight: FontWeight.bold,
        letterSpacing: 2,
        wordSpacing: calculateLineSpacing(20, 32),
        height: calculateLineSpacing(20, 32),
      ),
      subtitle2: TextStyle(
        fontFamily: 'LeagueSpartan',
        fontSize: 19,
        fontWeight: FontWeight.bold,
        letterSpacing: 4.16,
        wordSpacing: calculateLineSpacing(19, 23),
        height: calculateLineSpacing(19, 23),
      ),
      bodyText1: GoogleFonts.lato(
        fontWeight: FontWeight.normal,
        fontSize: 16,
        letterSpacing: 3.5,
        height: calculateLineSpacing(16, 19),
        wordSpacing: calculateLineSpacing(16, 19),
      ),
      bodyText2: GoogleFonts.lato(
        fontWeight: FontWeight.normal,
        fontSize: 14,
        letterSpacing: 0.25,
      ),
      button: GoogleFonts.lato(
        fontWeight: FontWeight.bold,
        fontSize: 14,
        letterSpacing: 1.25,
      ),
      caption: GoogleFonts.lato(
        fontWeight: FontWeight.normal,
        fontSize: 13,
        letterSpacing: 2.85,
        height: calculateLineSpacing(13, 16),
        wordSpacing: calculateLineSpacing(13, 16),
      ),
      overline: GoogleFonts.lato(
        fontWeight: FontWeight.normal,
        fontSize: 11,
        letterSpacing: 1.5,
      ),
    ),
  );
}
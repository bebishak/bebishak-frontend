import 'package:flutter/material.dart';
import 'package:flutter_frontend/models/models.dart';
import 'package:flutter_frontend/screens/recipe_detail.dart';
import 'package:flutter_frontend/screens/recipe_form.dart';
import 'package:flutter_frontend/services/backend.dart';
import 'package:flutter_frontend/shared/constants.dart';
import 'package:flutter_frontend/shared/utility_functions.dart';
import 'package:flutter_frontend/shared/widget_prefabs.dart';
import 'package:flutter_frontend/widgets/auth_enforcer.dart';
import 'package:flutter_frontend/widgets/kukie_bottom_nav_bar.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';

import 'dart:developer' as developer;

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {

  /// Controls the size of the recipe cards
  final Size recipeCardSize = const Size(177, 242);
  Size sizeFlexibleSpaceBar = const Size(0, 0);

  Widget _createRecipeCard(Recipe? recipe, {
    Color backgroundColor = greenColor1,
    bool tapToAdd = false,
    Key? key,
  }) {
    if (!tapToAdd && recipe == null) {
      throw Exception("recipe cannot be null if tapToAdd is false.");
    }
    return InkWell(
      key: key,
      onTap: () {
        if (tapToAdd) {
          Navigator.push(
              context,
              createFadingPageRouteBuilder(child: const AuthEnforcer(child: RecipeFormScreen()))
          );
        } else {
          if (recipe != null) {
            Navigator.push(
              context,
              createFadingPageRouteBuilder(child: RecipeScreen(recipe: recipe))
            );
          }
        }
      },
      child: Container(
        padding: const EdgeInsets.fromLTRB(
          20,
          10,
          20,
          25,
        ),
        margin: const EdgeInsets.symmetric(
            vertical: 10
        ),
        constraints: tapToAdd ? BoxConstraints(
          minHeight: recipeCardSize.height
        ) : null,
        decoration: BoxDecoration(
            color: backgroundColor,
            borderRadius: BorderRadius.circular(25),
            boxShadow: const [
              BoxShadow(
                color: Color.fromRGBO(0, 0, 0, 0.3),
                blurRadius: 3,
                spreadRadius: 2,
                offset: Offset(0, 2),
              ),
            ]
        ),
        child: tapToAdd ? const Center(
          child: Icon(
            Icons.add_circle_outline,
            color: greenColor1,
            size: 45,
          ),
        ) : Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            createImageWithPlaceholderWidget(
              placeholderPath: 'assets/dish.png',
              imgUrl: recipe!.imgUrl ?? '',
              imgSize: 75,
            ),
            Text(
              recipe.name.toUpperCase(),
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
              textAlign: TextAlign.center,
              style: GoogleFonts.lato(
                textStyle: Theme.of(context).textTheme.bodyText1,
                fontWeight: FontWeight.bold,
                letterSpacing: 1.6,
                height: calculateLineSpacing(16, 32),
                wordSpacing: calculateLineSpacing(16, 32),
                color: Colors.white,
              ),
            ),
            RatingBar.builder(
              initialRating: recipe.globalRating,
              ignoreGestures: true,
              itemCount: 5,
              itemSize: 24,
              itemBuilder: (context, _) => const Icon(
                Icons.star_rounded,
                color: Colors.amber,
              ),
              onRatingUpdate: (_) {},
            ),
            Text(
              recipe.desc,
              maxLines: 4,
              overflow: TextOverflow.ellipsis,
              textAlign: TextAlign.center,
              style: Theme.of(context).textTheme.caption!.copyWith(
                color: Colors.white,
                letterSpacing: 0,
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _createRecipeSectionTitle(
    String title, {
    Key? key
  }) => Padding(
    padding: const EdgeInsets.symmetric(horizontal: 20),
    child: Text(
      title,
      key: key,
      overflow: TextOverflow.ellipsis,
      style: Theme.of(context).textTheme.headline6!.copyWith(
        color: brownColor1,
        letterSpacing: 2.2,
        height: calculateLineSpacing(22, 32),
        wordSpacing: calculateLineSpacing(22, 32),
      ),
    ),
  );

  Widget _createRecipesWidget({
    List<Recipe>? trendingRecipes,
    List<Recipe>? likedRecipes,
    List<Recipe>? myRecipes
  }) => Column(
    crossAxisAlignment: CrossAxisAlignment.stretch,
    children: [
      const Center(
        child: SizedBox(
          width: 95,
          child: Divider(
            thickness: 5,
            color: greyColor,
          ),
        ),
      ),
      const SizedBox(height: 50,),
      _createRecipeSectionTitle(
        'TRENDING RECIPES',
        key: const Key('trendingRecipes')
      ),
      const SizedBox(height: 5,),
      _createRecipeRowListWidget(
        recipes: trendingRecipes,
        messageWhenEmpty: 'There are no trending recipes at the moment...'
      ),
      const SizedBox(height: 40,),
      _createRecipeSectionTitle(
        'FAVORITE RECIPES',
        key: const Key('favoriteRecipes')
      ),
      const SizedBox(height: 5,),
      _createRecipeRowListWidget(
        recipes: likedRecipes,
        tileBgColor: brownColor2,
        messageWhenEmpty: 'You have not liked any recipes'
      ),
      const SizedBox(height: 40,),
      _createRecipeSectionTitle(
        'YOUR RECIPES',
        key: const Key('yourRecipes')
      ),
      const SizedBox(height: 5,),
      _createRecipeRowListWidget(
        recipes: myRecipes,
        tileBgColor: blueColor,
        messageWhenEmpty: '',
        tapToAdd: true
      ),
    ],
  );

  Widget _createRecipeRowListWidget({
    required List<Recipe>? recipes,
    Color tileBgColor = greenColor1,
    required String messageWhenEmpty,
    bool tapToAdd = false,
  }) {
    return SizedBox(
      width: recipeCardSize.width,
//        height: trendingRecipes != null && trendingRecipes.isNotEmpty ?
//          recipeCardSize.height : null,
      child: recipes == null ? Container(
        constraints: BoxConstraints(
          minHeight: recipeCardSize.height
        ),
        child: const Center(
          child: SpinKitRing(color: greenColor1),
        ),
      ) : recipes.isNotEmpty || tapToAdd ? SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: IntrinsicHeight(
          child: Row(
            // If tapToAdd is true, last index of recipe+1 will be used for add
            // recipe card, so add the index by 1
            children: List<Widget>.generate(!tapToAdd ? recipes.length : recipes.length + 1, (index) {
              if (tapToAdd && index == recipes.length) {
                return SizedBox(
                  width: recipeCardSize.width,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 5),
                    child: _createRecipeCard(
                      null,
                      key: const Key('addRecipeCard'),
                      backgroundColor: greyColor,
                      tapToAdd: true,
                    ),
                  ),
                );
              } else {
                return Container(
                  padding: const EdgeInsets.symmetric(horizontal: 5),
                  width: recipeCardSize.width,
                  child: _createRecipeCard(
                    recipes[index],
                    backgroundColor: tileBgColor
                  ),
                );
              }
            }),
          ),
        ),
      ) : Padding(
        padding: const EdgeInsets.symmetric(
            horizontal: 15
        ),
        child: Text(
          messageWhenEmpty,
          textAlign: TextAlign.center,
          style: Theme.of(context).textTheme.bodyText1!.copyWith(
            color: darkGreyColor,
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    KukieUser user = context.read<KukieUser?>()!;
    return createDefaultAppBody(
      context: context,
      backgroundColor: blueColor,
      bottomNavigationBar: const KukieBottomNavigationBar(),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10),
        child: NestedScrollView(
          headerSliverBuilder: (context, innerBoxIsScrolled) => [
            SliverOverlapAbsorber(
              handle: NestedScrollView.sliverOverlapAbsorberHandleFor(context),
              sliver: DynamicSliverAppBar(
                maxHeight: 300,
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 35),
                  child: _PhotoSection(),
                ),
              ),
            )
          ],
          body: Builder(
            builder: (context) {
              return CustomScrollView(
                slivers: [
                  SliverOverlapInjector(
                    handle: NestedScrollView.sliverOverlapAbsorberHandleFor(context),
                  ),
                  SliverList(
                    delegate: SliverChildListDelegate([
                      Container(
                        decoration: const BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(45),
                            topRight: Radius.circular(45)
                          ),
                          boxShadow: [
                            BoxShadow(
                              color: Color.fromRGBO(0, 0, 0, 0.15),
                              blurRadius: 5,
                              spreadRadius: 1,
                            ),
                          ],
                        ),
                        padding: const EdgeInsets.symmetric(
                          vertical: 10,
                          horizontal: 5,
                        ),
                        margin: const EdgeInsets.symmetric(horizontal: 5),
                        child: FutureBuilder(
                          future: Future.wait([
                            // Trending recipes
                            getTrendingRecipes(userUid: user.uid),
                            // Favorite recipes
                            getRecipes(userUid: user.uid, isLiked: true),
                            // Get the user's recipes
                            getRecipes(
                              userUid: user.uid
                            )
                          ]),
                          builder: (context, AsyncSnapshot<List<dynamic>> snapshot) {
                            List<Recipe>? trendingRecipes;
                            List<Recipe>? likedRecipes;
                            List<Recipe>? myRecipes;
                            if (snapshot.connectionState == ConnectionState.done) {
                              if (!snapshot.hasData || snapshot.hasError || snapshot.data == null) {
                                developer.log(
                                  'Unable to process recipes',
                                  error: snapshot.error,
                                  name: 'home.dart'
                                );
                                trendingRecipes = [];
                                likedRecipes = [];
                                myRecipes = [];
                              } else {
                                trendingRecipes = snapshot.data![0] as List<Recipe>;
                                likedRecipes = snapshot.data![1] as List<Recipe>;
                                myRecipes = snapshot.data![2] as List<Recipe>;
                              }
                            }

                            return _createRecipesWidget(
                              myRecipes: myRecipes,
                              trendingRecipes: trendingRecipes,
                              likedRecipes: likedRecipes
                            );
                          },
                        ),
                      ),
                    ]),
                  ),
                ]
              );
            }
          ),
        ),
      ),
    );
  }
}

class _PhotoSection extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Text(
          'TAKE A PHOTO!',
          key: const Key('takeAPhoto'),
          textAlign: TextAlign.center,
          style: Theme.of(context).textTheme.subtitle2!.copyWith(
            color: Colors.white,
          ),
        ),
        const SizedBox(height: 3,),
        Container(
          decoration: const BoxDecoration(
              shape: BoxShape.circle,
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                  blurRadius: 2,
                  spreadRadius: 2,
                  color: Color.fromRGBO(0, 0, 0, 0.15),
                  offset: Offset(0, 2),
                )
              ]
          ),
          child: SizedBox(
            width: 68*1.5,
            height: 68*1.5,
            child: IconButton(
              key: const Key('photoButton'),
              padding: const EdgeInsets.all(0.0),
              icon: const Icon(
                Icons.camera_alt_outlined,
                color: Color.fromRGBO(0, 0, 0, 0.42),
                size: 68,
              ),
              onPressed: () async {
                await performTakePhotoOperation(context);
              },
            ),
          ),
        ),
        const SizedBox(height: 8,),
        Text(
          'LET\'S SEE WHAT YOU CAN MAKE WITH THE THINGS THAT YOU HAVE',
          key: const Key('subtitle'),
          textAlign: TextAlign.center,
          style: Theme.of(context).textTheme.caption!.copyWith(
            color: greenColor1,
          ),
        ),
        const SizedBox(height: 20,),
        Row(
          children: [
            Image.asset(
              'assets/diet.png',
              key: const Key('meal1'),
              height: 70,
              fit: BoxFit.scaleDown,
            ),
            Expanded(
              child: Align(
                alignment: Alignment.bottomRight,
                child: Image.asset(
                  'assets/meals.png',
                  key: const Key('meal2'),
                  height: 70,
                  fit: BoxFit.scaleDown,
                ),
              ),
            )
          ],
        )
      ],
    );
  }
}

/// Adapted from https://github.com/flutter/flutter/issues/18345
class DynamicSliverAppBar extends StatefulWidget {
  final Widget child;
  final double maxHeight;

  const DynamicSliverAppBar({
    required this.child,
    required this.maxHeight,
    Key? key,
  }) : super(key: key);

  @override
  _DynamicSliverAppBarState createState() => _DynamicSliverAppBarState();
}

class _DynamicSliverAppBarState extends State<DynamicSliverAppBar> {
  final GlobalKey _childKey = GlobalKey();
  bool isHeightCalculated = false;
  double height = 0;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance?.addPostFrameCallback((timeStamp) {
      if (!isHeightCalculated) {
        isHeightCalculated = true;
        setState(() {
          height = (_childKey.currentContext?.findRenderObject() as RenderBox)
              .size
              .height;
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return SliverAppBar(
      automaticallyImplyLeading: false,
      backgroundColor: Colors.transparent,
      expandedHeight: isHeightCalculated ? height : widget.maxHeight,
      flexibleSpace: FlexibleSpaceBar(
        background: Column(
          children: [
            Container(
              key: _childKey,
              child: widget.child,
            ),
            const Expanded(child: SizedBox.shrink()),
          ],
        ),
      ),
    );
  }
}
import 'package:flutter/material.dart';
import 'package:flutter_frontend/shared/widget_prefabs.dart';

class BufferScreen extends StatelessWidget {

  final Widget child;
  final Image bottomImage;

  const BufferScreen({
    Key? key,
    required this.child,
    required this.bottomImage,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: createDefaultAppBody(
        extendBodyBehindAppBar: true,
        context: context,
        body: Padding(
          padding: const EdgeInsets.fromLTRB(15, 0, 15, 10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                flex: 6,
                child: Container(
                  color: Colors.transparent,
                  child: Container(
                    decoration: const BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(40.0),
                        bottomRight: Radius.circular(40.0),
                      ),
                      boxShadow: [
                        BoxShadow(
                          color: Color.fromRGBO(0, 0, 0, 0.4),
                          spreadRadius: 5,
                          blurRadius: 7,
                          offset: Offset(0, 5), // changes position of shadow
                        ),
                      ],
                    ),
                    child: child,
                  ),
                ),
              ),
              Expanded(
                flex: 2,
                child: bottomImage,
              ),
            ],
          ),
        ),
      ),
    );
  }
}

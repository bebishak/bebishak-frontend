import 'package:flutter/material.dart';
import 'package:flutter_frontend/screens/about.dart';
import 'package:flutter_frontend/services/auth.dart';
import 'package:flutter_frontend/shared/constants.dart';
import 'package:flutter_frontend/widgets/auth_enforcer.dart';
import 'package:flutter_frontend/shared/utility_functions.dart';
import 'package:flutter_frontend/shared/widget_prefabs.dart';
import 'package:google_fonts/google_fonts.dart';

class SettingsScreen extends StatefulWidget {
  const SettingsScreen({Key? key}) : super(key: key);

  @override
  _SettingsScreenState createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {

  String langChoice = 'English';
  bool enableNotification = false;
  bool enableMissingIng = false;

  /// Create the tile widget for this screen
  Widget createTile({
    String title = '',
    String subtitle = '',
    Color? backgroundColor,
    Widget? trailing
  }) => Container(
    decoration: BoxDecoration(
      color: backgroundColor ?? greenColor2,
      boxShadow: const [
        BoxShadow(
          blurRadius: 8,
          spreadRadius: -2,
          color: Color.fromRGBO(0, 0, 0, 0.5),
          offset: Offset(0, 4)
        ),
      ],
      borderRadius: BorderRadius.circular(20.0)
    ),
    child: ListTile(
      title: Padding(
        padding: EdgeInsets.fromLTRB(0, subtitle.isNotEmpty ? 10:0, 0, 0),
        child: Text(
          title,
          style: Theme.of(context).textTheme.bodyText1!.copyWith(
            color: Colors.white,
            letterSpacing: 3.5,
            height: calculateLineSpacing(16, 19),
            wordSpacing: calculateLineSpacing(16, 19),
          ),
        ),
      ),
      subtitle: subtitle != '' ? Padding(
        padding: const EdgeInsets.fromLTRB(20, 0, 5, 7),
        child: Text(
          subtitle,
          style: GoogleFonts.lato(
            color: greyColor,
            fontWeight: FontWeight.normal,
            textStyle: Theme.of(context).textTheme.bodyText1,
            letterSpacing: 3.5,
            height: calculateLineSpacing(16, 19),
            wordSpacing: calculateLineSpacing(16, 19),
          ),
        ),
      ) : null,
      trailing: trailing,
    ),
  );

  @override
  Widget build(BuildContext context) {
    return AuthEnforcer(
      informUserWhenSessionExpire: true,
      child: createDefaultAppBody(
        context: context,
        backgroundColor: Colors.white,
        leading: createIconButtonForAppBar(
          iconColor: brownColor1,
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        // TODO: Add scroll view
        body: Padding(
          padding: const EdgeInsets.fromLTRB(15, 15, 15, 15),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Padding(
                padding: const EdgeInsets.fromLTRB(10, 0, 0 ,0),
                child: Text(
                  'SETTINGS',
                  textAlign: TextAlign.left,
                  style: Theme.of(context).textTheme.headline3!.copyWith(
                    fontFamily: 'LeagueSpartan',
                    color: greenColor1,
                    letterSpacing: 3.3,
                  ),
                ),
              ),
              const SizedBox(height: 20,),
              createTile(
                title: 'MANAGE SUBSCRIPTION',
                subtitle: 'open in browser',
                trailing: IconButton(
                  icon: const Icon(
                    Icons.arrow_forward_ios_sharp,
                    color: Colors.white,
                  ),
                  onPressed: () {
                    // TODO: Open subscription website page
                  },
                ),
              ),
              const SizedBox(height: 15,),
              createTile(
                title: 'NOTIFICATION',
                subtitle: 'get updated with our latest recipe',
                trailing: Switch(
                  value: enableNotification,
                  activeTrackColor: Colors.white,
                  inactiveTrackColor: greenColor3,
                  thumbColor: getMaterialColorProperty(greenColor1),
                  onChanged: (enabled) {
                    setState(() {
                      enableNotification = enabled;
                    });
                    // TODO: Reflect changes
                  },
                ),
              ),
              const SizedBox(height: 15,),
              createTile(
                title: 'MISSING INGREDIENTS',
                subtitle: 'highlight any missing ingredients',
                trailing: Switch(
                  value: enableMissingIng,
                  activeTrackColor: Colors.white,
                  inactiveTrackColor: greenColor3,
                  thumbColor: getMaterialColorProperty(greenColor1),
                  onChanged: (enabled) {
                    setState(() {
                      enableMissingIng = enabled;
                    });
                    // TODO: Reflect changes
                  },
                ),
              ),
              const SizedBox(height: 40,),
              createTile(
                title: 'APP INFO',
                backgroundColor: greenColor1,
                trailing: IconButton(
                  icon: const Icon(
                    Icons.arrow_forward_ios_sharp,
                    color: Colors.white,
                  ),
                  onPressed: () {
                    Navigator.push(
                      context,
                      createFadingPageRouteBuilder(child: const AboutScreen())
                    );
                  },
                ),
              ),
              Expanded(
                child: Align(
                  alignment: Alignment.bottomCenter,
                  child: ElevatedButton(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      child: Text(
                        'LOG OUT',
                        style: GoogleFonts.lato(
                          color: Colors.white,
                          textStyle: Theme.of(context).textTheme.bodyText1,
                          fontWeight: FontWeight.bold,
                          letterSpacing: 3.5,
                        ),
                      ),
                    ),
                    style: roundedButton.copyWith(
                        backgroundColor: getMaterialColorProperty(brownColor1)
                    ),
                    onPressed: () {
                      AuthService auth = AuthService();
                      auth.logout();
                    },
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

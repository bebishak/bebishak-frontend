import 'package:flutter/material.dart';
import 'package:flutter_frontend/shared/constants.dart';
import 'package:flutter_frontend/shared/widget_prefabs.dart';

class AboutScreen extends StatelessWidget {
  const AboutScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return createDefaultAppBody(
      context: context,
      backgroundColor: Colors.white,
      leading: createIconButtonForAppBar(
        iconColor: brownColor1,
        onPressed: () {
          Navigator.pop(context);
        }
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Text(
                'ABOUT THIS APP',
                overflow: TextOverflow.ellipsis,
                maxLines: 4,
                style: Theme.of(context).textTheme.headline3!.copyWith(
                  color: greenColor1,
                  height: 1.2
                ),
              ),
              const SizedBox(height: 25,),
              Text(
                'KUKIE VER. 1.0\n\nWORDS WORDS\nIDK WHAT TO PUT HERE\nKUKIE APP DESCRIPTION\n',
                overflow: TextOverflow.ellipsis,
                maxLines: 10,
                style: Theme.of(context).textTheme.bodyText1!.copyWith(
                  color: darkGreyColor,
                  fontWeight: FontWeight.bold,
                ),
              ),
              Text(
                'EMPHASIS ON THE PART\n',
                overflow: TextOverflow.ellipsis,
                maxLines: 10,
                style: Theme.of(context).textTheme.bodyText1!.copyWith(
                  color: brownColor1,
                  fontWeight: FontWeight.bold,
                ),
              ),
              Text(
                'MORE WORDS HERE',
                overflow: TextOverflow.ellipsis,
                maxLines: 10,
                style: Theme.of(context).textTheme.bodyText1!.copyWith(
                  color: darkGreyColor,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

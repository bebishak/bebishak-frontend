import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter_frontend/models/models.dart';
import 'package:flutter_frontend/screens/recipe_upload_confirm.dart';
import 'package:flutter_frontend/services/backend.dart';
import 'package:flutter_frontend/services/image_picker.dart';
import 'package:flutter_frontend/services/storage.dart';
import 'package:flutter_frontend/services/validator.dart';
import 'package:flutter_frontend/shared/constants.dart';
import 'package:flutter_frontend/shared/utility_functions.dart';
import 'package:flutter_frontend/shared/widget_prefabs.dart';
import 'package:flutter_frontend/widgets/toggle_button.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';

import 'dart:developer' as developer;

import 'package:image_picker/image_picker.dart';

class RecipeFormScreen extends StatefulWidget {

  final Recipe? previousRecipe;
  final Function()? onDelete;

  const RecipeFormScreen({
    Key? key,
    this.previousRecipe,
    this.onDelete
  }) : super(key: key);

  @override
  _RecipeFormScreenState createState() => _RecipeFormScreenState();
}

class _RecipeFormScreenState extends State<RecipeFormScreen> {

  final _formKey = GlobalKey<FormState>();
  final _ingredientFormKey = GlobalKey<FormState>();
  final FocusNode dishNameFocus = FocusNode();
  final FocusNode cookingDurationFocus = FocusNode();
  final FocusNode ingredientTfFocus = FocusNode();
  final FocusNode stepTfFocus = FocusNode();
  final double ingredientIconsHeight = 32;

  late TextEditingController _ingredientNameController;
  late TextEditingController _ingredientAmountController;
  late TextEditingController _ingredientUnitController;
  late TextEditingController _stepController;
  late bool isEditMode;
  late List<bool> selectedMeals;
  late List<bool> selectedCuisines;

  XFile? recipeImage;
  late Future<Uint8List>? recipeImageFuture;
  bool isVegan = false;
  String name = '',
      desc = '',
      cookingDuration = '',
      error = '',
      imgUrl = '';

  List<Ingredient> ingredients = [];
  List<String> steps = [];

  List<String> getStringTags(List<bool> selected, List<String> master) {
    List<String> list = [];

    for (int i=0; i<master.length; i++) {
      if (selected[i]) {
        list.add(master[i]);
      }
    }

    return list;
  }
  List<bool> getBooleanTags(List<String> selected, List<String> master) {
    List<bool> list = [for (String _ in master) false];

    for (String item in selected) {
      int index = master.indexOf(item);
      list[index] = true;
    }

    return list;
  }

  Widget _createTitle() => RichText(
    overflow: TextOverflow.ellipsis,
    maxLines: 4,
    text: TextSpan(
        style: Theme.of(context).textTheme.headline3!.copyWith(
          color: Colors.white,
          height: 1.3,
        ),
        children: [
          TextSpan(
            text: isEditMode ? 'EDIT ' : 'UPLOAD\n',
            style: Theme.of(context).textTheme.headline3!.copyWith(
              color: greenColor1,
            ),
          ),
          TextSpan(
              text: isEditMode ? 'YOUR\n' : 'A '
          ),
          TextSpan(
            text: isEditMode ? '' : 'NEW ',
            style: Theme.of(context).textTheme.headline3!.copyWith(
              color: greenColor1,
              height: 0,
            ),
          ),
          const TextSpan(
              text: 'RECIPE'
          ),
        ]
    ),
  );

  Widget _createPhotoUpload() => Row(
    mainAxisAlignment: MainAxisAlignment.center,
    children: [
      createImageContainer(
        boxShadow: const [
          BoxShadow(
            color: Color.fromRGBO(0, 0, 0, 0.15),
            offset: Offset(0, 2),
            blurRadius: 3,
            spreadRadius: 2,
          )
        ],
        image: isEditMode && imgUrl != '' ? createImageWithPlaceholderWidget(
          placeholderPath: 'assets/dish.png',
          imgUrl: imgUrl,
        ) : FutureBuilder<Uint8List>(
          future: recipeImageFuture,
          builder: (context, snapshot) {
            return recipeImage != null? snapshot.hasData ? Image.memory(
              snapshot.data!,
              fit: BoxFit.scaleDown,
            ) : const SpinKitRing(
              key: Key('rotatingCircle'),
              color: greenColor1,
            ) : Image.asset(
              'assets/dish.png',
              fit: BoxFit.scaleDown,
            );
          },
        ),
      ),
      const SizedBox(width: 10,),
      ElevatedButton(
        style: roundedButton.copyWith(
          backgroundColor: getMaterialColorProperty(brownColor1),
        ),
        onPressed: () async {
          // TODO: Take photo or upload from gallery
          // await showDialog(
          //   context: context,
          //   builder: (context) => Dialog(
          //     elevation: 0,
          //     child: Center(
          //       child: UploadImageSelection(
          //         onImagePicked: (XFile? image) {
          //           // TODO: Add implementation
          //         },
          //       ),
          //     ),
          //   )
          // );
          recipeImage = await pickGallery();
          setState(() {
            if (recipeImage != null) {
              imgUrl = '';
            } else if (isEditMode) {
              imgUrl = widget.previousRecipe!.imgUrl ?? '';
            }
            refreshRecipeImage();
          });
        },
        child: Text(
          isEditMode ? 'CHANGE PHOTO' : 'UPLOAD PHOTO',
          style: Theme.of(context).textTheme.caption!.copyWith(
            color: Colors.white,
          ),
        ),
      ),
    ],
  );

  Widget _createContainer({
    Widget? child,
    Color color = Colors.white,
    BorderRadius? borderRadius,
    EdgeInsets padding = const EdgeInsets.symmetric(
      vertical: 20,
      horizontal: 18,
    ),
    List<BoxShadow> boxShadow = const [
      BoxShadow(
        color: Color.fromRGBO(0, 0, 0, 0.15),
        offset: Offset(0, 2),
        blurRadius: 3,
        spreadRadius: 2,
      )
    ],
  }) => Container(
    padding: padding,
    decoration: BoxDecoration(
      color: color,
      borderRadius: borderRadius ?? BorderRadius.circular(20),
      boxShadow: boxShadow,
    ),
    child: child,
  );

  Widget _createIngredientsForm(List<Ingredient> ingredients) => Column(
    crossAxisAlignment: CrossAxisAlignment.stretch,
    children: [
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Flexible(
            flex: ingredients.isNotEmpty ? 12 : 19,
            child: Align(
              alignment: Alignment.centerLeft,
              child: Padding(
                padding: const EdgeInsets.only(left: 5),
                child: Image.asset(
                  'assets/food.png',
                  fit: BoxFit.scaleDown,
                  height: ingredientIconsHeight,
                  color: Colors.black.withOpacity(0.6),
                  isAntiAlias: true,
                ),
              ),
            ),
          ),
          Flexible(
            flex: ingredients.isNotEmpty ? 4 : 7,
            child: Align(
              alignment: Alignment.centerLeft,
              child: Padding(
                padding: const EdgeInsets.only(left: 5),
                child: Image.asset(
                  'assets/numbers.png',
                  fit: BoxFit.scaleDown,
                  height: ingredientIconsHeight,
                  color: Colors.black.withOpacity(0.6),
                  isAntiAlias: true,
                ),
              ),
            ),
          ),
          Flexible(
            flex: ingredients.isNotEmpty ? 8 : 7,
            child: Align(
              alignment: Alignment.centerLeft,
              child: Padding(
                padding: const EdgeInsets.only(left: 5),
                child: Image.asset(
                  'assets/measurement.png',
                  fit: BoxFit.scaleDown,
                  height: ingredientIconsHeight,
                  color: Colors.black.withOpacity(0.6),
                  isAntiAlias: true,
                ),
              ),
            ),
          ),
        ],
      ),
      for (int i=0; i<ingredients.length; i++) Padding(
        padding: const EdgeInsets.symmetric(vertical: 5),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Flexible(
              flex: 12,
              child: TextField(
                controller: TextEditingController(text: ingredients[i].name),
                decoration: defaultInputDecoration,
                style: GoogleFonts.lato(
                  textStyle: Theme.of(context).textTheme.subtitle2,
                  color: darkGreyColor,
                  fontWeight: FontWeight.normal,
                  letterSpacing: 4.16,
                ),
                onChanged: (val) {
                  ingredients[i].name = val.trim();
                },
              ),
            ),
            const SizedBox(width: 10,),
            Flexible(
              flex: 4,
              child: TextField(
                controller: TextEditingController(text: '${ingredients[i].amount}'),
                decoration: defaultInputDecoration,
                textAlign: TextAlign.center,
                style: GoogleFonts.lato(
                  textStyle: Theme.of(context).textTheme.subtitle2,
                  color: darkGreyColor,
                  fontWeight: FontWeight.normal,
                  letterSpacing: 4.16,
                ),
                onChanged: (val) {
                  try {
                    double amount = double.parse(val.trim());
                    if (PositiveNumberFieldValidator.validate(val) == null) {
                      ingredients[i].amount = amount;
                    }
                  } catch(e) {
                    developer.log(
                      'Unable to update amount of ingredient at index $i: ${e.toString()}',
                      name: 'recipe_form.dart - _createIngredientsForm()'
                    );
                  }
                },
              ),
            ),
            const SizedBox(width: 10,),
            Flexible(
              flex: 5,
              child: TextField(
                controller: TextEditingController(text: ingredients[i].unit),
                decoration: defaultInputDecoration,
                textAlign: TextAlign.center,
                style: GoogleFonts.lato(
                  textStyle: Theme.of(context).textTheme.subtitle2,
                  color: darkGreyColor,
                  fontWeight: FontWeight.normal,
                  letterSpacing: 4.16,
                ),
                onChanged: (val) {
                  ingredients[i].unit = val.trim();
                },
              ),
            ),
            IconButton(
              icon: const Icon(
                Icons.clear,
                color: brownColor1,
              ),
              onPressed: () {
                setState(() {
                  ingredients.removeAt(i);
                });
              },
            ),
          ],
        ),
      ),
      const SizedBox(height: 5,),
      Form(
        key: _ingredientFormKey,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Flexible(
              flex: 12,
              child: TextFormField(
                key: const Key('ingNameTF'),
                controller: _ingredientNameController,
                focusNode: ingredientTfFocus,
                style: GoogleFonts.lato(
                  textStyle: Theme.of(context).textTheme.subtitle2,
                  color: darkGreyColor,
                  fontWeight: FontWeight.normal,
                  letterSpacing: 4.16,
                ),
                decoration: defaultInputDecoration.copyWith(
                  hintText: 'INGREDIENT',
                  hintStyle: GoogleFonts.lato(
                    textStyle: Theme.of(context).textTheme.subtitle2,
                    color: darkGreyColor.withOpacity(0.4),
                    fontWeight: FontWeight.normal,
                    letterSpacing: 4.16,
                  ),
                ),
                validator: EmptyFieldValidator.validate,
              ),
            ),
            const SizedBox(width: 10,),
            Flexible(
              flex: 4,
              child: TextFormField(
                key: const Key('ingAmountTF'),
                controller: _ingredientAmountController,
                textAlign: TextAlign.center,
                keyboardType: const TextInputType.numberWithOptions(
                  decimal: true,
                  signed: false,
                ),
                style: GoogleFonts.lato(
                  textStyle: Theme.of(context).textTheme.subtitle2,
                  color: darkGreyColor,
                  fontWeight: FontWeight.normal,
                  letterSpacing: 4.16,
                ),
                decoration: defaultInputDecoration.copyWith(
                  contentPadding: const EdgeInsets.symmetric(
                    horizontal: 5
                  ),
                  hintText: 'QTY',
                  hintStyle: GoogleFonts.lato(
                    textStyle: Theme.of(context).textTheme.subtitle2,
                    color: darkGreyColor.withOpacity(0.4),
                    fontWeight: FontWeight.normal,
                    letterSpacing: 4.16,
                  ),
                ),
                validator: PositiveNumberFieldValidator.validate,
              ),
            ),
            const SizedBox(width: 10,),
            Flexible(
              flex: 5,
              child: TextFormField(
                key: const Key('ingUnitTF'),
                controller: _ingredientUnitController,
                textAlign: TextAlign.center,
                style: GoogleFonts.lato(
                  textStyle: Theme.of(context).textTheme.subtitle2,
                  color: darkGreyColor,
                  fontWeight: FontWeight.normal,
                  letterSpacing: 4.16,
                ),
                decoration: defaultInputDecoration.copyWith(
                  contentPadding: const EdgeInsets.symmetric(
                      horizontal: 5
                  ),
                  hintText: 'UNIT',
                  hintStyle: GoogleFonts.lato(
                    textStyle: Theme.of(context).textTheme.subtitle2,
                    color: darkGreyColor.withOpacity(0.4),
                    fontWeight: FontWeight.normal,
                    letterSpacing: 4.16,
                  ),
                ),
                validator: PositiveNumberFieldValidator.validate,
              ),
            ),
          ],
        ),
      ),
      const SizedBox(height: 25,),
      Center(
        child: _createAddButton(onTap: () {
          try {
            String name = _ingredientNameController.text.trim();
            double amount = double.parse(_ingredientAmountController.text.trim());
            String unit = _ingredientUnitController.text.trim();

            if (name.isNotEmpty && unit.isNotEmpty) {
              Ingredient ing = Ingredient(
                name: name,
                amount: amount,
                unit: unit,
              );
              setState(() {
                _ingredientNameController.text = '';
                _ingredientAmountController.text = '';
                _ingredientUnitController.text = '';
                ingredients.add(ing);
                ingredientTfFocus.requestFocus();
              });
            }
          } catch (e) {
            developer.log(
                'Unable to add ingredient: ${e.toString()}',
                name: 'recipe_form.dart - _createIngredientsForm()'
            );
          }
        })
      ),
    ],
  );

  Widget _createStepsForm(List<String> steps) => Column(
    crossAxisAlignment: CrossAxisAlignment.stretch,
    children: [
      // The individual steps
      for (int i=0; i<steps.length; i++) Padding(
        padding: const EdgeInsets.symmetric(vertical: 5),
        child: IntrinsicHeight(
          child: Row(
            children: [
              Flexible(
                flex: 1,
                child: Padding(
                  padding: const EdgeInsets.only(top: 0),
                  child: Align(
                    alignment: Alignment.center,
                    child: Text(
                      '${i + 1}.',
                      style: GoogleFonts.lato(
                          textStyle: Theme.of(context).textTheme.caption,
                          fontWeight: FontWeight.normal,
                          color: greenColor1,
                          letterSpacing: 3.5
                      ),
                    ),
                  ),
                ),
              ),
              Flexible(
                flex: 11,
                child: TextField(
                  minLines: 1,
                  maxLines: 7,
                  controller: TextEditingController(text: steps[i]),
                  decoration: defaultInputDecoration,
                  style: GoogleFonts.lato(
                    textStyle: Theme.of(context).textTheme.bodyText1,
                    color: darkGreyColor,
                    fontWeight: FontWeight.normal,
                    letterSpacing: 2.84,
                  ),
                  onChanged: (val) {
                    steps[i] = val;
                  },
                ),
              ),
              Flexible(
                flex: 1,
                child: IconButton(
                  icon: const Icon(
                    Icons.clear,
                    color: brownColor1,
                  ),
                  onPressed: () {
                    setState(() {
                      steps.removeAt(i);
                    });
                  },
                ),
              ),
            ],
          ),
        ),
      ),
      const SizedBox(height: 5,),
      // The input form
      IntrinsicHeight(
        child: Row(
          children: [
            Flexible(
              flex: 1,
              child: Padding(
                padding: const EdgeInsets.only(top: 0),
                child: Align(
                  alignment: Alignment.center,
                  child: Text(
                    '${steps.length + 1}.',
                    style: GoogleFonts.lato(
                      textStyle: Theme.of(context).textTheme.caption,
                      fontWeight: FontWeight.normal,
                      color: greenColor1,
                      letterSpacing: 3.5
                    ),
                  ),
                ),
              ),
            ),
            Flexible(
              flex: 12,
              child: TextField(
                controller: _stepController,
                focusNode: stepTfFocus,
                minLines: 1,
                maxLines: 7,
                decoration: defaultInputDecoration.copyWith(
                  hintText: 'add a step',
                  hintStyle: GoogleFonts.lato(
                    textStyle: Theme.of(context).textTheme.bodyText1,
                    color: darkGreyColor.withOpacity(0.4),
                    fontWeight: FontWeight.normal,
                    letterSpacing: 2.84,
                  ),
                ),
                style: GoogleFonts.lato(
                  textStyle: Theme.of(context).textTheme.bodyText1,
                  color: darkGreyColor,
                  fontWeight: FontWeight.normal,
                  letterSpacing: 2.84,
                ),
              ),
            ),
          ],
        ),
      ),
      const SizedBox(height: 25,),
      Center(
        child: _createAddButton(onTap: () {
            if (_stepController.text.isNotEmpty) {
              setState(() {
                steps.add(_stepController.text.trim());
                _stepController.text = '';
                stepTfFocus.requestFocus();
              });
            }
          }
        ),
      ),
    ],
  );

  /// Creates the add button to be used in the
  /// ingredients and steps form
  Widget _createAddButton({
    Function()? onTap,
  }) => GestureDetector(
    onTap: onTap,
    child: Container(
      decoration: BoxDecoration(
        color: greenColor2,
        borderRadius: BorderRadius.circular(10),
        boxShadow: [
          BoxShadow(
            color: Colors.black.withOpacity(0.15),
            spreadRadius: 1,
            blurRadius: 3,
            offset: const Offset(0, 2),
          )
        ]
      ),
      child: const Icon(
        Icons.add,
        color: Colors.white,
        size: 40,
      ),
    ),
  );

  /// Perform the recipe upload procedure
  Future<void> performUploadProcess(Recipe recipe) async {
    bool uploadFailed = false;
    showDialog(
      context: context,
      builder: (context) => Dialog(
        elevation: 0,
        backgroundColor: Colors.transparent,
        child: createUploadLoadingDialogWidget(context),
      ),
    );
    // Upload recipe image to firebase storage
    if (recipeImage != null) {
      StorageService ss = StorageService();
      String uid = context.read<KukieUser?>()!.uid;
      await ss.uploadRecipeImage(
        image: recipeImage!,
        uid: uid,
        recipeName: recipe.name,
      ).then((url) {
        recipe.imgUrl = url;
      }).catchError((e) {
        developer.log(
            'Unable to upload recipe image:',
            name: 'recipe_form.dart',
            error: e
        );
        Navigator.pop(context);   // Pop loading
        uploadFailed = true;
        showDialog(
          context: context,
          builder: (context) => Dialog(
            elevation: 0,
            backgroundColor: Colors.transparent,
            child: createFailedUploadDialogWidget(context),
          ),
        );
      });
    }

    if (!uploadFailed) {
      // Upload/update recipe to database
      Future taskFuture = uploadRecipe(
        recipe,
        isUpdate: isEditMode
      );
      await taskFuture.then((value) async {
        Navigator.pop(context);   // Pop loading
        await showDialog(
          context: context,
          builder: (context) => Dialog(
            elevation: 0,
            backgroundColor: Colors.transparent,
            child: createFinishedUploadDialogWidget(context),
          ),
        );
        Navigator.pop(context, true);   // Pop recipe form
      }).catchError((e, stackTrace) {
        developer.log(
          'Unable to ${isEditMode? 'upload' : 'edit'} recipe:',
          name: 'recipe_upload_confirm.dart',
          error: e,
          stackTrace: stackTrace
        );
        Navigator.pop(context);   // Pop loading
        showDialog(
          context: context,
          builder: (context) => Dialog(
            elevation: 0,
            backgroundColor: Colors.transparent,
            child: createFailedUploadDialogWidget(context),
          ),
        );
      });
    }
  }

  void refreshRecipeImage() {
    recipeImageFuture = recipeImage != null ? recipeImage!.readAsBytes() : null;
  }

  @override
  void initState() {
    super.initState();
    _ingredientNameController = TextEditingController();
    _ingredientAmountController = TextEditingController();
    _ingredientUnitController = TextEditingController();
    _stepController = TextEditingController();
    isEditMode = widget.previousRecipe != null;
    if (isEditMode) {
      Recipe recipe = widget.previousRecipe!;
      name = recipe.name;
      desc = recipe.desc;
      cookingDuration = recipe.duration.toString();
      ingredients.addAll(recipe.ingredients);
      steps.addAll(recipe.steps);
      selectedMeals = getBooleanTags(recipe.mealTags, meals);
      selectedCuisines = getBooleanTags(recipe.cuisineTags, cuisines);
      isVegan = recipe.isVegan;
      imgUrl = recipe.imgUrl ?? '';
    } else {
      selectedMeals = [for (String _ in meals) false];
      selectedCuisines = [for (String _ in cuisines) false];
    }

    refreshRecipeImage();
  }

  @override
  void dispose() {
    _ingredientNameController.dispose();
    _ingredientAmountController.dispose();
    _ingredientUnitController.dispose();
    _stepController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return createDefaultAppBody(
      context: context,
      backgroundColor: blueColor,
      leading: createIconButtonForAppBar(
        iconColor: Colors.white,
        onPressed: () {
          Navigator.pop(context);
        }
      ),
      trailing: isEditMode ? RawMaterialButton(
        child: const Icon(
          Icons.delete_outlined,
          color: Colors.white,
          size: 35,
        ),
        fillColor: brownColor1,
        elevation: 2.0,
        shape: const CircleBorder(),
        padding: const EdgeInsets.all(5),
        onPressed: () {
          KukieUser? user = context.read<KukieUser?>();
          if (user != null) {
            showDialog(
              context: context,
              builder: (context) => Dialog(
                elevation: 0,
                backgroundColor: Colors.transparent,
                child: createDeleteRecipeDialogWidget(
                  context,
                  userId: user.uid,
                  recipe: widget.previousRecipe!,
                  onDelete: widget.onDelete
                ),
              ),
            );
          }
        },
      ) : null,
      body: Form(
        key: _formKey
        ,
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.only(
              bottom: 10,
              left: 20,
              right: 20,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                _createTitle(),
                const SizedBox(height: 20,),
                _createPhotoUpload(),
                const SizedBox(height: 20,),
                _createContainer(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Text(
                        'DISH NAME',
                        style: GoogleFonts.lato(
                          textStyle: Theme.of(context).textTheme.bodyText1,
                          fontWeight: FontWeight.bold,
                          color: brownColor1,
                        ),
                      ),
                      const SizedBox(height: 10,),
                      TextFormField(
                        key: const Key('dishNameTF'),
                        focusNode: dishNameFocus,
                        initialValue: name,
                        style: GoogleFonts.lato(
                          textStyle: Theme.of(context).textTheme.subtitle2,
                          color: darkGreyColor,
                          fontWeight: FontWeight.normal,
                          letterSpacing: 2.48,
                        ),
                        decoration: defaultInputDecoration,
                        validator: (val) => EmptyFieldValidator.validate(val, focusNode: dishNameFocus),
                        onChanged: (val) => name = val,
                      ),
                      const SizedBox(height: 30,),
                      Text(
                        'DISH DESCRIPTION',
                        style: GoogleFonts.lato(
                          textStyle: Theme.of(context).textTheme.bodyText1,
                          fontWeight: FontWeight.bold,
                          color: brownColor1,
                        ),
                      ),
                      const SizedBox(height: 10,),
                      TextFormField(
                        minLines: 10,
                        maxLines: 10,
                        initialValue: desc,
                        key: const Key('dishDescTF'),
                        style: GoogleFonts.lato(
                          textStyle: Theme.of(context).textTheme.subtitle2,
                          color: darkGreyColor,
                          fontWeight: FontWeight.normal,
                          letterSpacing: 4.16,
                        ),
                        decoration: defaultInputDecoration.copyWith(
                          contentPadding: const EdgeInsets.symmetric(
                            vertical: 5,
                            horizontal: 10,
                          ),
                        ),
                        onChanged: (val) => desc = val,
                      ),
                      const SizedBox(height: 20,),
                      Text(
                        'COOKING DURATION (MIN.)',
                        style: GoogleFonts.lato(
                          textStyle: Theme.of(context).textTheme.bodyText1,
                          fontWeight: FontWeight.bold,
                          color: brownColor1,
                        ),
                      ),
                      const SizedBox(height: 10,),
                      TextFormField(
                        key: const Key('cookingTimeTF'),
                        focusNode: cookingDurationFocus,
                        initialValue: cookingDuration,
                        keyboardType: const TextInputType.numberWithOptions(
                          signed: false,
                          decimal: true,
                        ),
                        style: GoogleFonts.lato(
                          textStyle: Theme.of(context).textTheme.subtitle2,
                          color: darkGreyColor,
                          fontWeight: FontWeight.normal,
                          letterSpacing: 4.16,
                        ),
                        decoration: defaultInputDecoration,
                        validator: (val) => PositiveNumberFieldValidator.validate(val, focusNode: cookingDurationFocus),
                        onChanged: (val) => cookingDuration = val,
                      ),
                    ],
                  ),
                ),
                const SizedBox(height: 20,),
                _createContainer(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Text(
                        'INGREDIENTS',
                        style: GoogleFonts.lato(
                          textStyle: Theme.of(context).textTheme.bodyText1,
                          fontWeight: FontWeight.bold,
                          color: greenColor1,
                        ),
                      ),
                      const SizedBox(height: 10,),
                      _createIngredientsForm(ingredients),
                      const SizedBox(height: 20,),
                      const Padding(
                        padding: EdgeInsets.symmetric(horizontal: 30),
                        child: Divider(
                          color: darkGreyColor,
                          thickness: 1,
                        ),
                      ),
                      const SizedBox(height: 20,),
                      Text(
                        'STEPS',
                        style: GoogleFonts.lato(
                          textStyle: Theme.of(context).textTheme.bodyText1,
                          fontWeight: FontWeight.bold,
                          color: greenColor1,
                        ),
                      ),
                      const SizedBox(height: 10,),
                      _createStepsForm(steps),
                    ],
                  ),
                ),
                const SizedBox(height: 20,),
                _createContainer(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Text(
                        'MEAL',
                        style: GoogleFonts.lato(
                          textStyle: Theme.of(context).textTheme.bodyText1,
                          fontWeight: FontWeight.bold,
                          color: greenColor1,
                        ),
                      ),
                      const SizedBox(height: 10,),
                      Wrap(
                        spacing: 10,
                        runSpacing: 5,
                        direction: Axis.horizontal,
                        children: [
                          for (int i = 0; i < meals.length; i++) ToggleElevatedButton(
                            toggledColor: greenColor1,
                            untoggledColor: greenColor3,
                            isToggled: selectedMeals[i],
                            onChanged: (isSelected) {
                              setState(() {
                                selectedMeals[i] = isSelected;
                              });
                            },
                            child: Padding(
                              padding: const EdgeInsets.symmetric(
                                horizontal: 6,
                              ),
                              child: Text(
                                meals[i].toUpperCase(),
                                style: Theme.of(context).textTheme.caption!.copyWith(
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(height: 20,),
                      Text(
                        'CUISINE',
                        style: GoogleFonts.lato(
                          textStyle: Theme.of(context).textTheme.bodyText1,
                          fontWeight: FontWeight.bold,
                          color: greenColor1,
                        ),
                      ),
                      const SizedBox(height: 10,),
                      Wrap(
                        spacing: 10,
                        runSpacing: 5,
                        direction: Axis.horizontal,
                        children: [
                          for (int i = 0; i < cuisines.length; i++) ToggleElevatedButton(
                            toggledColor: greenColor1,
                            untoggledColor: greenColor3,
                            isToggled: selectedCuisines[i],
                            onChanged: (isSelected) {
                              setState(() {
                                selectedCuisines[i] = isSelected;
                              });
                            },
                            child: Padding(
                              padding: const EdgeInsets.symmetric(
                                horizontal: 6,
                              ),
                              child: Text(
                                cuisines[i].toUpperCase(),
                                style: Theme.of(context).textTheme.caption!.copyWith(
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(height: 10,),
                      Row(
                        children: [
                          Text(
                            'VEGAN',
                            style: GoogleFonts.lato(
                              textStyle: Theme.of(context).textTheme.bodyText1,
                              fontWeight: FontWeight.bold,
                              color: greenColor1,
                            ),
                          ),
                          const SizedBox(width: 15,),
                          Switch(
                            value: isVegan,
                            activeTrackColor: greyColor,
                            inactiveTrackColor: greenColor3,
                            thumbColor: getMaterialColorProperty(greenColor1),
                            onChanged: (enabled) {
                              setState(() {
                                isVegan = enabled;
                              });
                            },
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                if (error != '') Padding(
                  padding: const EdgeInsets.only(top: 15),
                  child: Text(
                    error,
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.bodyText1!.copyWith(
                      color: Colors.red,
                    ),
                  ),
                ),
                SizedBox(height: error == ''? 30 : 15,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    ElevatedButton(
                      style: roundedButton.copyWith(
                        backgroundColor: getMaterialColorProperty(greenColor1),
                      ),
                      onPressed: () async {
                        setState(() {
                          error = '';
                        });
                        if (_formKey.currentState!.validate()) {
                          KukieUser? user = context.read<KukieUser?>();

                          if (user == null) {
                            setState(() {
                              error = 'Unable to verify user. Please try to login again.';
                            });
                          } else {
                            final Recipe recipe = Recipe(
                              username: user.username,
                              authorUid: user.uid,
                              name: name,
                              desc: desc,
                              duration: double.parse(cookingDuration),
                              ingredients: ingredients,
                              steps: steps,
                              mealTags: getStringTags(selectedMeals, meals),
                              cuisineTags: getStringTags(selectedCuisines, cuisines),
                              isVegan: isVegan,
                              imgUrl: imgUrl,
                            );
                            bool isGoToUpload = await Navigator.push(
                              context,
                              createFadingPageRouteBuilder(child: const ConfirmUploadRecipeScreen())
                            );

                            if (isGoToUpload) {
                              await performUploadProcess(recipe);
                            }
                          }
                        }
                      },
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                          vertical: 12,
                          horizontal: 8,
                        ),
                        child: Text(
                          isEditMode ? 'SAVE CHANGES' : 'NEXT',
                          style: Theme.of(context).textTheme.caption!.copyWith(
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

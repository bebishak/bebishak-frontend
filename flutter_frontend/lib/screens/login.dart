import 'package:flutter/material.dart';
import 'package:flutter_frontend/models/models.dart';
import 'package:flutter_frontend/screens/forgot_pw.dart';
import 'package:flutter_frontend/screens/home.dart';
import 'package:flutter_frontend/services/auth.dart';
import 'package:flutter_frontend/services/backend.dart';
import 'package:flutter_frontend/services/validator.dart';
import 'package:flutter_frontend/shared/constants.dart';
import 'package:flutter_frontend/shared/widget_prefabs.dart';
import 'package:flutter_frontend/widgets/auth_enforcer.dart';
import 'package:flutter_frontend/widgets/loading.dart';

import 'dart:developer' as developer;

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final _formKey = GlobalKey<FormState>();

  String email = '', password = '', error = '';

  Future performLoginOperation() async {
    showLoading(context);
    setState(() {
      error = '';
    });
    if (_formKey.currentState!.validate()) {
      AuthService auth = AuthService();
      developer.log(
          'Performing login operation...',
          name: 'login.dart - performLoginOperation()'
      );
      await auth.signIn(email, password).then((value) async {
        if (value == null) {
          Navigator.pop(context);   // Pop loading
          setState(() {
            error = 'Unable to verify account';
          });
        } else {
          // Check if user data exists in database
          // If not, create it
          KukieUser user = auth.userFromFirebaseUser(value)!;
          bool userDataExistsInDb = await checkIfUserExistsInDb(user);
          if (!userDataExistsInDb) {
            developer.log(
                'User does not exist in db',
                name: 'login.dart - performLoginOperation()'
            );
            await createUserInDb(user).then((value) {
              userDataExistsInDb = true;
              developer.log(
                  'Successfully created new user in db',
                  name: 'login.dart - performLoginOperation()'
              );
            }).catchError((e, stacktrace) {
              Navigator.pop(context);   // Pop loading
              developer.log(
                  'Unable to create user data in database:',
                  error: e,
                  stackTrace: stacktrace,
                  name: 'login.dart - performLoginOperation()'
              );
            });
          }

          if (userDataExistsInDb) {
            developer.log(
                'User exists in db',
                name: 'login.dart - performLoginOperation()'
            );
            // Go to main menu
            Navigator.popUntil(context, (route) => route.isFirst);
            Navigator.pushReplacement(
                context,
                createFadingPageRouteBuilder(child: const AuthEnforcer(child: HomeScreen()))
            );
          }
        }
      }).catchError((e) {
        developer.log(
            'Unable to login:',
            name: 'login.dart - performLoginOperation()',
            error: e
        );
        Navigator.pop(context);   // Pop loading
        setState(() {
          error = 'Something went wrong, please try again';
        });
      });
    }
    else {
      Navigator.pop(context);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false, // Allows on-screen keyboard without renderflex error
      backgroundColor: Colors.white,
      appBar: AppBar(
        // Back button
        backgroundColor: Colors.transparent,
        elevation: 0,
        automaticallyImplyLeading: false,
        leading: createIconButtonForAppBar(
            iconColor: brownColor1,
            onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      body:
          // Main form
          // Holds the salad image, email and password field, forgot password hyperlink, and login button
          Form(
            key: _formKey,
            child: ListView(
              children: [
                const SizedBox(height: 35.0), // Gap

                // Salad image
                Padding(
                    padding: const EdgeInsets.only(left: 65.0),
                    child: Align(
                      alignment: Alignment.centerLeft,
                      child: Image.asset(
                        'assets/salad.png',
                        fit: BoxFit.scaleDown,
                        height: 75,
                      ),
                    )),
                SizedBox(
                  // Green rectangle with circular edges
                  child: Container(
                    margin: const EdgeInsets.fromLTRB(25.0, 0.0, 25.0, 0.0),
                    decoration: BoxDecoration(
                      color: greenColor2,
                      borderRadius: BorderRadius.circular(25.0),
                      boxShadow: [
                        BoxShadow(
                            color: Colors.black.withOpacity(0.45),
                            blurRadius: 25.0,
                            spreadRadius: -10.0,
                            offset: const Offset(2.0, 15.0))
                      ],
                    ),
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(25.0, 40.0, 25.0, 0.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          // EMAIL text
                          Text('EMAIL',
                              style:
                                  Theme.of(context).textTheme.bodyText1!.copyWith(
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold,
                                      )),

                          const SizedBox(height: 6.0), // Gap

                          // Email Field
                          TextFormField(
                            initialValue: email,
                            decoration: InputDecoration(
                                fillColor: Colors.white,
                                isDense: true,
                                filled: true,
                                contentPadding:
                                    const EdgeInsets.symmetric(vertical: 5.0, horizontal: 10.0),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(15.0))),
                            validator: EmptyFieldValidator.validate,
                            onChanged: (val) => email = val,
                          ),

                          const SizedBox(height: 30.0), // Gap

                          // PASSWORD text
                          Text('PASSWORD',
                              style:
                                  Theme.of(context).textTheme.bodyText1!.copyWith(
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold,
                                      )),

                          const SizedBox(height: 6.0), // Gap

                          // Password field
                          TextFormField(
                            initialValue: password,
                            decoration: InputDecoration(
                                fillColor: Colors.white,
                                isDense: true,
                                filled: true,
                                contentPadding:
                                    const EdgeInsets.symmetric(vertical: 5.0, horizontal: 10.0),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(15.0))),
                            validator: EmailFieldValidator.validate,
                            obscureText: true,
                            onChanged: (val) => password = val,
                          ),
                          // FORGOT PASSWORD text button
                          Padding(
                            padding: const EdgeInsets.only(right: 105.0),
                            child: TextButton(
                              onPressed: () async {
                                Navigator.push(
                                  context,
                                  createFadingPageRouteBuilder(child: const ForgotPassScreen())
                                );
                              },
                              style: TextButton.styleFrom(
                                minimumSize: Size.zero,
                                padding: const EdgeInsets.only(top: 7.0),
                                tapTargetSize: MaterialTapTargetSize.shrinkWrap,
                              ),
                              child: Text(
                                'FORGOT PASSWORD',
                                style: Theme.of(context).textTheme.overline!.copyWith(
                                  color: greyColor,
                                  fontWeight: FontWeight.bold,
                                  decoration: TextDecoration.underline,
                                )
                              ),
                            ),
                          ),

                          const SizedBox(height: 25.0), // Gap

                          // LOG IN button
                          Align(
                            alignment: Alignment.bottomRight,
                            child: ElevatedButton(
                              style: ButtonStyle(
                                  backgroundColor:
                                      MaterialStateProperty.all<Color>(
                                          brownColor1),
                                  shape: MaterialStateProperty.all<
                                          RoundedRectangleBorder>(
                                      RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(15.0)))),
                              child: Text('LOG IN',
                              style:Theme.of(context).textTheme.button!.copyWith(
                                  color: Colors.white,
                                )
                              ),
                              onPressed: () async {
                                await performLoginOperation();
                              },
                            ),
                          ),
                          const SizedBox(height: 6.0,),  // Gap
                        ],
                      ),
                    ),
                  ),
                ),
            const SizedBox(height: 25.0), // Gap

            // Error Text
            Text(error,
              textAlign: TextAlign.center,
              style: Theme.of(context).textTheme.bodyText1!.copyWith(
                color: Colors.red,)
            ),

            const SizedBox(height: 25.0), // Gap

            // SIGN IN Text
            Center(
              child: Text(
              'SIGN IN',
              style: Theme.of(context).textTheme.subtitle1!.copyWith(
                color: blueColor,
                )
              ),
            ),
            // KUKIE Text
            Center(
              child: Text(
              'KUKIE',
              style: Theme.of(context).textTheme.headline1!.copyWith(
                color: greenColor1,
                fontWeight: FontWeight.bold,
                letterSpacing: 0,
                height: 1,
              )
            ),
          ),
        ],
      ),
    ));
  }
}

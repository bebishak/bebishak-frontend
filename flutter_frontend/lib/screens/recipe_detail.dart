import 'package:flutter/material.dart';
import 'package:flutter_frontend/models/models.dart';
import 'package:flutter_frontend/screens/home.dart';
import 'package:flutter_frontend/services/backend.dart';
import 'package:flutter_frontend/shared/constants.dart';
import 'package:flutter_frontend/shared/utility_functions.dart';

import 'dart:developer' as developer;

import 'package:flutter_frontend/shared/widget_prefabs.dart';
import 'package:flutter_frontend/widgets/auth_enforcer.dart';
import 'package:flutter_frontend/widgets/kukie_bottom_nav_bar.dart';
import 'package:flutter_frontend/widgets/loading.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';

const double horizontalPaddingInsideContainer = 30;

class RecipeScreen extends StatefulWidget {

  final Recipe recipe;
  final List<String> ownedIngredients;

  const RecipeScreen({
    Key? key,
    required this.recipe,
    this.ownedIngredients = const[],
  }) : super(key: key);

  @override
  _RecipeScreenState createState() => _RecipeScreenState();
}

class _RecipeScreenState extends State<RecipeScreen> {

  late final Recipe recipe;

  bool submitTapped = false;

  /// Create rating widget
  final RatingWidget ratingWidget = RatingWidget(
    empty: const Icon(
      Icons.star_outline_rounded,
      color: Color.fromRGBO(0, 0, 0, 0.43),
    ),
    half: const Icon(
      Icons.star_half_rounded,
      color: Colors.amber,
    ),
    full: const Icon(
      Icons.star_rounded,
      color: Colors.amber,
    ),
  );

  /// Create the container for the recipe title information,
  /// This is also the first thing the user sees when opening this
  /// screen.
  Widget _createTitleContainer({
    double iconHeight = 128
  }) => Wrap(
    children: [
      Column(
        children: [
          Stack(
            children: [
              SizedBox(
                height: iconHeight + 30 + 15 + 10,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    const Expanded(
                      flex: 3,
                      child: SizedBox(),
                    ),
                    Expanded(
                      flex: 2,
                      child: Container(
                        decoration: BoxDecoration(
                          color: brownColor2,
                          border: Border.all(
                            color: Colors.transparent,
                            width: 0,
                          ),
                          borderRadius: const BorderRadius.only(
                              topLeft: Radius.circular(20.0),
                              topRight: Radius.circular(20.0)
                          ),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 15),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              Container(
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(10),
                                  boxShadow: const [
                                    BoxShadow(
                                      color: Color.fromRGBO(0, 0, 0, 0.2),
                                      blurRadius: 10,
                                    )
                                  ]
                                ),
                                child: FavoriteRecipe(
                                  initiallyLiked: recipe.isLiked,
                                  onLiked: (isLiked) {
                                    recipe.isLiked = isLiked;
                                  },
                                )
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 10),
                child: Center(
                  child: createImageContainer(
                    image: createImageWithPlaceholderWidget(
                      placeholderPath: 'assets/dish.png',
                      imgUrl: recipe.imgUrl ?? '',
                    ),
                  ),
                ),
              ),
            ],
          ),
          Container(
            decoration: BoxDecoration(
              color: brownColor2,
              border: Border.all(
                  color: Colors.transparent,
                  width: 0
              ),
              borderRadius: const BorderRadius.only(
                bottomLeft: Radius.circular(20.0),
                bottomRight: Radius.circular(20.0),
              ),
              // boxShadow: [
              //   BoxShadow(
              //     color: Color.fromRGBO(0, 0, 0, 0.4),
              //     spreadRadius: 0.1,
              //     blurRadius: 1,
              //   ),
              // ],
            ),
            child: Center(
              child: Padding(
                key: const Key('content'),
                padding: const EdgeInsets.fromLTRB(
                  horizontalPaddingInsideContainer,
                  10,
                  horizontalPaddingInsideContainer,
                  35
                ),
                child: _createTitleWidget()
              ),
            ),
          ),
        ],
      ),
    ],
  );

  /// Create the widget used to display tags
  Widget _createTagsDisplayWidget(List<String> tags) {
    List<Widget> children = [];

    for (String tag in tags) {
      // Add text widget
      children.add(Text(
        tag.toUpperCase(),
        style: GoogleFonts.lato(
          textStyle: Theme.of(context).textTheme.caption,
          color: brownColor1,
          letterSpacing: 2.85,
        ),
      ));
      // Add divider
      children.add(const SizedBox(
        height: 15,
        child: VerticalDivider(
          color: brownColor1,
          thickness: 1.3,
        ),
      ));
    }

    // Remove the last divider
    if (tags.isNotEmpty) {
      children.removeLast();
    }

    return Wrap(
      alignment: WrapAlignment.center,
      children: children,
    );
  }

  /// Creates the contents of recipe's title information widget
  Widget _createTitleWidget() {
    List<String> tags = List.from(recipe.mealTags)
      ..addAll(recipe.cuisineTags);
    String desc = recipe.desc;
    String username = recipe.username.toUpperCase();

    if (recipe.isVegan) {
      tags.add('Vegan'.toUpperCase());
    }

    tags.sort();

    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Text(
          username.substring(username.length-1) == 'S' ? '$username\'' : '$username\'S',
          style: GoogleFonts.lato(
            textStyle: Theme.of(context).textTheme.bodyText1,
            fontWeight: FontWeight.normal,
            color: Colors.white,
            letterSpacing: 3.5,
          ),
        ),
        const SizedBox(height: 15,),
        Text(
          recipe.name.toUpperCase(),
          textAlign: TextAlign.center,
          style: Theme.of(context).textTheme.headline4!.copyWith(
            color: Colors.white,
            letterSpacing: 3,
          ),
        ),
        const SizedBox(height: 5,),
        _createTagsDisplayWidget(tags),
        const SizedBox(height: 10,),
        Text(
          '${convertDoubleToString(recipe.duration)} Min.'.toUpperCase(),
          style: GoogleFonts.lato(
            textStyle: Theme.of(context).textTheme.caption,
            fontWeight: FontWeight.normal,
            letterSpacing: 2.85,
            color: Colors.white,
          ),
        ),
        const SizedBox(height: 10,),
        RatingBar.builder(
          ignoreGestures: true,
          itemSize: 43,
          initialRating: recipe.globalRating,
          direction: Axis.horizontal,
          allowHalfRating: false,
          itemCount: 5,
          itemPadding: const EdgeInsets.symmetric(horizontal: 4.0),
          // ratingWidget: ratingWidget,
          itemBuilder: (context, _) => const Icon(
            Icons.star_rounded,
            color: Colors.amber,
          ),
          onRatingUpdate: (rating) {},
        ),
        const SizedBox(height: 10,),
        Text(
          desc,
          textAlign: TextAlign.center,
          style: GoogleFonts.lato(
            textStyle: Theme.of(context).textTheme.bodyText1,
            fontWeight: FontWeight.normal,
            letterSpacing: 3.5,
            color: Colors.white,
            height: calculateLineSpacing(19, 16)
          ),
        )
      ],
    );
  }

  /// Creates the container for displaying ingredients
  Widget _createIngredientsListContainer() {
    List<Ingredient> ingredientsList = recipe.ingredients;
    List<String> detectedIngredients = widget.ownedIngredients;

    return Container(
      padding: const EdgeInsets.symmetric(
        horizontal: horizontalPaddingInsideContainer,
        vertical: 20,
      ),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(20),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          IntrinsicHeight(
            child: Row(
              children: [
                Align(
                  alignment: Alignment.bottomCenter,
                  child: Text(
                    'INGREDIENTS',
                    style: Theme.of(context).textTheme.headline5!.copyWith(
                      color: brownColor1,
                      letterSpacing: 2.5,
                    ),
                  ),
                ),
                const SizedBox(width: 20,),
                Image.asset(
                  'assets/shopping-list.png',
                  width: 38,
                ),
              ],
            ),
          ),
          const Divider(
            color: brownColor1,
            thickness: 4,
          ),
          const SizedBox(height: 10,),
          ..._createIngredientWidgetList(ingredientsList, detectedIngredients),
        ],
      ),
    );
  }

  /// Create the widget list for ingredients to be added
  /// to the container
  List<Widget> _createIngredientWidgetList(
    List<Ingredient> ingredientsList,
    List<String> detectedIngredients,
  ) {
    List<Widget> widgets = [];

    for (Ingredient ingredient in ingredientsList) {
      widgets.add(Column(
        children: [
          IntrinsicHeight(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Align(
                  alignment: Alignment.topLeft,
                  child: Text(
                    '- ',
                    style: GoogleFonts.lato(
                        textStyle: Theme.of(context).textTheme.bodyText1,
                        fontWeight: FontWeight.normal,
                        color: darkGreyColor,
                        letterSpacing: 3.5
                    ),
                  ),
                ),
                Expanded(
                  child: Text(
                    '${ingredient.name} ${convertDoubleToString(ingredient.amount)} ${ingredient.unit}'.toUpperCase(),
                    textAlign: TextAlign.left,
                    maxLines: 10,
                    overflow: TextOverflow.ellipsis,
                    style: GoogleFonts.lato(
                      textStyle: Theme.of(context).textTheme.bodyText1,
                      fontWeight: FontWeight.normal,
                      letterSpacing: 3.5,
                      color: detectedIngredients.contains(ingredient.name.toUpperCase()) ? brownColor1 : darkGreyColor,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ));
      widgets.add(const SizedBox(
        height: 10,
      ));
    }

    return widgets;
  }

  /// Create the container to display ads
  Widget _createAdContainer() {
    return Container(
      decoration: BoxDecoration(
        color: greyColor,
        borderRadius: BorderRadius.circular(20),
      ),
      padding: const EdgeInsets.symmetric(vertical: 20),
      child: Center(
        child: Text(
          'AD',
          style: Theme.of(context).textTheme.subtitle1!.copyWith(
            color: greenColor1,
            letterSpacing: 2.5,
          ),
        ),
      ),
    );
  }

  /// Create the container for displaying the recipe steps
  Widget _createStepsContainer() {
    List<String> steps = recipe.steps;

    return Container(
      padding: const EdgeInsets.symmetric(
        horizontal: horizontalPaddingInsideContainer,
        vertical: 20,
      ),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(20),
      ),
      child: Column(
        children: [
          IntrinsicHeight(
            child: Row(
              children: [
                Align(
                  alignment: Alignment.bottomCenter,
                  child: Text(
                    'STEPS',
                    style: Theme.of(context).textTheme.headline5!.copyWith(
                      color: greenColor2,
                      letterSpacing: 2.5,
                    ),
                  ),
                ),
                const SizedBox(width: 20,),
                Padding(
                  padding: const EdgeInsets.only(
                    bottom: 10
                  ),
                  child: Image.asset(
                    'assets/stairs.png',
                    width: 38,
                  ),
                ),
              ],
            ),
          ),
          const Divider(
            color: greenColor1,
            thickness: 4,
          ),
          const SizedBox(height: 10,),
          for (int i=0; i<steps.length; i++) Column(
            children: [
              IntrinsicHeight(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Flexible(
                      flex: 1,
                      child: Align(
                        alignment: Alignment.topLeft,
                        child: Text(
                          '${i+1}.',
                          style: GoogleFonts.lato(
                            textStyle: Theme.of(context).textTheme.bodyText1,
                            fontWeight: FontWeight.normal,
                            color: darkGreyColor,
                            letterSpacing: 3.5
                          ),
                        ),
                      ),
                    ),
                    Flexible(
                      flex: 5,
                      child: Text(
                        steps[i],
                        style: GoogleFonts.lato(
                            textStyle: Theme.of(context).textTheme.bodyText1,
                            fontWeight: FontWeight.normal,
                            color: darkGreyColor,
                            letterSpacing: 3.5
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              const SizedBox(height: 20,),
            ],
          ),
        ],
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    recipe = widget.recipe.copyWith();
  }

  @override
  Widget build(BuildContext context) {
    KukieUser user = context.read<KukieUser?>()!;
    print(recipe.userRating);
    return createDefaultAppBody(
      context: context,
      backgroundColor: blueColor,
      bottomNavigationBar: const KukieBottomNavigationBar(),
      leading: createIconButtonForAppBar(
        onPressed: () {
          Navigator.pop(context);
        },
        iconColor: Colors.white,
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.fromLTRB(15, 0, 15, 20),
          child: Column(
            children: [
              _createTitleContainer(),
              const SizedBox(height: 20,),
              _createIngredientsListContainer(),
//          const SizedBox(height: 10,),
//          _createAdContainer(),
              const SizedBox(height: 20,),
              _createStepsContainer(),
              const SizedBox(height: 30,),
              RatingContainer(
                onCheckBoxTapped: (rating) async {
                  recipe.userRating = rating;

                  showLoading(context);

                  await updateRecipeRatingAndFavoriteStatus(
                    recipe: recipe,
                    user: user
                  ).then((value) {
                    Navigator.popUntil(context, (route) => route.isFirst);
                    Navigator.pushReplacement(
                      context,
                      createFadingPageRouteBuilder(child: const AuthEnforcer(
                        child: HomeScreen(),
                      )),
                    );
                  }).catchError((e) {
                    Navigator.pop(context);   // Pop loading
                    developer.log(
                      'Unable to update recipe rating and favorite status:',
                      error: e,
                      name: 'recipe_detail.dart'
                    );
                    Fluttertoast.showToast(
                      msg: 'Something went wrong, please try again'
                    );
                  });
                },
                initialRating: widget.recipe.userRating,
              ),
            ],
          ),
        ),
      ),
    );
  }
}

/// Container widget for the user to rate
/// the recipe
///
/// [onCheckBoxTapped] Callback to be called when the check box is
/// tapped.
///
/// [initialRating] Value to put on the rating bar initially.
class RatingContainer extends StatefulWidget {

  final Function(double rating) onCheckBoxTapped;
  final double initialRating;

  const RatingContainer({
    Key? key,
    required this.onCheckBoxTapped,
    this.initialRating = 0,
  }) : super(key: key);

  @override
  _RatingContainerState createState() => _RatingContainerState();
}

class _RatingContainerState extends State<RatingContainer> {

  bool submitTapped = false;
  late double rating;

  @override
  void initState() {
    super.initState();
    rating = widget.initialRating;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(
        horizontal: horizontalPaddingInsideContainer,
        vertical: 20,
      ),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(20),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(
            'RATE THIS RECIPE',
            style: Theme.of(context).textTheme.headline5!.copyWith(
              color: brownColor1,
              letterSpacing: 2.5,
            ),
          ),
          const SizedBox(height: 7,),
          RatingBar.builder(
            itemCount: 5,
            itemSize: 40,
            itemPadding: const EdgeInsets.symmetric(horizontal: 10),
            initialRating: rating,
            allowHalfRating: false,
            wrapAlignment: WrapAlignment.center,
            updateOnDrag: true,
            // ratingWidget: ratingWidget,
            itemBuilder: (context, _) => const Icon(
              Icons.star_rounded,
              color: Colors.amber,
            ),
            onRatingUpdate: (rating) {
              setState(() {
                this.rating = rating;
              });
            },
          ),
          const SizedBox(height: 10,),
          InkWell(
            child: Icon(
              submitTapped ? Icons.check_circle : Icons.check_circle_outline,
              size: 43,
              color: rating > 0? greenColor1 : greenColor1.withOpacity(0.4),
            ),
            onTap: () async {
              if (submitTapped) {
                widget.onCheckBoxTapped(rating);
              }
            },
            onTapCancel: () {
              setState(() {
                submitTapped = false;
              });
            },
            onTapDown: (details) {
              if (rating > 0 ) {
                setState(() {
                  submitTapped = true;
                });
              }
            },
          ),
        ],
      ),
    );
  }
}

/// Widget to display recipe like status
class FavoriteRecipe extends StatefulWidget {

  final bool initiallyLiked;
  final Function(bool isLiked) onLiked;

  const FavoriteRecipe({
    Key? key,
    required this.onLiked,
    this.initiallyLiked = false,
  }) : super(key: key);

  @override
  _FavoriteRecipeState createState() => _FavoriteRecipeState();
}

class _FavoriteRecipeState extends State<FavoriteRecipe> {

  late bool isLiked;

  @override
  void initState() {
    super.initState();
    isLiked = widget.initiallyLiked;
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: Padding(
        padding: const EdgeInsets.all(3.0),
        child: Icon(
          isLiked ? Icons.favorite : Icons.favorite_outline,
          color: isLiked ? Colors.red : Colors.black.withOpacity(0.43),
          size: 30,
        ),
      ),
      onTap: () {
        setState(() {
          isLiked = !isLiked;
        });
        widget.onLiked(isLiked);
      },
    );
  }
}

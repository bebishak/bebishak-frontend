import 'package:flutter/material.dart';
import 'package:flutter_frontend/screens/email_verify.dart';
import 'package:flutter_frontend/services/auth.dart';
import 'package:flutter_frontend/services/validator.dart';
import 'package:flutter_frontend/shared/constants.dart';
import 'package:flutter_frontend/shared/widget_prefabs.dart';
import 'package:flutter_frontend/widgets/loading.dart';

import 'dart:developer' as developer;

class RegisterScreen extends StatefulWidget {
  const RegisterScreen({Key? key}) : super(key: key);

  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  final _formKey = GlobalKey<FormState>();

  String email = '',
      username = '',
      password = '',
      confirmPassword = '',
      error = '';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false, // Allows on-screen keyboard without renderflex error
      backgroundColor: Colors.white,
      appBar: AppBar(
        // Back button
        backgroundColor: Colors.transparent,
        elevation: 0,
        automaticallyImplyLeading: false,
        leading: createIconButtonForAppBar(
            iconColor: brownColor1,
            onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      body:
          // Main form
          Form(
            key: _formKey,
            child: ListView(
              children: [
                // Hot image
                Padding(
                  padding: const EdgeInsets.only(right: 65.0),
                  child: Align(
                    alignment: Alignment.centerRight,
                    child: Image.asset(
                      'assets/hot.png',
                      fit: BoxFit.scaleDown,
                      height: 75,
                    ),
                  )
                ),
                SizedBox(
                  // Green rectangle with circular edges
                  child: Container(
                    margin: const EdgeInsets.fromLTRB(25.0, 0.0, 25.0, 0.0),
                    decoration: BoxDecoration(
                      color: greenColor2,
                      borderRadius: BorderRadius.circular(25.0),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black.withOpacity(0.45),
                          blurRadius: 25.0,
                          spreadRadius: -10.0,
                          offset: const Offset(2.0, 15.0)
                        )
                      ],
                    ),
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(25.0, 30.0, 25.0, 0.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          // EMAIL text
                          Text(
                            'EMAIL',
                            style: Theme.of(context).textTheme.bodyText1!.copyWith(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                            ),
                          ),

                          const SizedBox(height: 6.0,),  // Gap

                          // Email Field
                          TextFormField(
                            initialValue: email,
                            decoration: InputDecoration(
                                fillColor: Colors.white,
                                isDense: true,
                                filled: true,
                                contentPadding:
                                    const EdgeInsets.symmetric(vertical: 5.0, horizontal: 10.0),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(15.0),)),
                            validator: EmptyFieldValidator.validate,
                            onChanged: (val) => email = val,
                          ),

                          const SizedBox(height: 12.0,),  // Gap

                          // USERNAME text
                          Text(
                            'USERNAME',
                            style: Theme.of(context).textTheme.bodyText1!.copyWith(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                            ),
                          ),

                          const SizedBox(height: 6.0,),  // Gap

                          // Username Field
                          TextFormField(
                            initialValue: username,
                            decoration: InputDecoration(
                                fillColor: Colors.white,
                                isDense: true,
                                filled: true,
                                contentPadding:
                                    const EdgeInsets.symmetric(vertical: 5.0, horizontal: 10.0),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(15.0))),
                            validator: EmptyFieldValidator.validate,
                            onChanged: (val) => username = val,
                          ),

                          const SizedBox(height: 12.0,),  // Gap

                          // PASSWORD text
                          Text(
                            'PASSWORD',
                            style: Theme.of(context).textTheme.bodyText1!.copyWith(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                            ),
                          ),

                          const SizedBox(height: 6.0,),  // Gap

                          // Password Field
                          TextFormField(
                            initialValue: password,
                            decoration: InputDecoration(
                                fillColor: Colors.white,
                                isDense: true,
                                filled: true,
                                contentPadding:
                                    const EdgeInsets.symmetric(vertical: 5.0, horizontal: 10.0),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(15.0))),
                            validator: PasswordFieldValidator.validate,
                            obscureText: true,
                            onChanged: (val) => password = val,
                          ),

                          const SizedBox(height: 12.0,),  // Gap

                          // CONFIRM PASSWORD text
                          Text(
                            'CONFIRM PASSWORD',
                            style: Theme.of(context).textTheme.bodyText1!.copyWith(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                            ),
                          ),

                          const SizedBox(height: 6.0,),  // Gap

                          // Confirm Password Field
                          TextFormField(
                            initialValue: confirmPassword,
                            decoration: InputDecoration(
                                fillColor: Colors.white,
                                isDense: true,
                                filled: true,
                                contentPadding:
                                    const EdgeInsets.symmetric(vertical: 5.0, horizontal: 10.0),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(15.0))),
                            validator: (val) => ConfirmPasswordFieldValidator.validate(val, password),
                            obscureText: true,
                            onChanged: (val) => confirmPassword = val,
                          ),

                          const SizedBox(height: 15.0), // Gap

                          // REGISTER button
                          Align(
                            alignment: Alignment.bottomRight,
                            child: ElevatedButton(
                              style: ButtonStyle(
                                  backgroundColor:
                                      MaterialStateProperty.all<Color>(
                                          brownColor1),
                                  shape: MaterialStateProperty.all<
                                          RoundedRectangleBorder>(
                                      RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(15.0)))),
                              child: Text('REGISTER',
                              style:Theme.of(context).textTheme.button!.copyWith(
                                  color: Colors.white,
                                )
                              ),
                              onPressed: () async {
                                showLoading(context);
                                setState(() {
                                  error = '';
                                });
                                if (_formKey.currentState!.validate()) {
                                  AuthService auth = AuthService();
                                  await auth.register(
                                    email: email,
                                    password: password,
                                    username: username,
                                  ).then((value) {
                                    Navigator.popUntil(context, (route) => route.isFirst);
                                    Navigator.pushReplacement(
                                      context,
                                      createFadingPageRouteBuilder(child: const EmailVerifyScreen()));
                                  }).catchError((e) {
                                    Navigator.pop(context);   // Pop loading
                                    developer.log(
                                      'Unable to register user:',
                                      name: 'register.dart',
                                      error: e
                                    );
                                    setState(() {
                                      error = 'Something went wrong, please try again';
                                    });
                                  });
                                } else {
                                  Navigator.pop(context);
                                }
                              },
                            ),
                          ),
                          const SizedBox(height: 6.0,),  // Gap
                        ],
                      ),
                    ),
                  ),
                ),
          const SizedBox(height: 25.0), // Gap

          // Error Text
          Text(error,
            textAlign: TextAlign.center,
            style: Theme.of(context).textTheme.bodyText1!.copyWith(
              color: Colors.red,)
          ),

          const SizedBox(height: 15.0), // Gap

          // SIGN UP Text
          Center(
            child: Text(
              'SIGN UP',
              style: Theme.of(context).textTheme.subtitle1!.copyWith(
                color: blueColor,
              )
            ),
          ),
          // KUKIE Text
          Center(
            child: Text(
            'KUKIE',
              style: Theme.of(context).textTheme.headline1!.copyWith(
                color: greenColor1,
                fontWeight: FontWeight.bold,
                letterSpacing: 0,
                height: 1,
              ),
            ),
          ),
        ],
      ),
    ));
  }
}

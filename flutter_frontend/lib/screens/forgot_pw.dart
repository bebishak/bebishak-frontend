import 'package:flutter/material.dart';
import 'package:flutter_frontend/services/auth.dart';
import 'package:flutter_frontend/services/validator.dart';
import 'package:flutter_frontend/shared/constants.dart';
import 'package:flutter_frontend/shared/utility_functions.dart';
import 'package:flutter_frontend/shared/widget_prefabs.dart';
import 'package:flutter_frontend/widgets/loading.dart';
import 'package:fluttertoast/fluttertoast.dart';

import 'dart:developer' as developer;

class ForgotPassScreen extends StatefulWidget {
  const ForgotPassScreen({Key? key}) : super(key: key);

  @override
  _ForgotPassScreenState createState() => _ForgotPassScreenState();
}

class _ForgotPassScreenState extends State<ForgotPassScreen> {
  final _formKey = GlobalKey<FormState>();
  String email = '';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false, // Allows on-screen keyboard without renderflex error
      backgroundColor: Colors.white,
      appBar: AppBar(
        // Back button
        backgroundColor: Colors.transparent,
        elevation: 0,
        automaticallyImplyLeading: false,
        leading: createIconButtonForAppBar(
            iconColor: brownColor1,
            onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      body:
          // Main form
          Form(
            key: _formKey,
            child: ListView(
              children: [
                const SizedBox(height: 20.0,),  // Gap

                // Lock over rectangle stack
                Stack(
                  children: [
                    // White rectangle with circular edges
                    Container(
                      margin: const EdgeInsets.fromLTRB(25.0, 85.0, 25.0, 0.0),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(25.0),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.black.withOpacity(0.45),
                            blurRadius: 25.0,
                            spreadRadius: -10.0,
                            offset: const Offset(2.0, 15.0),
                          ),
                        ]
                      ),
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(25.0, 85.0, 25.0, 0.0),
                        child: Column(
                          children: [
                            Text(
                              'ENTER THE\nEMAIL ADDRESS\nFOR YOUR ACCOUNT',
                              textAlign: TextAlign.center,
                              style: Theme.of(context).textTheme.headline6!.copyWith(
                                color: greenColor1,
                              ),
                            ),

                            const SizedBox(height: 10.0,),  // Gap

                            // And we'll help you With the rest text
                            Text(
                              'AND WE\'LL HELP YOU\n WITH THE REST',
                              textAlign: TextAlign.center,
                              style: Theme.of(context).textTheme.caption!.copyWith(
                                color: greenColor1,
                              ),
                            ),

                            const SizedBox(height: 25.0,),  // Gap

                            // Email field
                            TextFormField(
                              initialValue: email,
                              decoration: InputDecoration(
                                fillColor: greyColor,
                                filled: true,
                                contentPadding: const EdgeInsets.symmetric(vertical: 0.0, horizontal: 10.0),
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(15.0),
                                  borderSide: BorderSide.none,
                                ),
                                hintText: 'YOUR EMAIL HERE',
                                hintStyle: Theme.of(context).textTheme.caption!.copyWith(
                                  color: darkGreyColor,
                                ),
                              ),
                              textAlign: TextAlign.center,
                              validator: EmptyFieldValidator.validate,
                              onChanged: (val) => email = val,
                            ),

                            const SizedBox(height: 30.0,),  // Gap

                            // SEND button
                            Padding(
                              padding: const EdgeInsets.fromLTRB(65.0, 0.0, 65.0, 0.0),
                              child: ElevatedButton(
                                style: ButtonStyle(
                                    backgroundColor:
                                        MaterialStateProperty.all<Color>(
                                            greenColor1),
                                    shape: MaterialStateProperty.all<
                                            RoundedRectangleBorder>(
                                        RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(15.0)))),
                                child: Text('SEND',
                                style:Theme.of(context).textTheme.button!.copyWith(
                                    color: Colors.white,
                                  )
                                ),
                                onPressed: () async {
                                  showLoading(context);
                                  AuthService auth = AuthService();
                                  await auth.resetPassword(email).then((value) {
                                    Navigator.pop(context);   // Pop loading
                                    Navigator.pop(context);   // Pop forget password
                                    Fluttertoast.showToast(msg: 'Please check your email to reset your password');
                                  }).catchError((e) {
                                    developer.log(
                                      'Unable to send reset password email',
                                      error: e,
                                      name: 'forgot_pw.dart'
                                    );
                                    Fluttertoast.showToast(
                                      msg: 'Something went wrong, please try again',
                                    );
                                  });
                                },
                              ),
                            ),
                          const SizedBox(height: 10.0), // Gap
                          ],
                        ),
                      ),
                    ),
                    // Lock image
                    Container(
                      alignment: Alignment.topCenter,
                      child: Image.asset(
                        'assets/lock.png',
                        fit: BoxFit.scaleDown,
                        height: 136.0,
                      ),
                    ),
                  ],
                ),
          const SizedBox(height: 50.0), // Gap

          // FORGOT PASSWORD Text
          Center(
            child: Text(
              'FORGOT PASSWORD',
              style: Theme.of(context).textTheme.subtitle2!.copyWith(
                fontSize: 16,
                color: blueColor,
                letterSpacing: 1.6,
                height: calculateLineSpacing(16, 32),
                wordSpacing: calculateLineSpacing(16, 32)
              )
            ),
          ),
          // KUKIE Text
          Center(
            child: Text(
            'KUKIE',
              style: Theme.of(context).textTheme.headline1!.copyWith(
                color: greenColor1,
                fontWeight: FontWeight.bold,
                letterSpacing: 0,
                height: 1,
              )
            ),
          ),
        ],
      ),
    ));
  }
}

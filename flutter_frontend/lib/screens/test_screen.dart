import 'package:flutter/material.dart';
import 'package:flutter_frontend/shared/constants.dart';
import 'package:flutter_frontend/shared/widget_prefabs.dart';

class TestScreen extends StatefulWidget {
  const TestScreen({Key? key}) : super(key: key);

  @override
  State<TestScreen> createState() => _TestScreenState();
}

class _TestScreenState extends State<TestScreen> {

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return createDefaultAppBody(
      context: context,
      backgroundColor: Colors.white,
      leading: createIconButtonForAppBar(
          iconColor: brownColor1,
          onPressed: () {
            // TODO: Go back to profile page
          }
      ),
      body: Form(
        key: _formKey,
        child: Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 25
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              const SizedBox(height: 25,),
              Padding(
                padding: const EdgeInsets.only(left: 25),
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: Image.asset(
                    'assets/dish.png',
                    fit: BoxFit.scaleDown,
                    height: 75,
                  ),
                ),
              ),
              Container(
                padding: const EdgeInsets.all(50),
                decoration: BoxDecoration(
                  color: Colors.blue,
                  borderRadius: BorderRadius.circular(25),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black.withOpacity(0.45),
                      blurRadius: 25,
                      spreadRadius: 3,
                      offset: const Offset(0, 10)
                    )
                  ]
                ),
                child: const Text("Container"),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

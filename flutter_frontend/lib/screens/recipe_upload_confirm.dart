import 'package:flutter/material.dart';
import 'package:flutter_frontend/screens/buffer_screen.dart';
import 'package:flutter_frontend/shared/constants.dart';
import 'package:flutter_frontend/shared/utility_functions.dart';

class ConfirmUploadRecipeScreen extends StatelessWidget {
  const ConfirmUploadRecipeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BufferScreen(
      child: Padding(
        padding: const EdgeInsets.fromLTRB(90, 0, 90, 30),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              'WE\'RE READY TO UPLOAD.\nARE YOU?',
              textAlign: TextAlign.center,
              style: Theme.of(context).textTheme.subtitle1!.copyWith(
                color: brownColor1,
                letterSpacing: 2,
                height: calculateLineSpacing(20, 32),
                wordSpacing: calculateLineSpacing(20, 32),
              ),
            ),
            const SizedBox(height: 20,),
            Image.asset(
              'assets/cooking-pot.png',
              fit: BoxFit.scaleDown,
              width: 100,
            ),
            const SizedBox(height: 20,),
            ElevatedButton(
              onPressed: () {
                Navigator.pop(context, false);
              },
              child: Text(
                'BACK TO EDITING',
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.caption!.copyWith(
                  letterSpacing: 2.85,
                  color: Colors.white,
                  wordSpacing: calculateLineSpacing(13, 16),
                  height: calculateLineSpacing(13, 16),
                ),
              ),
              style: roundedButton.copyWith(
                backgroundColor: getMaterialColorProperty(
                    Theme.of(context).colorScheme.primary
                ),
              ),
            ),
            const SizedBox(height: 10),
            ElevatedButton(
              onPressed: () async {
                Navigator.pop(context, true);   // Pop this screen
              },
              child: Text(
                'UPLOAD NOW',
                style: Theme.of(context).textTheme.caption!.copyWith(
                  letterSpacing: 2.85,
                  color: Colors.white,
                  wordSpacing: calculateLineSpacing(13, 16),
                  height: calculateLineSpacing(13, 16),
                ),
              ),
              style: roundedButton.copyWith(
                  backgroundColor: getMaterialColorProperty(greenColor1)
              ),
            ),
          ],
        ),
      ),
      bottomImage: Image.asset(
        'assets/mittens2.png',
        key: const Key('mitten'),
        fit: BoxFit.scaleDown,
      ),
    );
  }
}


import 'package:flutter/material.dart';
import 'package:flutter_frontend/screens/start.dart';
import 'package:flutter_frontend/shared/constants.dart';
import 'package:flutter_frontend/shared/widget_prefabs.dart';
import 'package:flutter_frontend/shared/utility_functions.dart';

import 'dart:developer' as developer;


class EmailVerifyScreen extends StatefulWidget {
  const EmailVerifyScreen({Key? key}) : super(key: key);

  @override
  _EmailVerifyScreenState createState() => _EmailVerifyScreenState();
}

class _EmailVerifyScreenState extends State<EmailVerifyScreen> {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,  // Allows on-screen keyboard without renderflex error
      backgroundColor: Colors.white,
      appBar: AppBar(
        // Back button
        backgroundColor: Colors.transparent,
        elevation: 0,
        automaticallyImplyLeading: false,
        // leading: createIconButtonForAppBar(
        //     iconColor: brownColor1,
        //     onPressed: () {
        //     Navigator.pop(context);
        //   },
        // ),
      ),
      body:
          // Main form
          Form(
            key: _formKey,
            child: ListView(
              children: [
                const SizedBox(height: 85.0,),  // Gap

                // Mail over rectangle stack
                Stack(
                  children: [
                    // White rectangle with circular edges
                    Container(
                      height: 350,
                      width: 400,
                      margin: const EdgeInsets.fromLTRB(25.0, 85.0, 25.0, 0.0),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(25.0),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.black.withOpacity(0.45),
                            blurRadius: 25.0,
                            spreadRadius: -10.0,
                            offset: const Offset(2.0, 15.0),
                          ),
                        ]
                      ),
                      child: ListView(
                        padding: const EdgeInsets.fromLTRB(25.0, 85.0, 25.0, 0.0),
                        children: [
                          // Verify Your Account text
                          Text(
                            'VERIFY YOUR\nACCOUNT',
                            textAlign: TextAlign.center,
                            style: Theme.of(context).textTheme.headline6!.copyWith(
                              color: greenColor1,
                            ),
                          ),

                          const SizedBox(height: 10.0,),  // Gap

                          // We'vesent you an email! Click the link provided to activate your Account! text
                          Text(
                            'WE\'VE SENT YOU AN EMAIL!\nCLICK THE LINK PROVIDED TO\nACTIVATE YOUR ACCOUNT!',
                            textAlign: TextAlign.center,
                            style: Theme.of(context).textTheme.caption!.copyWith(
                              color: greenColor1,
                            ),
                          ),

                          const SizedBox(height: 85.0,),  // Gap

                          // OK button
                          Padding(
                            padding: const EdgeInsets.fromLTRB(65.0, 0.0, 65.0, 0.0),
                            child: ElevatedButton(
                              style: ButtonStyle(
                                  backgroundColor:
                                      MaterialStateProperty.all<Color>(
                                          greenColor1),
                                  shape: MaterialStateProperty.all<
                                          RoundedRectangleBorder>(
                                      RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(15.0)))),
                              child: Text('OK',
                              style:Theme.of(context).textTheme.button!.copyWith(
                                  color: Colors.white,
                                )
                              ),
                              onPressed: () async {
                                Navigator.pushReplacement(
                                  context,
                                  createFadingPageRouteBuilder(child: const StartScreen()));
                              },
                            ),
                          ),
                        ],
                      ),
                    ),
                    // Mail image
                    Container(
                      alignment: Alignment.topCenter,
                      child: Image.asset(
                        'assets/mail.png',
                        fit: BoxFit.scaleDown,
                        height: 136.0,
                      ),
                    ),
                  ],
                ),
          const SizedBox(height: 30.0), // Gap

          // SIGN UP Text
          Center(
            child: Text(
              'SIGN UP',
              style: Theme.of(context).textTheme.subtitle2!.copyWith(
                fontSize: 16,
                color: blueColor,
                letterSpacing: 1.6,
                height: calculateLineSpacing(16, 32),
                wordSpacing: calculateLineSpacing(16, 32)
              )
            ),
          ),
          // KUKIE Text
          Center(
            child: Text(
            'KUKIE',
              style: Theme.of(context).textTheme.headline1!.copyWith(
                color: greenColor1,
                fontWeight: FontWeight.bold,
                letterSpacing: 0,
                height: 1,
              )
            ),
          ),
        ],
      ),
    ));
  }
}
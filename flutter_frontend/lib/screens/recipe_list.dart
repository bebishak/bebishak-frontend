import 'package:flutter/material.dart';
import 'package:flutter_frontend/models/models.dart';
import 'package:flutter_frontend/services/backend.dart';
import 'package:flutter_frontend/shared/constants.dart';
import 'package:flutter_frontend/shared/widget_prefabs.dart';
import 'package:flutter_frontend/widgets/auth_enforcer.dart';
import 'package:flutter_frontend/widgets/kukie_bottom_nav_bar.dart';
import 'package:flutter_frontend/widgets/recipe_search.dart';
import 'package:flutter_frontend/widgets/recipe_tile.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class RecipeListScreen extends StatefulWidget {

  final List<String> ingredients;
  final String userUid;

  const RecipeListScreen({
    Key? key,
    this.ingredients = const [],
    required this.userUid
  }) : super(key: key);

  @override
  _RecipeListScreenState createState() => _RecipeListScreenState();
}

class _RecipeListScreenState extends State<RecipeListScreen> {

  final TextEditingController _inputController = TextEditingController();
  late Future<List<Recipe>> recipesFuture;
  RecipeFilterData prevFilter = RecipeFilterData();

  Widget _createTestRecipeList() => ListView.builder(
    itemCount: 4,
    itemBuilder: (context, index) {
      Recipe recipe = Recipe(
        name: 'food name',
        username: 'username',
        authorUid: 'authorUid',
        desc: 'three times did the cheese move sideways to Switzerland by radio some garble woohoo wologigngo',
        isLiked: false,
        rateCount: 20,
        globalRating: 4,
      );
      return RecipeTile(
        recipe: recipe,
      );
    }
  );

  @override
  void initState() {
    super.initState();
    recipesFuture = getRecipes(
      ingredients: widget.ingredients,
      userUid: widget.userUid
    );
  }

  @override
  void dispose() {
    _inputController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AuthEnforcer(
      informUserWhenSessionExpire: true,
      child: createDefaultAppBody(
        context: context,
        backgroundColor: blueColor,
        bottomNavigationBar: const KukieBottomNavigationBar(),
        leading: createIconButtonForAppBar(
          iconColor: Colors.white,
          onPressed: () {
            Navigator.pop(context);
          }
        ),
        body: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              RecipeSearch(
                filterInfo: prevFilter,
                inputController: _inputController,
                onSearch: (RecipeFilterData filters) {
                  if (prevFilter != filters) {
                    setState(() {
                      prevFilter = filters;
                      recipesFuture = getRecipes(
                        ingredients: widget.ingredients,
                        userUid: widget.userUid,
                      );
                    });
                  }
                }
              ),
              const SizedBox(height: 20,),
              Expanded(
                child: FutureBuilder<List<Recipe>>(
                  future: recipesFuture,
                  builder: (context, snapshot) {

                    bool isDoneLoading = snapshot.connectionState == ConnectionState.done;
                    List<Recipe> recipes = [];
                    if (isDoneLoading && snapshot.hasData) {
                      recipes = snapshot.data ?? [];
                      return ListView.builder(
                        itemCount: recipes.length,
                        itemBuilder: (context, index) => RecipeTile(
                          recipe: recipes[index],
                          ownedIngredients: widget.ingredients,
                        ),
                      );
                    } else {
                      return const Center(
                        child: SpinKitRing(
                          color: greenColor1,
                        ),
                      );
                    }
                  }
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

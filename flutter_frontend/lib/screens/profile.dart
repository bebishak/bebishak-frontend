import 'package:contained_tab_bar_view/contained_tab_bar_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter_frontend/models/models.dart';
import 'package:flutter_frontend/screens/recipe_detail.dart';
import 'package:flutter_frontend/screens/recipe_form.dart';
import 'package:flutter_frontend/services/auth.dart';
import 'package:flutter_frontend/services/backend.dart';
import 'package:flutter_frontend/shared/constants.dart';
import 'package:flutter_frontend/shared/utility_functions.dart';
import 'package:flutter_frontend/shared/widget_prefabs.dart';
import 'package:flutter_frontend/widgets/auth_enforcer.dart';
import 'package:flutter_frontend/widgets/kukie_bottom_nav_bar.dart';
import 'package:flutter_frontend/widgets/recipe_search.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';

import 'dart:developer' as developer;

class ProfileScreen extends StatefulWidget {
  const ProfileScreen({Key? key}) : super(key: key);

  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {

  final double lowerSectionPadding = 15;
  RecipeFilterData filterData =  RecipeFilterData();

  Widget _createProfileInfo(String username) => Row(
    mainAxisAlignment: MainAxisAlignment.center,
    children: [
      createImageContainer(
        height: 90,
        width: 90,
        padding: const EdgeInsets.symmetric(
          vertical: 7,
          horizontal: 10,
        ),
        borderRadius: BorderRadius.circular(20),
        backgroundColor: const Color.fromRGBO(238, 219, 213, 1),
        boxShadow: const [
          BoxShadow(
            color: Color.fromRGBO(0, 0, 0, 0.2),
            blurRadius: 4,
            spreadRadius: 1,
            offset: Offset(0, 2),
          )
        ],
        image: createImageWithPlaceholderWidget(
          placeholderPath: 'assets/worker.png',
          imgUrl: '',   // TODO: Change to variable
          imgSize: 90,
        ),
      ),
      Container(
        padding: const EdgeInsets.symmetric(
          vertical: 10,
          horizontal: 10,
        ),
        decoration: const BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
            topRight: Radius.circular(10),
            bottomRight: Radius.circular(10),
          ),
        ),
        child: Text(
          '@$username',
          overflow: TextOverflow.ellipsis,
          style: GoogleFonts.lato(
            textStyle: Theme.of(context).textTheme.bodyText1,
            fontWeight: FontWeight.bold,
            color: brownColor1,
          ),
        ),
      ),
    ],
  );

  Widget _createTopSection(KukieUser user) => Stack(
    children: [
      Container(
        height: 150,
        decoration: const BoxDecoration(
          color: Colors.amber,
          image: DecorationImage(
            image: AssetImage('assets/profile_background.jpg'),
            fit: BoxFit.cover,
          ),
          borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(45),
          )
        ),
      ),
      Column(
        children: [
          const SizedBox(height: 105,),
          _createProfileInfo(user.username),
        ],
      ),
    ],
  );

  @override
  Widget build(BuildContext context) {
    KukieUser? user = context.watch<KukieUser?>();
    return AuthEnforcer(
      informUserWhenSessionExpire: true,
      child: createDefaultAppBody(
        context: context,
        backgroundColor: blueColor,
        bottomNavigationBar: const KukieBottomNavigationBar(),
        extendBodyBehindAppBar: true,
        trailing: createIconButtonForAppBar(
          icon: Icons.login_outlined,
          iconColor: Colors.white,
          onPressed: () async {
            // Navigator.push(
            //   context,
            //   createFadingPageRouteBuilder(
            //     child: const SettingsScreen(),
            //   ),
            // );
            AuthService auth = AuthService();
            await auth.logout().catchError((e) {
              developer.log(
                'Unable to logout user:',
                error: e,
                name: 'profile.dart'
              );
              Fluttertoast.showToast(msg: 'Unable to logout due to an error');
            });
          }
        ),
        body: Column(
          children: [
            _createTopSection(user ?? UninitializedKukieUser()),
            const SizedBox(height: 10,),
            Padding(
              padding: EdgeInsets.symmetric(
                horizontal: lowerSectionPadding,
              ),
              child: RecipeSearch(
                onSearch: (filterInfo) {
                  // Only rebuild widgets if there are changes to the filter
                  // information
                  if (filterData != filterInfo) {
                    setState(() {
                      filterData = filterInfo;
                    });
                  }
                },
              ),
            ),
            const SizedBox(height: 10,),
            FutureBuilder(
              future: Future.wait([
                if (user != null) getRecipes(
                  userUid: user.uid,
                  filterOptions: filterData,
                ),    // My Recipes
                if (user != null)getRecipes(
                  userUid: user.uid,
                  isLiked: true,
                  filterOptions: filterData,
                )   // Liked recipes
              ]),
              builder: (BuildContext context, AsyncSnapshot<List<dynamic>> snapshot) {
                bool futureDone = snapshot.connectionState == ConnectionState.done;
                List<Recipe> myRecipes = [];
                List<Recipe> likedRecipes = [];

                if (futureDone && !snapshot.hasError && snapshot.hasData) {
                  myRecipes.addAll(snapshot.data![0] as List<Recipe>);
                  likedRecipes.addAll(snapshot.data![1] as List<Recipe>);
                }

                return ProfileRecipeTabView(
                  futureDone: futureDone,
                  likedRecipes: likedRecipes,
                  myRecipes: myRecipes,
                  onRecipeListRefresh: () {
                    setState(() {});
                  },
                  onDelete: () {
                    setState(() {});
                  }
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}

class ProfileRecipeTabView extends StatefulWidget {

  final List<Recipe> myRecipes, likedRecipes;
  final bool futureDone;
  final Function() onRecipeListRefresh;
  final Function() onDelete;

  const ProfileRecipeTabView({
    Key? key,
    required this.myRecipes,
    required this.likedRecipes,
    required this.futureDone,
    required this.onRecipeListRefresh,
    required this.onDelete,
  }) : super(key: key);

  @override
  _ProfileRecipeTabViewState createState() => _ProfileRecipeTabViewState();
}

class _ProfileRecipeTabViewState extends State<ProfileRecipeTabView> {
  /// The index of the tab bar currently being selected.
  int tabIndex = 0;
  final int maxRecipeCount = 20;

  final Widget loadingWidget = const Center(
    child:  SpinKitRing(
      color: greenColor1,
    ),
  );

  TextStyle tabBarTextStyle = TextStyle(
    fontFamily: 'LeagueSpartan',
    fontWeight: FontWeight.bold,
    color: greenColor1,
    letterSpacing: 1.6,
    height: calculateLineSpacing(16, 32),
    wordSpacing: calculateLineSpacing(16, 32),
  );

  Widget _createRecipeListBody({
    BorderRadius? borderRadius,
    Widget? child,
    int index = 0,
  }) => Container(
    margin: const EdgeInsets.fromLTRB(15, 5, 15, 0
    ),
    decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: borderRadius ?? const BorderRadius.only(
          topLeft: Radius.circular(35),
          topRight: Radius.circular(35),
        ),
        boxShadow: const [
          BoxShadow(
            color: Color.fromRGBO(0, 0, 0, 0.15),
            blurRadius: 3,
            spreadRadius: 1,
          )
        ]
    ),
    child: Padding(
      padding: const EdgeInsets.fromLTRB(
          25, 0, 25, 0
      ),
      child: child,
    ),
  );

  Widget _createMyRecipesListWidget(List<Recipe> recipes) => ListView.builder(
      itemCount: recipes.length + 1, // Add by 1 for recipe count widget
      itemBuilder: (context, index) {
        if (index == 0) {
          return Padding(
            padding: const EdgeInsets.only(
                top: 20,
                left: 12,
                bottom: 10
            ),
            child: Row(
              children: [
                _createRecipeCountWidget(recipes.length),
                Expanded(
                    child: InkWell(
                      child: const Align(
                        alignment: Alignment.centerRight,
                        child: Icon(
                          Icons.add_circle_outline,
                          color: greenColor1,
                        ),
                      ),
                      onTap: () async {
                        if (recipes.length < maxRecipeCount) {
                          var response = await Navigator.push(
                              context,
                              createFadingPageRouteBuilder(
                                  child: const AuthEnforcer(child: RecipeFormScreen())
                              )
                          );
                          if (response == true) {
                            widget.onRecipeListRefresh();
                          }
                        } else {
                          Fluttertoast.showToast(
                            msg: 'Unable to add new recipes'
                          );
                        }
                      },
                    )
                )
              ],
            ),
          );
        } else {
          return ProfileRecipeTile(
            recipe: recipes[index - 1],  // Index 0 is used for recipe count,
            showAuthor: false,
            tapWillEditRecipe: true,
            performIfChangeInRecipe: widget.onRecipeListRefresh,
            onDelete: widget.onDelete,
          );
        }
      }
  );

  Widget _createRecipeCountWidget(int count, {
    int? max,
  }) {
    max ??= maxRecipeCount;
    return Container(
      padding: const EdgeInsets.symmetric(
        vertical: 3,
        horizontal: 7,
      ),
      decoration: BoxDecoration(
        color: brownColor2,
        borderRadius: BorderRadius.circular(5),
      ),
      child: Text(
        max > 0 ? '$count/$max' : '$count',
        style: Theme.of(context).textTheme.bodyText1!.copyWith(
          color: Colors.white,
        ),
      ),
    );
  }

  Widget _createLikedRecipesListWidget(List<Recipe> recipes) => ListView.builder(
      itemCount: recipes.length + 1,
      itemBuilder: (context, index) {
        if (index == 0) {
          return Padding(
            padding: const EdgeInsets.only(
                top: 20,
                left: 12,
                bottom: 10
            ),
            child: Row(
              children: [
                _createRecipeCountWidget(recipes.length, max: -1),
              ],
            ),
          );
        } else {
          return ProfileRecipeTile(
            recipe: recipes[index - 1],
            showAuthor: true,
            backgroundColor: brownColor2,
            authorTextColor: brownColor1,
          );
        }
      }
  );

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Padding(
        padding: const EdgeInsets.symmetric(
          horizontal: 0,
        ),
        child: ContainedTabBarView(
          tabBarProperties: TabBarProperties(
            indicator: ContainerTabIndicator(
              radius: BorderRadius.circular(20),
              color: Colors.transparent,
            ),
          ),
          onChange: (index) {
            setState(() {
              tabIndex = index;
            });
          },
          callOnChangeWhileIndexIsChanging: true,
          tabs: [
            Container(
              padding: const EdgeInsets.symmetric(
                vertical: 10,
                horizontal: 10,
              ),
              decoration: BoxDecoration(
                color: tabIndex == 0 ? greenColor1 : Colors.white,
                borderRadius: BorderRadius.circular(20),
              ),
              child: Text(
                'YOUR RECIPES',
                style: tabBarTextStyle.copyWith(
                  color: tabIndex == 0 ? Colors.white : greenColor1,
                ),
              ),
            ),
            Container(
              padding: const EdgeInsets.symmetric(
                vertical: 10,
                horizontal: 10,
              ),
              decoration: BoxDecoration(
                color: tabIndex == 1 ? greenColor1 : Colors.white,
                borderRadius: BorderRadius.circular(20),
              ),
              child: Text(
                'LIKED RECIPES',
                style: tabBarTextStyle.copyWith(
                  color: tabIndex == 1 ? Colors.white : greenColor1,
                ),
              ),
            ),
          ],
          views: [
            _createRecipeListBody(
              child: MediaQuery.removePadding(
                  context: context,
                  removeTop: true,
                  child: widget.futureDone ?
                  _createMyRecipesListWidget(widget.myRecipes) :
                  loadingWidget
              ),
            ),
            _createRecipeListBody(
              child: MediaQuery.removePadding(
                context: context,
                removeTop: true,
                child: widget.futureDone ?
                _createLikedRecipesListWidget(widget.likedRecipes) :
                loadingWidget,
              ),
            ),
          ],
        )
      ),
    );
  }
}


class ProfileRecipeTile extends StatelessWidget {

  final double borderRadius = 20;
  final double horizontalPadding = 10;
  final double imgWidth;
  final Color backgroundColor;
  final Color authorTextColor;
  final bool showAuthor;
  final Recipe recipe;
  final bool tapWillEditRecipe;
  final Function()? performIfChangeInRecipe;
  final Function()? onDelete;

  const ProfileRecipeTile({
    Key? key,
    required this.recipe,
    this.imgWidth = 94,
    this.backgroundColor = greenColor2,
    this.showAuthor = true,
    this.authorTextColor = brownColor1,
    this.tapWillEditRecipe = false,
    this.performIfChangeInRecipe,
    this.onDelete,
  }) : super(key: key);

  void _performTapOperation(BuildContext context) async {
    dynamic response = false;
    if (tapWillEditRecipe) {
      response = await Navigator.push(
          context,
          createFadingPageRouteBuilder(
              child: AuthEnforcer(child: RecipeFormScreen(
                previousRecipe: recipe,
                onDelete: onDelete
              ))
          )
      );
    } else {
      response = await Navigator.push(
        context,
        createFadingPageRouteBuilder(child: RecipeScreen(
          recipe: recipe,
        )),
      );
    }

    if (response == true && performIfChangeInRecipe != null) {
      performIfChangeInRecipe!();
    }
  }

  @override
  Widget build(BuildContext context) {

    String foodName = recipe.name,
            username = recipe.username,
            desc = recipe.desc,
            imgUrl = recipe.imgUrl ?? '';
    double rating = recipe.globalRating;

    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          const SizedBox(width: 5,),
          Flexible(
            flex: 1,
            child: InkWell(
              onTap: () {
                _performTapOperation(context);
              },
              child: Container(
                padding: const EdgeInsets.all(8.0),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(borderRadius),
                    color: Colors.white,
                    boxShadow: const [
                      BoxShadow(
                        color: Color.fromRGBO(0, 0, 0, 0.15),
                        blurRadius: 5,
                        spreadRadius: 3,
                        offset: Offset(0, 1),
                      )
                    ]
                ),
                child: createImageWithPlaceholderWidget(
                    placeholderPath: 'assets/dish.png',
                    imgUrl: imgUrl,
                    imgSize: imgWidth
                ),
              ),
            ),
          ),
          const SizedBox(width: 10,),
          Flexible(
            flex: 2,
            child: InkWell(
              onTap: () {
                _performTapOperation(context);
              },
              child: Container(
                padding: const EdgeInsets.symmetric(
                  vertical: 10,
                  horizontal: 15,
                ),
                decoration: BoxDecoration(
                  color: backgroundColor,
                  borderRadius: BorderRadius.circular(borderRadius),
                  boxShadow: const [
                    BoxShadow(
                      color: Color.fromRGBO(0, 0, 0, 0.15),
                      blurRadius: 5,
                      spreadRadius: 3,
                      offset: Offset(0, 1),
                    )
                  ],
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Text(
                      foodName.toUpperCase(),
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                        fontFamily: 'LeagueSpartan',
                        color: Colors.white,
                        fontSize: 16,
                        letterSpacing: 3.5,
                        height: calculateLineSpacing(16, 21),
                        wordSpacing: calculateLineSpacing(16, 21),
                      ),
                    ),
                    RatingBar.builder(
                      allowHalfRating: false,
                      initialRating: rating,
                      ignoreGestures: true,
                      itemSize: 23,
                      itemBuilder: (context, _) => const Icon(
                        Icons.star_rounded,
                        color: Colors.amber,
                      ),
                      onRatingUpdate: (rating) {},
                    ),
                    if (showAuthor) Text(
                      'By $username'.toUpperCase(),
                      style: Theme.of(context).textTheme.caption!.copyWith(
                        color: authorTextColor,
                        letterSpacing: 2.85,
                        wordSpacing: calculateLineSpacing(13, 16),
                        height: calculateLineSpacing(13, 16),
                      ),
                    ),
                    Text(
                      desc,
                      maxLines: 2,
                      style: Theme.of(context).textTheme.caption!.copyWith(
                        color: Colors.white,
                        letterSpacing: 1.2,
                        height: calculateLineSpacing(13, 16),
                        wordSpacing: calculateLineSpacing(13, 16),
                      ),
                      overflow: TextOverflow.ellipsis,
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

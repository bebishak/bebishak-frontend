import 'package:flutter/material.dart';
import 'package:flutter_frontend/models/models.dart';
import 'package:flutter_frontend/screens/processing.dart';
import 'package:flutter_frontend/screens/recipe_list.dart';
import 'package:flutter_frontend/services/image_picker.dart';
import 'package:flutter_frontend/shared/constants.dart';
import 'package:flutter_frontend/shared/utility_functions.dart';
import 'package:flutter_frontend/shared/widget_prefabs.dart';
import 'package:flutter_frontend/widgets/kukie_bottom_nav_bar.dart';
import 'package:google_fonts/google_fonts.dart';

import 'dart:developer' as developer;

import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';

class IngredientsScreen extends StatefulWidget {

  final List<String> detectedIng;

  const IngredientsScreen({
    Key? key,
    this.detectedIng = const [],
  }) : super(key: key);

  @override
  _IngredientsScreenState createState() => _IngredientsScreenState();
}

class _IngredientsScreenState extends State<IngredientsScreen> {

  late TextEditingController _inputController;
  List<String> ingredients = [];

  /// Add the inputted ingredient
  void addIngredient(String ingredient) {
    ingredient = ingredient.trim().toUpperCase();
    if (ingredient.isNotEmpty) {
      if (!ingredients.contains(ingredient)) {
        ingredients.add(ingredient);
        ingredients.sort();
        developer.log(
            '$ingredient added to ingredients list',
            name: 'ingredients.dart'
        );
      }
    } else {
      developer.log(
          'Ingredient not added because it is empty or already exists in the list',
          name: 'ingredients.dart'
      );
    }
  }

  /// Adds a group of ingredients
  void addIngredients(List<String> ingredients) {
    ingredients = ingredients.map((ingredient) => ingredient.toUpperCase()).toList();
    this.ingredients.addAll(ingredients);
    // Sort
    this.ingredients.sort();
  }

  /// Create the widget tile for added ingredient
  Widget createIngredientItemTile({
    required String item,
    bool canDelete = false,
    Function()? onDelete,
    Color? backgroundColor,
  }) => Container(
    padding: EdgeInsets.only(
      left: 15,
      top: canDelete ? 0 : 10,
      bottom: canDelete ? 0 : 10,
    ),
    decoration: BoxDecoration(
      color: backgroundColor ?? brownColor1,
      borderRadius: BorderRadius.circular(25),
      boxShadow: const [
        BoxShadow(
            color: Color.fromRGBO(0, 0, 0, 0.1),
            offset: Offset(0, 4),
            blurRadius: 5
        ),
      ]
    ),
    child: IntrinsicWidth(
      child: Row(
        children: [
          Text(
            item.toUpperCase(),
            style: GoogleFonts.lato(
              textStyle: Theme.of(context).textTheme.bodyText1,
              color: Colors.white,
              letterSpacing: 3.5,
              fontWeight: FontWeight.normal,
            ),
          ),
          if (canDelete) IconButton(
            icon: const Icon(
              Icons.clear,
              size: 15,
              color: Colors.white,
            ),
            onPressed: onDelete,
          ),
        ],
      ),
    ),
  );

  /// Create the widget to display the list of ingredients
  Widget createIngredientsListWidget({
    required List<String> ingredients,
    bool canDelete = false,
  }) {
    List<Widget> widgets = [];

//    ingredients = ingredients.toSet().toList();

    for (String ingredient in ingredients) {
      widgets.add(createIngredientItemTile(
        item: ingredient,
        canDelete: canDelete,
        onDelete: () {
          setState(() {
            ingredients.remove(ingredient);
          });
        }
      ));
    }

    return Wrap(
      runSpacing: 10,
      spacing: 10,
      direction: Axis.horizontal,
      children: widgets,
    );
  }

  /// Creates the widget to input ingredients
  Widget _createInputWidget() => Row(
    mainAxisAlignment: MainAxisAlignment.center,
    children: [
      Expanded(
        child: Container(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(25),
            color: Colors.white,
            boxShadow: const [
              BoxShadow(
                offset: Offset(0, 3),
                color: Color.fromRGBO(0, 0, 0, 0.15),
                blurRadius: 5,
                spreadRadius: 1,
              ),
            ],
          ),
          child: TextField(
            controller: _inputController,
            style: GoogleFonts.lato(
                textStyle: Theme.of(context).textTheme.subtitle2,
                fontWeight: FontWeight.normal,
                letterSpacing: 1.9,
                color: brownColor1,
            ),
            decoration: InputDecoration(
              contentPadding: const EdgeInsets.symmetric(vertical: 0),
              border: InputBorder.none,
              hintText: 'add more ingredients',
              hintStyle: GoogleFonts.lato(
                textStyle: Theme.of(context).textTheme.subtitle2,
                fontWeight: FontWeight.normal,
                letterSpacing: 1.9,
                color: greyColor,
              ),
            ),
            onSubmitted: (String val) {
              setState(() {
                _inputController.text = '';
                addIngredient(val);
              });
            },
          ),
        ),
      ),
      const SizedBox(width: 10,),
      Container(
        decoration: const BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(10)),
          boxShadow: [
            BoxShadow(
                color: Color.fromRGBO(0, 0, 0, 0.2),
                offset: Offset(0, 3),
                blurRadius: 3
            )
          ],
        ),
        child: IconButton(
          icon: const Icon(
            Icons.arrow_forward_ios,
            color: greenColor2,
            size: 33,
          ),
          onPressed: () {
            setState(() {
              String item = _inputController.text;
              _inputController.text = '';
              addIngredient(item);
            });
          },
        ),
      )
    ],
  );

  @override
  void initState() {
    super.initState();
    _inputController = TextEditingController();
    ingredients.addAll(widget.detectedIng);
    ingredients.sort();
  }

  @override
  void dispose() {
    _inputController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    KukieUser? user = context.read<KukieUser?>();

    return createDefaultAppBody(
      context: context,
      backgroundColor: blueColor,
      bottomNavigationBar: const KukieBottomNavigationBar(),
      leading: createIconButtonForAppBar(
        onPressed: () {
          Navigator.pop(context);
        },
        iconColor: Colors.white
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  ElevatedButton(
                    style: roundedButton.copyWith(
                      backgroundColor: getMaterialColorProperty(greenColor1)
                    ),
                    onPressed: () async {
                      XFile? img = await takePhoto();
                      if (img != null) {
                        final List<String> detectedIng = await Navigator.push(
                          context,
                          createFadingPageRouteBuilder(child: ProcessingScreen(
                            imageFile: img,
                          ),),
                        );
                        ingredients.addAll(detectedIng);
                        ingredients.sort();
                      }
                    },
                    child: const Text(
                      'TAKE ANOTHER PICTURE',
                    ),
                  ),
                ],
              ),
              const SizedBox(height: 20,),
              Container(
                decoration: const BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(Radius.circular(15.0))
                ),
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 25),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'INGREDIENTS IDENTIFIED',
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                        style: Theme.of(context).textTheme.subtitle1!.copyWith(
                          color: greenColor1,
                        ),
                      ),
                      const SizedBox(height: 15,),
                      createIngredientsListWidget(
                        ingredients: ingredients,
                        canDelete: true,
                      ),
                    ],
                  ),
                ),
              ),
              const SizedBox(height: 20,),
              _createInputWidget(),
              const SizedBox(height: 60,),
              Text(
                'WHENEVER YOU\'RE READY...',
                maxLines: 3,
                overflow: TextOverflow.ellipsis,
                textAlign: TextAlign.center,
                style: GoogleFonts.lato(
                  color: Colors.white,
                  fontWeight: FontWeight.normal,
                  textStyle: Theme.of(context).textTheme.subtitle2
                ),
              ),
              const SizedBox(height: 20,),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  ElevatedButton(
                    style: roundedButton.copyWith(
                        backgroundColor: getMaterialColorProperty(greenColor1)
                    ),
                    onPressed: () {
                      Navigator.push(
                        context,
                        createFadingPageRouteBuilder(child: RecipeListScreen(
                          ingredients: ingredients,
                          userUid: user?.uid ?? '',
                        ))
                      );
                    },
                    child: const Padding(
                      padding: EdgeInsets.symmetric(horizontal: 5),
                      child: Text(
                        'LOOK FOR RECIPES',
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      )
    );
  }
}

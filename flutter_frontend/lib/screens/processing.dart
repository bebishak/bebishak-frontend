import 'dart:async';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_frontend/screens/buffer_screen.dart';
import 'package:flutter_frontend/services/backend.dart';
import 'package:flutter_frontend/shared/constants.dart';
import 'package:flutter_frontend/shared/custom_painter.dart';
import 'package:flutter_frontend/shared/utility_functions.dart';
import 'package:flutter_frontend/widgets/auth_enforcer.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:image_picker/image_picker.dart';

class ProcessingScreen extends StatefulWidget {

  final XFile imageFile;

  const ProcessingScreen({
    Key? key,
    required this.imageFile,
  }) : super(key: key);

  @override
  State<ProcessingScreen> createState() => _ProcessingScreenState();
}

class _ProcessingScreenState extends State<ProcessingScreen> {

  Future<List<String>>? ingredientFuture;

  @override
  void initState() {
    super.initState();
    ingredientFuture = identifyIngredients(widget.imageFile);
  }

  @override
  Widget build(BuildContext context) {
    return AuthEnforcer(
      informUserWhenSessionExpire: true,
      child: FutureBuilder<List<String>>(
        future: ingredientFuture,
        builder: (context, snapshot) {

          bool isDone = snapshot.connectionState == ConnectionState.done;

          if (isDone) {
            Future.delayed(const Duration(seconds: 1,)).then((value) {
              Navigator.pop(
                context,
                  !snapshot.hasError && snapshot.hasData ? snapshot.data ?? [] : []
              );
            });
          }

          return BufferScreen(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(50, 15, 50, 30),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    isDone? 'DONEZO!' : 'PROCESSING INGREDIENTS',
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.subtitle1!.copyWith(
                      color: brownColor1,
                      letterSpacing: 2,
                      height: calculateLineSpacing(20, 32),
                      wordSpacing: calculateLineSpacing(20, 32),
                    ),
                  ),
                  const SizedBox(height: 45,),
                  Wrap(
                    children: [
                      Stack(
                        children: [
                          isDone ? Center(
                            child: SizedBox.fromSize(
                              size: const Size.square(128),
                              child: CustomPaint(
                                painter: CirclePainter(
                                  color: greenColor1,
                                  paintWidth: 7
                                ),
                              ),
                            ),
                          ) : const SpinKitRing(
                            key: Key('rotatingCircle'),
                            color: greenColor1,
                            size: 128,
                          ),
                          SizedBox(
                            height: 128,
                            child: Center(
                              child: Image.asset(
                                'assets/${isDone? 'cook-book' : 'cook-book (1)'}.png',
                                fit: BoxFit.scaleDown,
                                width: 75,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                  const SizedBox(height: 55,),
                  // const SizedBox(height: 30),
                  // Container(
                  //   constraints: const BoxConstraints(
                  //     minHeight: 128
                  //   ),
                  //   decoration: const BoxDecoration(
                  //     color: greyColor,
                  //     borderRadius: BorderRadius.all(Radius.circular(20.0)),
                  //   ),
                  //   child: Center(
                  //     child: Text(
                  //       'AD',
                  //       style: Theme.of(context).textTheme.headline6!.copyWith(
                  //         color: greenColor1,
                  //         letterSpacing: 2.2,
                  //       ),
                  //     ),
                  //   ),
                  // ),
                  const FunFacts(),
                  const SizedBox(height: 20,)
                ],
              ),
            ),
            bottomImage: Image.asset(
              'assets/many-tools.png',
              key: const Key('manyTools'),
              fit: BoxFit.scaleDown,
            ),
          );
        }
      ),
    );
  }
}

class FunFacts extends StatefulWidget {
  const FunFacts({Key? key}) : super(key: key);

  @override
  _FunFactsState createState() => _FunFactsState();
}

class _FunFactsState extends State<FunFacts> {

  final List<String> funFacts = [
    "Making a meal menu for the week may help some people organize their food usage and back on waste",
    "Excess food, scraps, and even some bones are great ingredients for various stocks or broths",
    "One of the simplest ways to avoid food waste on the go is to bring food from home",
    "Avoid spoilage by storing cooked foods on shelves above raw foods",
  ];
  String selectedFunFact = '';

  late Timer timer;

  @override
  void initState() {
    super.initState();
    final _random = Random();
    selectedFunFact = funFacts[_random.nextInt(funFacts.length)];
    timer = Timer.periodic(const Duration(seconds: 2), (timer) {
      setState(() {
        selectedFunFact = funFacts[_random.nextInt(funFacts.length)];
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Text(
      selectedFunFact,
      textAlign: TextAlign.center,
      style: Theme.of(context).textTheme.bodyText1!.copyWith(
          color: greenColor1,
          letterSpacing: calculateLineSpacing(16, 155),
          fontWeight: FontWeight.bold,
          height: calculateLineSpacing(16, 19)
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
    timer.cancel();
  }
}

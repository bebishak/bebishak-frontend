import 'package:flutter/material.dart';
import 'package:flutter_frontend/models/models.dart';
import 'package:flutter_frontend/screens/login.dart';
import 'package:flutter_frontend/screens/register.dart';
import 'package:flutter_frontend/shared/constants.dart';
import 'package:flutter_frontend/shared/utility_functions.dart';
import 'package:flutter_frontend/shared/widget_prefabs.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:provider/provider.dart';

import 'dart:developer' as developer;

class StartScreen extends StatefulWidget {

  final bool showSessionExpireMessage;

  ///[showSessionExpireMessage] Whether to display the message
  ///that the user's session has expired via toast.
  const StartScreen({
    Key? key,
    this.showSessionExpireMessage = false,
  }) : super(key: key);

  @override
  State<StartScreen> createState() => _StartScreenState();
}

class _StartScreenState extends State<StartScreen> {

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance?.addPostFrameCallback((_) {
      // Make this route the only accessible one
      try {
        Navigator.popUntil(context, (route) => route.isFirst);
      } catch(e) {
        developer.log(
          'Error when popping start screen:',
          error: e,
          name: 'start.dart - initState()'
        );
      }

      // Show session expire toast message
      if (widget.showSessionExpireMessage) {
        Fluttertoast.showToast(
            msg: 'Your session has expired, please log in again.',
            toastLength: Toast.LENGTH_SHORT
        );
      }
    });
  }

  @override
  Widget build(BuildContext context) {

    KukieUser? user = context.watch<KukieUser?>();

    return Scaffold(
      backgroundColor: Colors.white,
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          // const SizedBox(height: 285.0),  // Gap

          // KUKIE Text
          Padding(
            padding: const EdgeInsets.only(right: 65.0),
            child: Text(
              'KUKIE',
              style: Theme.of(context).textTheme.headline1!.copyWith(
                color: greenColor1,
                fontWeight: FontWeight.bold,
                letterSpacing: 0.0,
                height: 1.0,
              ),
            ),
          ),
          Row(
            children: [
              // Salad image
              Padding(
                padding: const EdgeInsets.fromLTRB(225.0, 25.0, 0.0, 0.0),
                child: Image.asset(
                  'assets/salad.png',
                  fit: BoxFit.scaleDown,
                  height: 50.0,
                ),
              ),
              // Hot image
              Image.asset(
                'assets/hot.png',
                fit: BoxFit.scaleDown,
                height: 75.0,
              )
            ],
          ),
          // Container and divider stack
          Stack(
            children: [
              Positioned.fill(
                top: 7.5,
                child: Container(
                  color: blueColor,
                  child: ListView(
                    children: [
                      // From your pantry To your cookbook In one go text
                      Padding(
                        padding: const EdgeInsets.only(left: 25.0),
                        child: Text(
                          'FROM YOUR PANTRY\nTO YOUR COOKBOOK\nIN ONE GO',
                          style: Theme.of(context).textTheme.subtitle2!.copyWith(
                            color: Colors.white,
                            fontSize: 12.0,
                            height: 2.5,
                          ),
                        ),
                      ),

                      // const SizedBox(height: 25.0), // Gap
                      const SizedBox(height: 20.0), // Gap

                      LoginButtonOrPleaseWait(user: user),
                    ],
                  ),
                ),
              ),
              // Divider
              Container(
                height: 17.0,
                // * Height of blue container depends on bottom margin value
                margin: const EdgeInsets.fromLTRB(25.0, 0.0, 25.0, 280.0),
                decoration: BoxDecoration(
                  color: brownColor2,
                  borderRadius: BorderRadius.circular(25.0),
                ),
              ),
              Positioned.fill(
                bottom: 12,
                // 2021 text
                child: Align(
                  alignment: Alignment.bottomCenter,
                  child: Text(
                    '2021',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontFamily: 'Kiona',
                        fontSize: 22.0,
                        color: greenColor1,
                        fontWeight: FontWeight.bold,
                        letterSpacing: calculateLineSpacing(22.0, 30.0)
                    ),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}

class LoginButtonOrPleaseWait extends StatefulWidget {

  final KukieUser? user;

  const LoginButtonOrPleaseWait({
    Key? key,
    required this.user
  }) : super(key: key);

  @override
  _LoginButtonOrPleaseWaitState createState() => _LoginButtonOrPleaseWaitState();
}

class _LoginButtonOrPleaseWaitState extends State<LoginButtonOrPleaseWait> {

  bool isProcessingLogin = false;
  bool showAuthButtons = false;

  List<Widget> getRowChildren() {

    List<Widget> widgets = [];

    if (showAuthButtons) {
      // LOG IN button
      widgets.addAll([ElevatedButton(
        style: roundedButton.copyWith(
          backgroundColor: getMaterialColorProperty(brownColor1),
        ),
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 30),
          child: Text('LOG IN',
              style:Theme.of(context).textTheme.button!.copyWith(
                color: Colors.white,
                letterSpacing: 3.5
              )
          ),
        ),
        onPressed: () async {
          Navigator.push(
            context,
            createFadingPageRouteBuilder(child: const LoginScreen())
          );
          // setState(() {
          //   isProcessingLogin = true;
          // });
          // try {
          //   AuthService auth = AuthService();
          //   await auth.login();
          // } catch(e) {
          //   Fluttertoast.showToast(msg: 'Unable to log in due to an error');
          //   developer.log('Failed logging in: ${e.toString()}', name: 'start.dart');
          //   setState(() {
          //     isProcessingLogin = false;
          //   });
          // }
        },
      ),
      const SizedBox(height: 5.0), // Gap

      // REGISTER button
      ElevatedButton(
        style: roundedButton.copyWith(
          backgroundColor: getMaterialColorProperty(brownColor1),
        ),
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 15),
          child: Text('REGISTER',
              style:Theme.of(context).textTheme.button!.copyWith(
                color: Colors.white,
                letterSpacing: 3.5
              )
          ),
        ),
        onPressed: () {
          Navigator.push(
            context,
            createFadingPageRouteBuilder(child: const RegisterScreen())
          );
        },
      ),

      const SizedBox(height: 10.0), // Gap
    ]);
    } else {
      // Please wait widget
      widgets.addAll([
        Text(
          'Please wait...',
          textAlign: TextAlign.center,
          style: TextStyle(
              fontFamily: 'Kiona',
              fontSize: 22.0,
              color: greenColor1,
              fontWeight: FontWeight.bold,
              letterSpacing: calculateLineSpacing(22.0, 30.0)
          ),
        ),
        const SizedBox(width: 10,),
        const SpinKitRing(
          color: greenColor1,
          size: 22,
          lineWidth: 3,
        ),
      ]);
    }

    return widgets;
  }

  @override
  Widget build(BuildContext context) {
    showAuthButtons = !isProcessingLogin || widget.user == null || widget.user is! UninitializedKukieUser;
    // showAuthButtons = false;
    return showAuthButtons? Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: getRowChildren(),
    ) : Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: getRowChildren(),
    );
  }
}
